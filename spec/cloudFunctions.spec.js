// Hook into your testing server
var Parse = require('parse/node');
var constants = require("./constants");
Parse.initialize(constants.APPLICATION_ID, null, constants.MASTER_KEY);
Parse.serverURL = 'https://parseapi.back4app.com'
Parse.Cloud.useMasterKey()
var rewire = require('rewire');

var signup = require("../src/services/parse/server/cloud-functions").signUp(Parse);
var getMarkets = require("../src/services/parse/server/cloud-functions").getMarkets(Parse);

var purgeTable = require("./utils/purge-parse-table")(Parse);
var ResponseStub = require("./utils/response-stub");

const Market = Parse.Object.extend('Market');
const Rating = Parse.Object.extend('Rating');



describe("CloudFunctions", () => {
    beforeAll((done) => {
        Promise.all([purgeTable("User"), purgeTable("Session"), purgeTable("Market"), purgeTable("Rating")])
            .catch((e) => fail(e))
            .then(() => done());
    });

    it("should create a new account", async (done) => {
        try {

            signup({ params: { password: "azerty", email: "email@foo.bar" } })
                .then(() => fail('missing param "username", the signup should fail!'))
                .catch(error => { })

            signup({ params: { username: "lien", email: "email@foo.bar" } })
                .then(() => fail('missing param "password", the signup should fail!'))
                .catch(error => { })

            signup({ params: { username: "lien", password: "azerty" } })
                .then(() => fail('missing param "email", the signup should fail!'))
                .catch(error => { })

            signup({ params: { username: "lien", password: "azerty", email: "emailfoo.bar" } })
                .then(() => {
                    fail('invalid email, the signup should fail!')
                })
                .catch(error => {
                })

            signup({ params: { username: "lien", password: "azerty", email: "email@foobar" } })
                .then(() => {
                    fail('invalid email, the signup should fail!')
                })
                .catch(error => {
                })

            await signup({ params: { username: "lien", password: "azerty", email: "email@foo.bar" } })
            const user = await (new Parse.Query(Parse.User)).find();
            expect(user.length).toBe(1);
            expect(user[0].get("username")).toBe("lien");
            expect(user[0].get("email")).toBe("email@foo.bar");

            try {
                await signup({ params: { username: "lien", password: "azerty", email: "emaiiil@foo.bar" } })
                fail("this username already exist, the signup should fail")
            } catch (error) {
            }

            try {
                await signup({ params: { username: "liiien", password: "azerty", email: "email@foo.bar" } })
                fail("this username already exist, the signup should fail")
            } catch (error) {
            }

            done()

        } catch (error) {
            fail(error)
        }
    }, 15000);

    it("should fetch the markets", async (done) => {
        try {
            let foundMarkets = await getMarkets({ params: { region: [[1, 1], [0, 0]], filters: { types: ['indoor', 'outdoor', 'producer'] } } });
            expect(foundMarkets).toEqual([]);
            await new Market().save({
                marketType: "indoor",
                gps: new Parse.GeoPoint({ latitude: 0, longitude: 0 })
            })
            foundMarkets = await getMarkets({ params: { region: [[10, 10], [0, 0]], filters: { types: ['indoor', 'outdoor', 'producer'] } } });
            expect(foundMarkets.length).toBe(1);

            //"indoor" excluded
            foundMarkets = await getMarkets({ params: { region: [[10, 10], [0, 0]], filters: { types: ['outdoor', 'producer'] } } });
            expect(foundMarkets.length).toBe(0);


            await new Market().save({
                marketType: "outdoor",
                gps: new Parse.GeoPoint({ latitude: 50, longitude: 0 })
            })
            //last added market out of geoBox
            foundMarkets = await getMarkets({ params: { region: [[10, 10], [0, 0]], filters: { types: ['indoor', 'outdoor', 'producer'] } } });
            expect(foundMarkets.length).toBe(1);

            //market with other type should be included.
            await new Market().save({
                marketType: "outdoor",
                gps: new Parse.GeoPoint({ latitude: 10, longitude: 10 })
            })
            foundMarkets = await getMarkets({ params: { region: [[10, 10], [0, 0]], filters: { types: ['indoor', 'outdoor', 'producer'] } } });
            expect(foundMarkets.length).toBe(2);

            try {
                await getMarkets({ params: { region: [[10, 10], [0, 0]], filters: {  } } });
                fail("filters.types missing, should fail!")
            } catch (error) {
            }

            try {
                await getMarkets({ params: { region: [[10, 10], [0, 0]] } });
                fail("filters missing, should fail!")
            } catch (error) {
            }
        } catch (error) {
            console.log(error.message);

        }

        done()
    }, 15000);

});
