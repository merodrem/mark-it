// Hook into your testing server
var Parse = require('parse/node');
var constants = require("./constants");
Parse.initialize(constants.APPLICATION_ID, null, constants.MASTER_KEY);
Parse.serverURL = 'https://parseapi.back4app.com'
Parse.Cloud.useMasterKey()
var rewire = require('rewire');
const rewireHelpers = rewire("../src/services/parse/server/cloud-functions");

var storeVote = rewireHelpers.__get__('storeVote');
var normalizeText = rewireHelpers.__get__('normalizeText');
var areSchedulesEqual = rewireHelpers.__get__('areSchedulesEqual');
var areLocationsEquals = rewireHelpers.__get__('areLocationsEquals');
var selectProductType = rewireHelpers.__get__('selectProductType');
var validateEmail = rewireHelpers.__get__('validateEmail');
var getMostVotedValue = rewireHelpers.__get__('getMostVotedValue');
var marketLocationExists = rewireHelpers.__get__('marketLocationExists');
var getAverageRating = rewireHelpers.__get__('getAverageRating');

var signup = require("../src/services/parse/server/cloud-functions").signUp(Parse);
var purgeTable = require("./utils/purge-parse-table")(Parse);
var ResponseStub = require("./utils/response-stub");

const Market = Parse.Object.extend('Market');
const Rating = Parse.Object.extend('Rating');



describe("HelperFunctions", () => {
    beforeEach((done) => {
        Promise.all([purgeTable("User"), purgeTable("Vote"), purgeTable("Market")])
            .catch((e) => fail(e))
            .then(() => done());
    });

    it("check if a market already exists with a given location", async (done) => {
        const baseLocation = {
            coordinates: {
                latitude: 10,
                longitude: 0
            },
            street: "my lovely street",
            number: 42,
            zip: "6880",
            city: 'TownCity',
            country: 'Belarus'
        }
        const marketA = new Market();
        await marketA.save({
            location: baseLocation,
            gps: new Parse.GeoPoint({ latitude: 10, longitude: 0 })
        })

        expect(await marketLocationExists(baseLocation)).toBe(true)
        expect(await marketLocationExists(baseLocation, marketA.id)).toBe(false)
        expect(await marketLocationExists({ ...baseLocation, country: "Belgium" })).toBe(false)

        done()
    }, 15000);



    it("should store a vote for a market", async (done) => {
        try {
            const market = new Market();
            await market.save()
            const user = await signup({ params: { username: "lien", password: "azerty", email: "email@foo.bar" } })
            await storeVote(market, user, "marketName", "hey")
            await storeVote(market, user, "marketType", "indoor")
            await storeVote(market, user, "productType", "[\"organic\"]")
            await storeVote(market, user, "schedule", 'I don\'t care')
            await storeVote(market, user, "address", "doesn't matter either")

            const votes = await (new Parse.Query('Vote')).equalTo("market", market).equalTo("user", user).find()
            expect(votes.length).toBe(5)
            expect(votes.filter(vote => vote.get("target") == "marketName")[0].get("votedValue"))
                .toBe(JSON.stringify("hey"))
            expect(votes.filter(vote => vote.get("target") == "marketType")[0].get("votedValue"))
                .toBe(JSON.stringify('indoor'))
            expect(votes.filter(vote => vote.get("target") == "productType")[0].get("votedValue"))
                .toBe(JSON.stringify("[\"organic\"]"))
            expect(votes.filter(vote => vote.get("target") == "schedule")[0].get("votedValue"))
                .toBe(JSON.stringify("I don't care"))
            expect(votes.filter(vote => vote.get("target") == "address")[0].get("votedValue"))
                .toBe(JSON.stringify("doesn't matter either"))

            try {
                await storeVote(market, user, "marketName", "hey")
                await storeVote(market, user, "marketType", "indoor")
                await storeVote(market, user, "productType", "[\"organic\"]")
                await storeVote(market, user, "schedule", 'I don\'t care')
                await storeVote(market, user, "address", "doesn't matter either")
                fail('should have failed, the user has already voted for these features.')
            } catch (error) {

            }

            done()
        } catch (error) {
            fail(error)
        }
    }, 15000);

    it("should flatten slighlty different texts", async (done) => {
        try {
            const texts = [
                normalizeText(" a Text to be flatten"),
                normalizeText("a Text to be flatten "),
                normalizeText("a Text to be    flatten "),
                normalizeText(" a Text to be flatten "),
                normalizeText("a teXt to be fLatten"),
            ];
            texts.forEach(text => {
                expect(text).toBe("a-text-to-be-flatten")
            });
            done()
        } catch (error) {
            fail(error)
        }
    }, 15000);

    it("Compare schedules", async (done) => {
        try {
            const now = new Date();
            const baseSchedule = {
                monday: {
                    slots: [{start: new Date(1970, 0, 1, 10, 0).toISOString(), end: new Date(1970, 0, 1, 11, 0).toISOString()}],
                    open: true
                },
                tuesday: {
                    slots: [{}],
                    open: false
                },

                wednesday: {
                    slots: [{}],
                    open: false
                },

                thursday: {
                    slots: [{}],
                    open: false
                },

                friday: {
                    slots: [{}],
                    open: false
                },

                saturday: {
                    slots: [{}],
                    open: false
                },

                sunday: {
                    slots: [{}],
                    open: false
                },
                openingPeriods: [
                    {
                        start: new Date(2020, 0, 1).toISOString(),
                        end: new Date(2020, 11, 31).toISOString()
                    }
                ]
            }
            expect(areSchedulesEqual(baseSchedule, baseSchedule)).toBe(true)

            //different startingDay
            expect(areSchedulesEqual(baseSchedule, {
                ...baseSchedule, openingPeriods:
                    [{
                        start: new Date(2020, 0, 2).toISOString(),
                        end: baseSchedule.openingPeriods[0].end
                    }]
            })).toBe(false)

            //different endingDay
            expect(areSchedulesEqual(baseSchedule, {
                ...baseSchedule, openingPeriods:
                    [{
                        start: baseSchedule.openingPeriods[0].start,
                        end: new Date(2020, 11, 30).toISOString()
                    }]
            }
            )).toBe(false)

            //first period equal, next one different
            expect(areSchedulesEqual({
                ...baseSchedule, openingPeriods:
                    [baseSchedule.openingPeriods[0],
                    {
                        start: baseSchedule.openingPeriods[0].start,
                        end: new Date(2020, 11, 30).toISOString()
                    }
                    ]
            }, 
            {
                ...baseSchedule, openingPeriods:
                    [baseSchedule.openingPeriods[0],
                    {
                        start: baseSchedule.openingPeriods[0].start,
                        end: new Date(2020, 10, 30).toISOString()
                    }
                    ]
            }
            )).toBe(false)



            //different 'open' status for monday
            expect(areSchedulesEqual(baseSchedule, { ...baseSchedule, monday: { ...baseSchedule.monday, open: false } })).toBe(false)

            //different slots for monday
            expect(areSchedulesEqual(baseSchedule, { ...baseSchedule, monday: { ...baseSchedule.monday, slots: [{ start: now.toISOString(), end: now.toISOString() }] } })).toBe(false)

            //different slots for monday
            expect(areSchedulesEqual({ ...baseSchedule, monday: { ...baseSchedule.monday, slots: [{ start: now.toISOString(), end: now.toISOString() }] } }, { ...baseSchedule, monday: { ...baseSchedule.monday, slots: [{ start: now.toISOString(), end: now.toISOString() }, { start: now.toISOString(), end: new Date().toISOString() }] } })).toBe(false)

            //different slots for monday
            expect(areSchedulesEqual({ ...baseSchedule, monday: { ...baseSchedule.monday, slots: [{ start: now.toISOString(), end: new Date(2020, 11, 11, 10, 13, 14) }] } }, { ...baseSchedule, monday: { ...baseSchedule.monday, slots: [{ start: now.toISOString(), end: new Date(2020, 11, 11, 11, 13, 14) }] } })).toBe(false)

            //different slots for monday
            expect(areSchedulesEqual({ ...baseSchedule, monday: { ...baseSchedule.monday, slots: [{ start: now.toISOString(), end: new Date(2020, 11, 11, 10, 13, 14) }] } }, { ...baseSchedule, monday: { ...baseSchedule.monday, slots: [{ start: now.toISOString(), end: new Date(2020, 11, 11, 10, 14, 14) }] } })).toBe(false)
            done()
        } catch (error) {
            fail(error)
        }
    }, 15000);



    it("Compare locations", async (done) => {
        const baseLocation = {
            coordinates: {
                latitude: 0,
                longitude: 0
            },
            street: "my lovely street",
            number: 42,
            zip: "6880",
            city: 'TownCity',
            country: 'Belarus'
        }

        expect(areLocationsEquals(baseLocation, baseLocation)).toBe(true)
        expect(areLocationsEquals(baseLocation, { ...baseLocation, street: "  my LovelY streeT     " })).toBe(true)
        expect(areLocationsEquals(baseLocation, { ...baseLocation, city: " tOWnciTy" })).toBe(true)
        expect(areLocationsEquals(baseLocation, { ...baseLocation, country: "bELARUS" })).toBe(true)
        expect(areLocationsEquals(baseLocation, { ...baseLocation, coordinates: { latitude: 10, longitude: 20 } })).toBe(false)
        expect(areLocationsEquals(baseLocation, { ...baseLocation, street: "other street" })).toBe(false)
        expect(areLocationsEquals(baseLocation, { ...baseLocation, number: 0 })).toBe(false)
        expect(areLocationsEquals(baseLocation, { ...baseLocation, zip: "T22" })).toBe(false)
        expect(areLocationsEquals(baseLocation, { ...baseLocation, city: "OtherTown" })).toBe(false)
        expect(areLocationsEquals(baseLocation, { ...baseLocation, country: "Persia" })).toBe(false)
        done()
    }, 15000);

    it("get the product types", async (done) => {
        const typesA = [
            ['A', 1],
            ['B', 2],
            ['C', 3],
            ['D', 4],
            ['E', 5]
        ];

        expect(selectProductType(typesA)).not.toContain('A');
        expect(selectProductType(typesA)).not.toContain('B');
        expect(selectProductType(typesA)).toContain('C');
        expect(selectProductType(typesA)).toContain('D');
        expect(selectProductType(typesA)).toContain('E');

        const typesB = [
            ['A', 1],
            ['B', 2],
            ['C', 3],
            ['D', 4],
            ['E', 0]
        ];
        expect(selectProductType(typesB)).not.toContain('A');
        expect(selectProductType(typesB)).toContain('B');
        expect(selectProductType(typesB)).toContain('C');
        expect(selectProductType(typesB)).toContain('D');
        expect(selectProductType(typesB)).not.toContain('E');

        const typesC = [
            ['A', 1],
            ['B', 2],
            ['C', 0],
            ['D', 0],
            ['E', 0]
        ];
        expect(selectProductType(typesC)).toContain('A');
        expect(selectProductType(typesC)).toContain('B');
        expect(selectProductType(typesC)).not.toContain('C');
        expect(selectProductType(typesC)).not.toContain('D');
        expect(selectProductType(typesC)).not.toContain('E');

        const typesD = [
            ['A', 1],
            ['B', 2]
        ];
        expect(selectProductType(typesD)).not.toContain('A');
        expect(selectProductType(typesD)).toContain('B');


        done()
    }, 15000);

    it("validate an email", async (done) => {

        /**
         * these test cases come from:
         * https://codefool.tumblr.com/post/15288874550/list-of-valid-and-invalid-email-addresses
         * 
         */
        const validEmails = [
            'email@example.com',
            'firstname.lastname@example.com',
            'email@subdomain.example.com',
            'firstname+lastname@example.com',
            'email@[123.123.123.123]',
            '"email"@example.com',
            '1234567890@example.com',
            'email@example-one.com',
            '_______@example.com',
            'email@example.name',
            'email@example.museum',
            'email@example.co.jp',
            'firstname-lastname@example.com',
        ];
        const invalidEmails = [
            'plainaddress',
            '#@%^%#$@#$@#.com',
            '@example.com',
            'Joe Smith <email@example.com>',
            'email.example.com',
            '.email@example.com',
            'email.@example.com',
            'email..email@example.com',
            'email@example.com (Joe Smith)',
            'email@example',
            'email@111.222.333.44444',
            'email@example..com',
            'Abc..123@example.com',
            '"(),:;<>[\]@example.com',
            'just"not"right@example.com',
            'this\ is\"really\"not\\\\allowed@example.com'
        ]

        validEmails.forEach(email => expect(validateEmail(email)).toBe(true))
        invalidEmails.forEach(email => expect(validateEmail(email)).toBe(false))

        done()
    }, 15000);


    it("get the most voted value for a feature", async (done) => {
        const market = new Market();
        await market.save()
        const user1 = await signup({ params: { username: "j", password: "azerty", email: "email@bar.foo" } })
        const user2 = await signup({ params: { username: "u", password: "azerty", email: "e@foo.bar" } })
        const user3 = await signup({ params: { username: "l", password: "azerty", email: "m@foo.bar" } })
        const user4 = await signup({ params: { username: "i", password: "azerty", email: "a@foo.bar" } })
        const user5 = await signup({ params: { username: "e", password: "azerty", email: "i@foo.bar" } })
        const user6 = await signup({ params: { username: "n", password: "azerty", email: "l@foo.bar" } })
        await storeVote(market, user3, "marketName", "hey")
        expect(await getMostVotedValue(market, "marketName")).toBe("hey")
        await storeVote(market, user4, "marketName", "yeh")
        expect(await getMostVotedValue(market, "marketName")).toBe("yeh")
        await storeVote(market, user1, "marketName", "hey")
        expect(await getMostVotedValue(market, "marketName")).toBe("hey")
        await storeVote(market, user2, "marketName", "yeh")
        await storeVote(market, user5, "marketName", "yeh")
        await storeVote(market, user6, "marketName", "yeh")
        expect(await getMostVotedValue(market, "marketName")).toBe("yeh")

        const baseLocation = {
            coordinates: {
                latitude: 0,
                longitude: 0
            },
            street: "my lovely street",
            number: 42,
            zip: "6880",
            city: 'TownCity',
            country: 'Belarus'
        }

        await storeVote(market, user1, "address", baseLocation)
        expect(await getMostVotedValue(market, "address")).toEqual(baseLocation)
        await storeVote(market, user2, "address", { ...baseLocation, street: "  my LovelY streeT     " })
        expect(await getMostVotedValue(market, "address")).toEqual({ ...baseLocation, street: "  my LovelY streeT     " })
        await storeVote(market, user3, "address", { ...baseLocation, city: " tOWnciTy" })
        expect(await getMostVotedValue(market, "address")).toEqual({ ...baseLocation, city: " tOWnciTy" })
        await storeVote(market, user4, "address", { ...baseLocation, city: "OtherTown" })
        expect(await getMostVotedValue(market, "address")).toEqual({ ...baseLocation, city: " tOWnciTy" })
        await storeVote(market, user5, "address", { ...baseLocation, city: "OtherTown" })
        expect(await getMostVotedValue(market, "address")).toEqual({ ...baseLocation, city: " tOWnciTy" })
        await storeVote(market, user6, "address", { ...baseLocation, city: "OtherTown" })
        expect(await getMostVotedValue(market, "address")).toEqual({ ...baseLocation, city: "OtherTown" })

        await storeVote(market, user3, "marketType", "indoor")
        expect(await getMostVotedValue(market, "marketType")).toBe("indoor")
        await storeVote(market, user4, "marketType", "outdoor")
        expect(await getMostVotedValue(market, "marketType")).toBe("outdoor")
        await storeVote(market, user1, "marketType", "outdoor")
        expect(await getMostVotedValue(market, "marketType")).toBe("outdoor")
        await storeVote(market, user2, "marketType", "producer")
        expect(await getMostVotedValue(market, "marketType")).toBe("outdoor")
        await storeVote(market, user5, "marketType", "producer")
        expect(await getMostVotedValue(market, "marketType")).toBe("producer")

        const baseSchedule = {
            monday: {
                slots: [{start: new Date(1970, 0, 1, 10, 0).toISOString(), end: new Date(1970, 0, 1, 11, 0).toISOString()}],
                open: true
            },
            tuesday: {
                slots: [{}],
                open: false
            },

            wednesday: {
                slots: [{}],
                open: false
            },

            thursday: {
                slots: [{}],
                open: false
            },

            friday: {
                slots: [{}],
                open: false
            },

            saturday: {
                slots: [{}],
                open: false
            },

            sunday: {
                slots: [{}],
                open: false
            },
            openingPeriods: [
                {
                    start: new Date(2020, 0, 1).toISOString(),
                    end: new Date(2020, 11, 31).toISOString()
                }
            ]
        }

        await storeVote(market, user1, "schedule", baseSchedule)
        expect(await getMostVotedValue(market, "schedule")).toEqual(baseSchedule)
        await storeVote(market, user2, "schedule", baseSchedule)
        expect(await getMostVotedValue(market, "schedule")).toEqual(baseSchedule)
        await storeVote(market, user3, "schedule", { ...baseSchedule, monday: { ...baseSchedule.monday, open: false } })
        expect(await getMostVotedValue(market, "schedule")).toEqual(baseSchedule)
        await storeVote(market, user4, "schedule", { ...baseSchedule, monday: { ...baseSchedule.monday, open: false } })
        expect(await getMostVotedValue(market, "schedule")).toEqual({ ...baseSchedule, monday: { ...baseSchedule.monday, open: false } })
        await storeVote(market, user5, "schedule", { ...baseSchedule, monday: { ...baseSchedule.monday, open: false } })
        expect(await getMostVotedValue(market, "schedule")).toEqual({ ...baseSchedule, monday: { ...baseSchedule.monday, open: false } })


        const typesA = [
            ['A', 1],
            ['B', 2],
            ['C', 3],
            ['D', 4],
            ['E', 5]
        ];
        await storeVote(market, user1, "productTypes", ['A', 'B', 'C', 'D', 'E'])
        await storeVote(market, user2, "productTypes", ['B', 'C', 'D', 'E'])
        await storeVote(market, user3, "productTypes", ['C', 'D', 'E'])
        await storeVote(market, user4, "productTypes", ['D', 'E'])
        await storeVote(market, user5, "productTypes", ['E'])
        const types = await getMostVotedValue(market, "productTypes");
        expect(types).not.toContain('A');
        expect(types).not.toContain('B');
        expect(types).toContain('C');
        expect(types).toContain('D');
        expect(types).toContain('E');

        done()
    }, 15000);
    // it("add a vote for a feature in a market entry and updates the ranking for that entry", async (done) => {
    //     const market = new Market();
    //     await market.save({
    //         nameVote: [
    //             ["name1", 5],
    //             ["name2", 3],
    //             ["name3", 1],
    //             ["name4", 1],
    //             ["name5", 1],
    //         ]
    //     })

    //     updateRanking(market, "nameVote", "name5");
    //     expect(market.get('nameVote').findIndex(elem => elem[0] == "name5")).toBe(2)
    //     updateRanking(market, "nameVote", "name6");
    //     expect(market.get('nameVote').findIndex(elem => elem[0] == "name6")).toBe(3)
    //     updateRanking(market, "nameVote", "name6");
    //     expect(market.get('nameVote').findIndex(elem => elem[0] == "name6")).toBe(2)
    //     updateRanking(market, "nameVote", "name6");
    //     expect(market.get('nameVote').findIndex(elem => elem[0] == "name6")).toBe(1)
    //     updateRanking(market, "nameVote", "name6");
    //     expect(market.get('nameVote').findIndex(elem => elem[0] == "name6")).toBe(1)
    //     updateRanking(market, "nameVote", "name6");
    //     expect(market.get('nameVote').findIndex(elem => elem[0] == "name6")).toBe(0)

    //     expect(market.get('nameVote')[0][1]).toBe(5)
    //     expect(market.get('nameVote')[1][1]).toBe(5)
    //     expect(market.get('nameVote')[2][1]).toBe(3)
    //     expect(market.get('nameVote')[3][1]).toBe(2)
    //     expect(market.get('nameVote')[4][1]).toBe(1)
    //     expect(market.get('nameVote')[5][1]).toBe(1)


    //     try {
    //         updateRanking(market, "unknown", "name6")
    //         fail("invalid feture name, the function should fail")
    //     }
    //     catch (error) {

    //     }
    //     done()
    // }, 15000);

    it("add a vote for a feature in a market entries and updates the ranking for that entry", async (done) => {
        const market = new Market();
        await market.save();
        expect(await getAverageRating(market.id)).toBe(undefined);

        const rating1 = new Rating()
        await rating1.save({
            market: market,
            rating: 3
        })
        expect(await getAverageRating(market.id)).toBe(3);

        const rating2 = new Rating()
        await rating2.save({
            market: market,
            rating: 4
        })
        expect(await getAverageRating(market.id)).toBe(3.5);

        done()
    }, 15000);

});
