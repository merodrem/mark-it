![HunterSmash](https://framagit.org/merodrem/mark-it/-/raw/master/src/assets/img/logo.png)
# Welcome to mark'it



## About
Mark'it is born from a personnal initiative. Its conceptor, engaged against the large-scale supermarket model, took the habit to go shopping in local farmers markets. One day, he took the decision to become nomad. But when you end up in a place you don't know, it becomes quite difficult to find alternatives to supermarkets. This is how he thought about a community-driven platform to reference these local alternatives. The Mark'it project was born.

## Tools
Mark'it is built in Typescript using:
- React Native + redux for the frontend
- The BaaS Parse for as backend

## installation
1. Download/clone the project
2. In src/asstets/data folder, create a file named 'conf.ts'.
This file will contain all keys to remote APIs the app is using. The file needs to contain the following keys:<code>
export const SERVER_URL = '';//the url of the server hosting Parse
export const MARKIT_SERVER = '';//the url to your server handling livequeries (not used so far in the project)
export const GOOGLE_API_KEY = ''; // a ggogle API key for geocoding addresses into GPS coordinates
export const APP_ID = ''; // Parse application ID
export const JAVASCRIPT_KEY = ''; //Parse javascript key
export const MAPBOX_TOKEN =  ''; // a from the mapbox mapping platform used to display the map
</code>
3. For the next steps, refer to classical react native app building. [The official doc explains it perfectly](https://reactnative.dev/docs/environment-setup)
