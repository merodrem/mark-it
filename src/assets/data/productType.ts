export const PRODUCT_TYPE = Object.freeze([
        'organic',
        'food',
        'streetFood',
        'alcohol',
        'groceries',
        'clothes',
        'flea',
        'other',
]as const);