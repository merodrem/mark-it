
export const MARKET_TYPE = Object.freeze({
    outdoor: {
        pin: require('../img/outdoorPin.png'),
        logo: require('../img/outdoorLogo.png'),
        image: require('../img/outdoor.jpg')
    },
    indoor: {
        pin: require('../img/indoorPin.png'),
        logo: require('../img/indoorLogo.png'),
        image: require('../img/indoor.jpg')
    },
    producer: {
        pin: require('../img/producerPin.png'),
        logo: require('../img/producerLogo.png'),
        image: require('../img/producer.jpg')
    },
}
);