export const CAROUSEL_DATA= Object.freeze([
    {
        title: 'welcomeStep1',
        image: require('../img/welcome1.png')
    },
    {
        title: 'welcomeStep2',
        image: require('../img/welcome2.png')
    },
    {
        title: 'welcomeStep3',
        image: require('../img/welcome3.png')
    },
    {
        title: 'welcomeStep4',
        image: require('../img/welcome4.png')
    },
]);