import { StyleSheet } from "react-native";
import { SCREEN_WIDTH, SCREEN_HEIGHT } from "./appData";

export const PRIMARY_COLOR = "#B7DC81";
export const ACCENT_COLOR = "#f0b24e";
export const TERNARY_COLOR = "#F4F4F4";

const theme = StyleSheet.create({
    bgImage: {
        flex: 1,
        top: 0,
        left: 0,
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modal: {
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 16
    },
    modalButtonContainer: {
        margin: 8,
        borderRadius:0
    },
    cancelButton: {
        borderColor: 'tomato',
        borderRadius:0
    },
    cancelButtonTitle: {
        borderRadius: 0,
        color: 'tomato'
    },
    submitButton: {
        borderRadius: 0,
        backgroundColor: ACCENT_COLOR
    },
    errorText: {
        color: 'tomato'
    },
    
});

export default theme;
