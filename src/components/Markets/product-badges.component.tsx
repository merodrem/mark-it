import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Badge } from 'react-native-elements';
import { ACCENT_COLOR } from '../../assets/data/theme';
import { translate } from '../../services/i18n.service';
import { ProductType } from '../../models/market.model';



type Props = {
    productTypes: Array<ProductType>
}
function ProductBadgesComponent(props:Props) {
    return (
        <View style={styles.badges}>
            {props.productTypes.map(type => {
                return (
                    <Badge
                        value={translate(type)}
                        textStyle={styles.badgeText}
                        badgeStyle={styles.badgeContainer}
                        key={type}
                    />
                );
            }
            )}
        </View>
    );

}

const styles = StyleSheet.create({
    badgeText: {
        color: 'black',
        fontSize: 10
    },
    badgeContainer: {
        margin: 2,
        padding: 5,
        backgroundColor: ACCENT_COLOR,
        justifyContent: 'center'
    },
    badges: {
        flexDirection: 'row',
        width: 'auto', 
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },
});


export default React.memo(ProductBadgesComponent)