
import Modal from 'react-native-modal';
import { View } from 'react-native';
import React from 'react';
import { Button, AirbnbRating, Input } from 'react-native-elements';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../assets/data/appData';
import theme from '../../assets/data/theme';
import { translate } from '../../services/i18n.service';
import { ModalBaseProps } from '../../models/modalBaseProps.model';

type Props = {
    onRatingCompleted: (modalRating: number, modalRatingComment: string|undefined) => void
}&ModalBaseProps

export class RatingModalComponent extends React.Component<Props> {

    state = {
        modalRating: undefined as number|undefined
    }
    modalRatingComment: string|undefined = undefined;

    render() {
        return (
            <Modal isVisible={this.props.isVisible}
                deviceWidth={SCREEN_WIDTH}
                deviceHeight={SCREEN_HEIGHT}
                hideModalContentWhileAnimating={true}
                onBackdropPress={this.props.onHide}
                onBackButtonPress={this.props.onHide}>
                <View style={theme.modal}>
                    <AirbnbRating
                        count={5}
                        reviews={[translate("rating1"),translate("rating2"),translate("rating3"),translate("rating4"),translate("rating5")]}
                        defaultRating={0}
                        size={20}
                        onFinishRating={rating => this.setState({modalRating: rating})}
                    />
                    <Input
                        placeholder='add a comment'
                        multiline={true}
                        maxLength={500}
                        onChangeText={comment => this.modalRatingComment = comment}
                        inputStyle={{ maxHeight: SCREEN_HEIGHT / 3, fontSize:14 }}
                    />
                    <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center' }}>
                        <Button
                            containerStyle={theme.modalButtonContainer}
                            buttonStyle={theme.cancelButton}
                            titleStyle={theme.cancelButtonTitle}
                            onPress={this.props.onHide}
                            title={translate("cancel")}
                            type='outline'
                        />
                        <Button
                            containerStyle={theme.modalButtonContainer}
                            buttonStyle={theme.submitButton}
                            disabled={!this.state.modalRating}
                            onPress={() => this.state.modalRating && this.props.onRatingCompleted(this.state.modalRating, this.modalRatingComment)}
                            title={translate("submit")}
                        />
                    </View>
                </View>
            </Modal>
        );
    }
}

