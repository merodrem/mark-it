import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';
import { DAYS } from '../../assets/data/day';
import { isOpenAllYearRound } from '../../services/schedule.service';
import { formatDate, formatTime } from '../../services/date.service';
import { translate } from '../../services/i18n.service';
import { Schedule } from '../../models/schedule.model';



type Props = {
    schedule: Schedule
}

function DisplayScheduleComponent(props: Props) {
    const { schedule } = props
    
    return (
        <View style={{ width: 'auto', height: 'auto' }}>
            {DAYS.map(day => {
                return (
                    <View style={{ flexDirection: 'row', paddingBottom: 4, paddingLeft: 2, width: 'auto' }} key={day}>
                        <Text style={{ fontWeight: 'bold', marginRight: 10, width: '40%' }}>{translate(day)}:</Text>
                        {
                            schedule[day].open ?
                                <View style={{ flexDirection: 'column' }}>
                                    {
                                        schedule[day].slots.map(slot => <Text key={slot.start}>{formatTime(slot.start) + ' - ' + formatTime(slot.end)}</Text>)
                                    }
                                </View>
                                : <Text>closed</Text>
                        }
                    </View>
                )
            })}
            {!isOpenAllYearRound(schedule) &&
                <View style={{ width: '100%'}}>
                    {schedule.openingPeriods.map(period => 
                        <Text key={period.start+period.end} style={{ paddingVertical: 4 }}><Text style={{ fontWeight: 'bold' }}>{formatDate(new Date(period.start))}</Text>  {translate("toDate")}  <Text style={{ fontWeight: 'bold' }}>{formatDate(new Date(period.end))}</Text></Text>
                    )}
                </View>
            }
        </View>
    );
}



export default React.memo(DisplayScheduleComponent)