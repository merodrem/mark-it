import React from 'react';
import { Picker } from '@react-native-community/picker'
import { StyleSheet, View, ScrollView } from 'react-native';
import theme, { TERNARY_COLOR, PRIMARY_COLOR, ACCENT_COLOR } from '../../assets/data/theme';
import { Input, Text, CheckBox, Icon, Button, Header, } from 'react-native-elements';
import ScheduleInputComponent from './schedule-input.component';
import Toast from 'react-native-simple-toast';
import * as ScheduleService from '../../services/schedule.service';
import * as ParseServer from '../../services/parse/parse-server.service';
import { PRODUCT_TYPE } from '../../assets/data/productType';
import { geocode } from '../../services/google.service';
import { stringLocation } from '../../services/market.service';
import { connect } from 'react-redux';
import AddressFormComponent from './address-form.component';
import { translate } from '../../services/i18n.service';
import ConfirmGPSComponent from './confirmGPS-modal.component';
import { MARKET_TYPE } from '../../assets/data/marketType';
import { MarketType, ProductType, Market } from '../../models/market.model';
import { Schedule } from '../../models/schedule.model';
import { LatLng } from '../../models/location.model';
import { Dispatch } from 'redux';
import { NavigationProp } from '@react-navigation/native';


type State = {
    marketType: MarketType | undefined,
    productTypes: Array<ProductType>,
    isLoading: boolean,
    isConfirmGPSVisible: boolean,
    isNameValid: boolean,
    isProductTypeValid: boolean,
    isScheduleValid: boolean,
    isMarketTypeValid: boolean,
    isLocationValid: boolean,
    locationErrorMessage: string,
    coords: LatLng,
    schedule: Schedule

}

type Props = {
    dispatch: Dispatch,
    navigation: NavigationProp<any>
}

class AddMarketComponent extends React.Component<Props> {

    nameEntry = '';
    state: State = {
        marketType: undefined,
        productTypes: [],
        isLoading: false,
        isConfirmGPSVisible: false,
        isNameValid: true,
        isProductTypeValid: true,
        isScheduleValid: true,
        isMarketTypeValid: true,
        isLocationValid: true,
        locationErrorMessage: '',
        coords: { latitude: 0, longitude: 0 },

        schedule: ScheduleService.initSchedule()
    };

    _validateGPS = this._validateGPS.bind(this);


    _toggleProductType(type: ProductType) {
        let newProductArray = [...this.state.productTypes];
        if (newProductArray.includes(type)) {
            newProductArray = newProductArray.filter(elem => elem !== type);
        }
        else {
            newProductArray.push(type)
        }
        this.setState({ productTypes: newProductArray })
    }


    _validateMarket() {
        const { isMarketTypeValid, isNameValid, isProductTypeValid, isScheduleValid } = this.state;
        if (isMarketTypeValid && isNameValid && isProductTypeValid && isScheduleValid) {
            if (!this.addressFormRef.locationEntries.country || this.addressFormRef.locationEntries.country === 'UNKNOWN') {
                this.setState({ isLocationValid: false, locationErrorMessage: translate("addMarketCountryError") }, this.addressFormRef.shakeLocationInput)
            }
            else {
                this.setState({ isLoading: true })
                geocode(stringLocation({ ...this.addressFormRef.locationEntries })).then(
                    coords => {
                        this.setState({ isConfirmGPSVisible: true, coords: { latitude: coords.lat, longitude: coords.lng } })
                    }
                ).catch((error) => {
                    console.log(error.message)
                    this.setState({ isLocationValid: false, locationErrorMessage: error.message }, this.addressFormRef.shakeLocationInput)
                    this.setState({ isLoading: false })

                }
                );
            }

        }
        else {
            this._scrollViewRef.scrollTo({ x: 0, y: 0, animated: true });
        }
    }

    async _createMarket(marketData: ParseServer.MarketPayload) {

        // this.setState({ isLoading: true })

        ParseServer.addMarket(marketData)
            .then((market) => {
                this.props.dispatch({ type: 'UPDATE_USER', payload: { addedMarkets: [...this.props.user.addedMarkets, market] } })
                this.props.navigation.navigate('Map');
            })
            .catch((error) => {
                if (error.code == 209) { // 209 is InvalidSessionToken
                    ParseServer.logout()
                        .catch(error => console.log(error.message))
                        .finally(() => {
                            this.props.dispatch({ type: 'TOGGLE_CONNECTED' })
                            Toast.show(translate("403"), Toast.LONG)
                            this.props.navigation.navigate("Signing", { goBack: true })
                        })
                }
                else if (error.code == 1) {
                    this.setState({ isLocationValid: false, locationErrorMessage: translate("locationExists") }, this.addressFormRef.shakeLocationInput)

                }
                else {
                    console.log(error.message)
                }
            })
            .finally(() => this.setState({ isLoading: false }))

    }

    _validateGPS() {
        this.setState({ isConfirmGPSVisible: false })
        const location = { ...this.addressFormRef.locationEntries, coordinates: this.state.coords }
        const marketData: ParseServer.MarketPayload = {
            name: this.nameEntry,
            schedule: this.state.schedule,
            location: location,
            type: this.state.marketType,
            productTypes: this.state.productTypes,
        }
        this._createMarket(marketData)

    }


    render() {
        const { productTypes, marketType, coords, isConfirmGPSVisible, isProductTypeValid, isScheduleValid, isMarketTypeValid, isNameValid, isLoading, schedule } = this.state;
        return (
            <View style={styles.mainContainer}>
                <ConfirmGPSComponent
                    type={this.state.marketType}
                    onHide={() => this.setState({ isConfirmGPSVisible: false, isLoading: false })}
                    onValidate={this._validateGPS}
                    coords={coords}
                    isVisible={isConfirmGPSVisible}
                />
                <Header
                    containerStyle={{ backgroundColor: PRIMARY_COLOR }}
                    leftComponent={<Button
                        onPress={this.props.navigation.goBack}
                        type="clear"
                        icon={<Icon
                            name="keyboard-arrow-left"
                            color='#000'
                            size={35}
                        />}
                    />}
                    centerComponent={{ text: translate("addMarketTitle"), style: { color: '#000', fontWeight: 'bold', fontSize: 18 } }}
                />

                <ScrollView
                    ref={ref => this._scrollViewRef = ref}
                    keyboardShouldPersistTaps='handled'
                >
                    < View style={styles.infoView} >
                        <View style={styles.informationField}>
                            <Text style={[styles.subtitle, { width: '30%' }]}>{translate("addMarketName")}: </Text>
                            <Input
                                containerStyle={{ width: '70%' }}
                                maxLength={50}
                                placeholder={translate("addMarketNamePlaceholder")}
                                inputStyle={{ fontSize: 16 }}
                                onChangeText={name => this.nameEntry = name}
                                errorMessage={
                                    isNameValid ? undefined : translate("addMarketNameError")
                                } />
                        </View>
                        <View style={[styles.informationField, !isMarketTypeValid && styles.formInputError,]}>
                            <Text style={[styles.subtitle, { width: '30%' }]}>{translate("addMarketType")}: </Text>
                            <Picker
                                style={{ width: '70%' }}
                                onValueChange={(itemValue, _) => this.setState({ marketType: itemValue })}
                                selectedValue={marketType}>
                                <Picker.Item label={translate("addMarketTypePlaceholder")} value="" />
                                {Object.keys(MARKET_TYPE).map(type => <Picker.Item key={type} label={translate(type)} value={type} />)}

                            </Picker>
                        </View>
                    </View >
                    <View style={[styles.infoView, !isProductTypeValid && styles.formInputError]}>
                        <Text style={styles.subtitle}>{translate("addMarketProductType")}:</Text>
                        {
                            !isProductTypeValid ?
                                <Text style={theme.errorText}>{translate("addMarketProductsError")}</Text>
                                : null
                        }
                        <View style={styles.productList}>
                            {
                                PRODUCT_TYPE.map(type =>
                                    <CheckBox
                                        containerStyle={styles.productCheck}
                                        checkedColor={PRIMARY_COLOR}
                                        textStyle={{ fontWeight: 'normal' }}
                                        key={type}
                                        title={translate(type)}
                                        checked={productTypes.includes(type)}
                                        onPress={() => { this._toggleProductType(type) }}
                                    />
                                )
                            }
                        </View>
                    </View>
                    <View style={[styles.infoView, !isScheduleValid && styles.formInputError]}>
                        <Text style={styles.subtitle}>{translate("addMarketSchedule")}</Text>
                        {
                            !isScheduleValid ?
                                <Text style={theme.errorText}> {translate("addMarketScheduleError")}</Text>
                                : null
                        }
                        <ScheduleInputComponent
                            schedule={this.state.schedule}
                            onScheduleChange={(schedule: Schedule) => {
                                this.setState({ schedule: schedule })
                            }
                            }
                        />
                    </View>
                    <View style={styles.infoView}>
                        <Text style={styles.subtitle}>{translate("address")}:</Text>
                        <AddressFormComponent
                            ref={form => this.addressFormRef = form}
                        />
                    </View>
                    {
                        !this.state.isLocationValid &&
                        <Text style={theme.errorText}>{this.state.locationErrorMessage}</Text>
                    }
                    <Button
                        onPress={() => this.setState({
                            isNameValid: this.nameEntry.length > 2,
                            isProductTypeValid: productTypes.length > 0,
                            isMarketTypeValid: Object.keys(MARKET_TYPE).includes(marketType),
                            isScheduleValid: ScheduleService.isValid(this.state.schedule)
                        }, this._validateMarket)}
                        icon={
                            <Icon
                                name='check'
                                size={24}
                                containerStyle={{ marginLeft: 4 }}
                                color='white'
                                type='foundation'
                            />
                        }
                        iconRight
                        disabled={isLoading}
                        loading={isLoading}
                        titleStyle={{ fontWeight: "600", fontSize: 20 }}
                        buttonStyle={styles.validateButton}
                        title={translate("addMarketValidate")}
                    />
                </ScrollView>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: TERNARY_COLOR
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 15,
        paddingBottom: 15,
        backgroundColor: PRIMARY_COLOR,
        marginBottom: 10
    },
    infoView: {
        backgroundColor: 'white',
        marginBottom: 16,
        padding: 4
    },
    informationField: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 2
    },
    subtitle: {
        fontSize: 16,
        paddingVertical: 8,
    },
    productList: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start'
    },
    productCheck: {
        width: '40%',
        backgroundColor: 'white',
        borderWidth: 0,
        padding: 0,
        margin: 0,
    },
    buttonContainer: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    validateButton: {
        backgroundColor: ACCENT_COLOR,
        borderWidth: 0,
        paddingLeft: 32,
        borderRadius: 0,
        paddingRight: 32,
        margin: 32,
    },
    formInputError: {
        borderColor: 'tomato',
        borderWidth: 1
    }
});
const mapStateToProps = (state) => {
    return {
        user: { ...state.userReducer }
    }
}

export default connect(mapStateToProps)(AddMarketComponent)
