import React from 'react';
import { Text, Rating } from 'react-native-elements';
import { translate } from '../../services/i18n.service';
import { RatingModel } from '../../models/rating.model';
import { formatTimestamp } from '../../services/date.service';

type Props = {
  rating: RatingModel
}
function RatingWithCommentComponent(props:Props) {
  const { rating } = props
  
    return (
    <>
    <Text style={{fontSize:12, color:'grey', textAlign:"right"}}>{formatTimestamp(new Date(props.rating.createdAt))}</Text>
      <Rating
        imageSize={20}
        readonly
        startingValue={rating.rating}
      />
      <Text style={{ fontStyle: "italic" }}>{rating.comment ? '"' + rating.comment + '"' : translate("ratingNoComment")}</Text>
    </>

  );
}

export default React.memo(RatingWithCommentComponent)