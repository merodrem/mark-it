
import Modal from 'react-native-modal';
import { View, Image, StyleSheet } from 'react-native';
import React from 'react';
import { Text, Button, CheckBox, Icon } from 'react-native-elements';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../../../assets/data/appData';
import theme, { ACCENT_COLOR, PRIMARY_COLOR } from '../../../assets/data/theme';
import { translate } from '../../../services/i18n.service';
import { MarketType } from '../../../models/market.model';
import { ModalBaseProps } from '../../../models/modalBaseProps.model';
import isEqual from 'lodash/isEqual'

type Props = {
    filters: {
        filterOnOpen: boolean,
        types: Array<MarketType>
    },
    onOpen: () => void,
    onIndoor: () => void,
    onOutdoor: () => void,
    onProducer: () => void,
    onRefreshFilters: () => void,
} & ModalBaseProps

function FilterModalComponent(props: Props) {

    return (
        <Modal deviceWidth={SCREEN_WIDTH}
            isVisible={props.isVisible}
            deviceHeight={SCREEN_HEIGHT}
            hideModalContentWhileAnimating={true}
            onBackdropPress={props.onHide}
            onBackButtonPress={props.onHide}>
            <View style={theme.modal}>
                <Text h4>{translate("mapFilterResults")}:</Text>
                <CheckBox
                    title={translate("mapFilterOpenMarktet")}
                    containerStyle={{ width: '80%' }}
                    checkedColor={PRIMARY_COLOR}
                    checked={props.filters.filterOnOpen}
                    onPress={() => {
                        props.onOpen()
                    }}
                />
                <View style={{ flexDirection: 'row' }}>
                    <CheckBox
                        title={<View style={styles.titleContainer}><Text>{translate("mapFilterIndoorMarktet")}</Text><Image style={styles.titleImage} resizeMode="contain" source={require("../../../assets/img/indoorPin.png")}></Image></View>}
                        containerStyle={{ width: '80%' }}
                        checkedColor={PRIMARY_COLOR}
                        checked={props.filters.types.includes("indoor")}
                        onPress={() => {
                            props.onIndoor()
                        }}
                    />
                </View>
                <CheckBox
                    title={<View style={styles.titleContainer}><Text>{translate("mapFilterOutdoorMarktet")}</Text><Image style={styles.titleImage} resizeMode="contain" source={require("../../../assets/img/outdoorPin.png")}></Image></View>}
                    containerStyle={{ width: '80%' }}
                    checkedColor={PRIMARY_COLOR}
                    checked={props.filters.types.includes("outdoor")}
                    onPress={props.onOutdoor}
                />
                <CheckBox
                    title={<View style={styles.titleContainer}><Text>{translate("mapFilterProducer")}</Text><Image style={styles.titleImage} resizeMode="contain" source={require("../../../assets/img/producerPin.png")}></Image></View>}
                    containerStyle={{ width: '80%' }}
                    checkedColor={PRIMARY_COLOR}
                    checked={props.filters.types.includes("producer")}
                    onPress={props.onProducer}
                />
                <Button
                    containerStyle={{ borderRadius: 0 }}
                    buttonStyle={{ backgroundColor: ACCENT_COLOR, borderRadius: 0, }}
                    icon={
                        <Icon
                            name='md-refresh'
                            size={25}
                            type='ionicon'
                            color={'black'}
                            iconStyle={{paddingHorizontal:8}}
                        />
                    }
                    onPress={props.onRefreshFilters}
                    titleStyle={{ color: 'black' }}
                />
            </View>
        </Modal>
    );
}

const styles = StyleSheet.create({
    titleContainer: {
        flex: 1,
        flexDirection: 'row', alignItems: 'center',
        justifyContent: "space-between"
    },
    titleImage: {
        width: 30,
        height: 30,
        alignSelf: 'flex-end'
    }
})

function areEqual(prevProps: Props, nextProps: Props) {
    return prevProps.isVisible == nextProps.isVisible
        && prevProps.filters.filterOnOpen == nextProps.filters.filterOnOpen
        && isEqual(prevProps.filters.types, nextProps.filters.types)
}

export default React.memo(FilterModalComponent, areEqual)