import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text, Button, Rating } from 'react-native-elements';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../../../assets/data/appData';
import ProductBadgesComponent from '../product-badges.component';
import { PRIMARY_COLOR } from '../../../assets/data/theme';
import { translate } from '../../../services/i18n.service';
import { ProductType } from '../../../models/market.model';
import Modal from 'react-native-modal';
import { ModalBaseProps } from '../../../models/modalBaseProps.model';

type Props = {
    onNavigate: () => void,
    name: string|undefined,
    productTypes:ProductType[],
    averageRatings: number|undefined,
} & ModalBaseProps

function MarkerCalloutComponent(props: Props) {

    return (
        <Modal
            isVisible={props.isVisible}
            deviceWidth={SCREEN_WIDTH}
            deviceHeight={SCREEN_HEIGHT}
            animationIn="slideInDown"
            animationOut="slideOutUp"
            backdropColor="rgba(0,0,0,0.5)"
            onBackButtonPress={props.onHide}
            onBackdropPress={props.onHide}
        >

            <View style={styles.mainContainer}>
                <View style={{ alignItems: 'center' }}>
                    <Text h4 style={{ textAlign: "center" }}>{props.name}</Text>
                    {props.averageRatings ? <Rating
                        readonly
                        ratingCount={5}
                        startingValue={props.averageRatings}
                        imageSize={15} /> : <Text>{translate("noRatings")}</Text>}
                </View>
                <View style={{ flexDirection: 'row', width: '100%' }}>
                    <ProductBadgesComponent
                        productTypes={props.productTypes}
                    />

                </View>
                <Button
                    title={translate("marketCallout")}
                    type="clear"
                    onPress={props.onNavigate}
                    titleStyle={{ color: PRIMARY_COLOR }}
                />
            </View>

            

        </Modal>

    );
}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: 'white',
        height: 'auto',
        width: "100%",
        position: "absolute",
        top: 0,
        padding: 4,
        zIndex: 10000,
    },
});

function areEqual(prevProps: Props, nextProps: Props) {
    return prevProps.isVisible == nextProps.isVisible
}
export default React.memo(MarkerCalloutComponent, areEqual)
