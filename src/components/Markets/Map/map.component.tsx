import React from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import MarkerCalloutComponent from './marker-callout.component';
import Toast from 'react-native-simple-toast';
import { Icon, Button } from 'react-native-elements';
import { ACCENT_COLOR, TERNARY_COLOR } from '../../../assets/data/theme';
import { getPosition } from '../../../services/geolocation.service';
import * as ParseServer from '../../../services/parse/parse-server.service';
import { MARKET_TYPE } from '../../../assets/data/marketType';
import { connect } from 'react-redux';
import FilterModalComponent from './filter-modal.component';
import { translate } from '../../../services/i18n.service';
import { GeolocationResponse } from '@react-native-community/geolocation';
import { Dispatch } from 'redux';
import { MarketType, Market } from '../../../models/market.model';
import { StackNavigationProp } from '@react-navigation/stack';
import MapboxGL from '@react-native-mapbox-gl/maps';
import { Region } from '../../../models/location.model';

function popPushInArray([...arr], elem: any) {
    const result = arr.includes(elem) ? arr.filter(val => val != elem) : (arr.push(elem), arr);
    return result;
}


type Props = {
    logged: boolean,
    navigation: StackNavigationProp<any>
    dispatch: Dispatch
}

type MapState = {
    isFilterModalVisible: boolean,
    isCalloutVisible: boolean,
    areMarketsLoading: boolean,
    filters: {
        filterOnOpen: boolean,
        types: Array<MarketType>
    },
    userCoords: GeoJSON.Position | undefined
    markets: Array<Market>,
    calloutMarket: Market | undefined
    region: Region
}
class MapComponent extends React.Component<Props> {

    state: MapState = {
        isFilterModalVisible: false,
        isCalloutVisible: false,
        areMarketsLoading: false,
        filters: {
            filterOnOpen: false,
            types: ['indoor', 'outdoor', 'producer']
        },
        userCoords: undefined,
        markets: [],
        calloutMarket: undefined,
        region: [[75.84546831962061, 85.05112862792063], [-152.32354353303077, -85.05112862791631]],
    };
    _refreshMarkets = this._refreshMarkets.bind(this)
    _addMarket = this._addMarket.bind(this);


    async _loadPosition(): Promise<GeolocationResponse> {
        try {
            const position = await getPosition()
            return position;
        } catch (error) {
            return Promise.reject(new Error(error.message));
        }
    }

    constructor(props: Props) {
        super(props)
        this._loadPosition().then(({ coords }) => {
            this.setState({
                userCoords: [coords.longitude, coords.latitude]
            })
        }
        ).catch(error => console.log(error))
    }

    componentDidMount() {

    }


    _refreshMarkets() {
        this.setState({ areMarketsLoading: true, isFilterModalVisible: false })
        ParseServer.getMarkets(this.state.region, this.state.filters)
            .then(markets => {
                this.setState({ markets: markets });
            })
            .catch(error => {
                if (error.code == 209) { // 209 is InvalidSessionToken
                    ParseServer.logout()
                        .catch(error => console.log(error.message))
                        .finally(() => {
                            this.props.dispatch({ type: 'TOGGLE_CONNECTED' })
                            ParseServer.getMarkets(this.state.region, this.state.filters).then(markets => {
                                this.setState({ markets: markets });
                            })
                        })
                }
                else {
                    Toast.show(error.message, Toast.LONG)
                }
            }).finally(() => this.setState({ areMarketsLoading: false }))
    }

    async _gotoLocation() {
        this._loadPosition().then(({ coords }) => {
            this.setState({
                userCoords: [coords.longitude, coords.latitude]
            }, () => {
                if (this.state.userCoords) {
                    this._camera.flyTo(this.state.userCoords, 2000);
                }
            });
        })
            .catch(error => {
                Toast.show(error.message, Toast.LONG)
            });
    }


    _addMarket() {
        if (this.props.logged)
            this.props.navigation.navigate("AddMarket")
        else {
            this.props.navigation.navigate("Signing", { redirectRoute: 'AddMarket' })
            Toast.show(translate("403"), Toast.LONG)


        }
    }


    render() {
        const { filters } = this.state;
        return (
            <View style={styles.mainContainer}>
                <MarkerCalloutComponent
                    isVisible={this.state.isCalloutVisible}
                    onHide={() => {
                        this.setState({ isCalloutVisible: false });
                    }
                    }
                    name={this.state.calloutMarket?.name ?? ""}
                    productTypes={this.state.calloutMarket?.productTypes?? []}
                    averageRatings={this.state.calloutMarket?.averageRatings ?? undefined}
                    onNavigate={() => {
                        const marketData = this.state.calloutMarket;
                        this.setState({ isCalloutVisible: false }, () => this.props.navigation.push("MarketDashboard", { ...marketData }));
                    }
                    }
                />

                <View
                    style={styles.newMarketButtonContainer}>
                    <TouchableOpacity
                        activeOpacity={0.5}
                    >
                        <Icon
                            name='md-add'
                            size={30}
                            type='ionicon'
                            reverse
                            raised
                            reverseColor='black'
                            color={ACCENT_COLOR}
                            onPress={this._addMarket}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.5}
                    >
                        <Icon
                            name='filter'
                            size={30}
                            type='antdesign'
                            reverse
                            raised
                            reverseColor='black'
                            color={TERNARY_COLOR}
                            onPress={() => this.setState({ isFilterModalVisible: !this.state.isFilterModalVisible })}
                        />
                    </TouchableOpacity>
                </View>
                <FilterModalComponent
                    onRefreshFilters={this._refreshMarkets}
                    filters={filters}
                    isVisible={this.state.isFilterModalVisible}
                    onHide={() => this.setState({ isFilterModalVisible: false })}
                    onOpen={() => this.setState({ filters: { ...filters, filterOnOpen: !filters.filterOnOpen } })}
                    onIndoor={() => { this.setState({ filters: { filterOnOpen: filters.filterOnOpen, types: popPushInArray(filters.types, "indoor") } }) }}
                    onOutdoor={() => { this.setState({ filters: { filterOnOpen: filters.filterOnOpen, types: popPushInArray(filters.types, "outdoor") } }) }}
                    onProducer={() => { this.setState({ filters: { filterOnOpen: filters.filterOnOpen, types: popPushInArray(filters.types, "producer") } }) }}
                />
                <Button
                    containerStyle={styles.refreshMarketsContainer}
                    buttonStyle={styles.refreshMarketsButton}
                    type="outline"
                    loading={this.state.areMarketsLoading}
                    loadingProps={{ color: "#000000" }}
                    icon={
                        <Icon
                            name='md-refresh'
                            size={20}
                            type='ionicon'
                            iconStyle={{ marginRight: 4 }}
                            color={'black'}
                        />
                    }
                    onPress={() => this._refreshMarkets()}
                    titleStyle={{ color: 'black' }}
                    title={translate("mapRefresh")}
                />

                <View style={styles.locateUserContainer}>
                    <TouchableOpacity
                        activeOpacity={0.5}
                    >
                        <Icon
                            name='md-locate'
                            size={20}
                            type='ionicon'
                            reverse
                            raised
                            reverseColor='black'
                            color={'white'}
                            onPress={() => this._gotoLocation()}
                        />
                    </TouchableOpacity>
                </View>
                <MapboxGL.MapView
                    rotateEnabled={false}
                    localizeLabels={true}
                    ref={(c) => (this._mapView = c)}
                    onRegionDidChange={region => this.setState({ region: region.properties.visibleBounds })}
                    style={styles.map}

                >
                    <MapboxGL.Camera
                        zoomLevel={this.state.userCoords ? 12 : 1}
                        centerCoordinate={this.state.userCoords}
                        ref={(c) => (this._camera = c)}
                    />

                    {this.state.userCoords &&
                        <MapboxGL.UserLocation
                        />
                    }
                    {this.state.markets.map(market => (
                        <MapboxGL.ShapeSource id={market.objectId}
                            key={market.objectId}
                            onPress={() => this.setState({ calloutMarket: market, isCalloutVisible: true })}
                            shape={
                                {
                                    type: 'Feature',
                                    properties: {
                                        icon: 'user_marker',
                                    },

                                    geometry: {
                                        type: 'Point',
                                        coordinates: [market.location.coordinates?.longitude, market.location.coordinates?.latitude]
                                    },
                                }
                            }>
                            <MapboxGL.SymbolLayer id={market.objectId} style={
                                {
                                    iconImage: MARKET_TYPE[market.marketType].pin,
                                    iconSize: 0.25,
                                    iconRotationAlignment: 'map',
                                    iconAllowOverlap: true,
                                    iconAnchor: "bottom"
                                }
                            } />
                        </MapboxGL.ShapeSource>
                    ))}
                </MapboxGL.MapView>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    map: {
        flex: 1,
        zIndex: -1,
    },
    newMarketButtonContainer: {
        margin: 15,
        position: 'absolute',
        right: 0,
        elevation: 5,
    },
    locateUserContainer: {
        margin: 15,
        position: 'absolute',
        right: 0,
        bottom: 0,
        elevation: 5,
    },
    refreshFiltersButonContainer: {
        margin: 8
    },
    refreshMarketsContainer: {
        margin: 15,
        position: 'absolute',
        left: 0,
        top: 25,
        elevation: 5,
    },
    refreshMarketsButton: {
        borderRadius: 25,
        backgroundColor: 'white'
    }
});

const mapStateToProps = (state: any) => {
    return {
        logged: state.userReducer.logged
    }
}

export default connect(mapStateToProps)(MapComponent)
