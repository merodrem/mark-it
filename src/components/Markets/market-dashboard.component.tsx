import React, { useState, useRef, MutableRefObject } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { Picker } from '@react-native-community/picker'
import { StyleSheet, UIManager, View, LayoutAnimation, Image, ActivityIndicator, GestureResponderEvent, Platform } from 'react-native';
import { Text, Rating, Button, Input, Card, Divider, Icon, Header } from 'react-native-elements';
import theme, { PRIMARY_COLOR, TERNARY_COLOR, ACCENT_COLOR } from '../../assets/data/theme';
import * as ScheduleService from '../../services/schedule.service';
import Toast from 'react-native-simple-toast';
import { stringLocation } from '../../services/market.service';
import { SCREEN_WIDTH, RATING_PAGE_SIZE } from '../../assets/data/appData';
import * as ParseServer from '../../services/parse/parse-server.service';
import { FlatList } from 'react-native-gesture-handler';
import ScheduleInputComponent from './schedule-input.component';
import { MARKET_TYPE } from '../../assets/data/marketType';
import { RatingModalComponent } from './rating-modal.component';
import { ProductTypeModalComponent } from './product-type-modal.component';
import AddressFormComponent from './address-form.component';
import { geocode } from '../../services/google.service';
import ProductBadgesComponent from './product-badges.component';
import DisplayScheduleComponent from './display-schedule.component';
import { translate } from '../../services/i18n.service';
import ConfirmGPSComponent from './confirmGPS-modal.component';
import RatingWithCommentComponent from './rating-with-comment.component';
import { RatingModel } from '../../models/rating.model';
import { MarketType, Market, ProductType } from '../../models/market.model';
import { useRoute, useNavigation } from '@react-navigation/native';
import { MarketFeature, Vote } from '../../models/vote.model';
import { UserState } from '../../store/Reducers/user/user.model';
import { LatLng } from '../../models/location.model';


if (
    Platform.OS === "android" &&
    UIManager.setLayoutAnimationEnabledExperimental
) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}
function MarketDashboardComponent() {

    const route = useRoute();
    const navigation = useNavigation();
    const user: UserState = useSelector(state => state.userReducer);
    const [market, setMarket] = useState({ ...route.params } as Market);
    const dispatch = useDispatch()
    const addressFormRef: MutableRefObject<AddressFormComponent | null> = useRef(null)

    const [ratings, setRatings] = useState([] as Array<RatingModel>);
    const [areRatingsLoading, setAreRatingsLoading] = useState(false);
    const [editMarketName, setEditMarketName] = useState(false);
    const [isConfirmGPSVisible, setIsConfirmGPSVisible] = useState(false);
    const [editMarketType, setEditMarketType] = useState(false);
    const [marketTypeVote, setMarketTypeVote] = useState('indoor' as MarketType);
    const [editSchedule, setEditSchedule] = useState(false);
    const [scheduleVote, setScheduleVote] = useState(market.schedule ?? ScheduleService.initSchedule());
    const [editAddress, setEditAddress] = useState(false);
    const [isNameValid, setIsNameValid] = useState(true);
    const [isLocationValid, setIsLocationValid] = useState(true);
    const [isScheduleValid, setIsScheduleValid] = useState(true);
    const [locationErrorMessage, setLocationErrorMessage] = useState('');
    const [coords, setCoords] = useState({ latitude: 0, longitude: 0 } as LatLng);
    const [isProductTypeModalVisible, setIsProductTypeModalVisible] = useState(false);
    const [isRatingModalVisible, setIsRatingModalVisible] = useState(false);
    const [isVoteLoading, setIsVoteLoading] = useState(false);
    const nameInput: MutableRefObject<Input | null> = useRef(null);

    let ratingsPage = useRef(0);
    let moreRatingsToLoad = useRef(true);
    let marketNameEntry = useRef(market.name);

    LayoutAnimation.configureNext({
        duration: 300,
        update: {
            property: LayoutAnimation.Properties.opacity,
            type: LayoutAnimation.Types.easeInEaseOut,
        }
    });

    _loadRatings()


    function _canEdit(target: MarketFeature) {

        return user?.ID != market.originalPoster.objectId
            && !user.votesFromUser.some(vote => vote?.market.objectId == market.objectId && vote?.target == target);
    }
    function _loadRatings() {
        const marketId = market.objectId;

        if (moreRatingsToLoad.current && !areRatingsLoading && market.averageRatings) {
            setAreRatingsLoading(true);
            ParseServer.loadRatings(marketId, ratingsPage.current)
                .then((r: Array<RatingModel>) => {
                    moreRatingsToLoad.current = r.length >= RATING_PAGE_SIZE;//should never be more than RATING_PAGE_SIZE, but well...
                    setRatings([...ratings, ...r])
                    ratingsPage.current++;
                })
                .catch(error => {
                    if (error.code == 209) { // 209 is InvalidSessionToken
                        ParseServer.logout()
                            .catch(error => console.log(error.message))
                            .finally(() => {
                                dispatch({ type: 'TOGGLE_CONNECTED' })
                                ParseServer.loadRatings(marketId, ratingsPage.current)
                                    .then((r: Array<RatingModel>) => {
                                        moreRatingsToLoad.current = r.length >= RATING_PAGE_SIZE;//should never be more than RATING_PAGE_SIZE, but well...
                                        setRatings([...ratings, ...r])
                                        ratingsPage.current++;
                                    })
                            })
                    }
                    else {
                        console.log(error)
                        moreRatingsToLoad.current = false;
                    }
                })
                .finally(() => setAreRatingsLoading(false))
        }
    }

    function _toggleEditMarketName() {
        setIsNameValid(true);
        setEditMarketName(!editMarketName);
    }

    function _submitMarketName() {
        if (marketNameEntry.current.length < 3) {
            setIsNameValid(false);
            nameInput.current && nameInput.current.shake();
        }
        else {
            setIsVoteLoading(true);
            ParseServer.voteForMarketFeature({ payload: marketNameEntry.current, marketId: market.objectId, featureType: "marketName" })
                .then(vote => {
                    _toggleEditMarketName()
                    dispatch({ type: 'UPDATE_USER', payload: { votesFromUser: [...user.votesFromUser, vote] } })
                    Toast.show(translate("voteRegistered"), Toast.LONG)
                    setMarket(vote.market);
                })
                .catch(error => {
                    if (error.code == 209) { // 209 is InvalidSessionToken
                        ParseServer.logout()
                            .catch(error => console.log(error.message))
                            .finally(() => {
                                dispatch({ type: 'TOGGLE_CONNECTED' })
                                Toast.show(translate("403"), Toast.LONG)
                                navigation.navigate("Signing", { goBack: true })
                            })
                    }
                    else {
                        Toast.show(error.message, Toast.LONG)
                    }
                }).finally(() => setIsVoteLoading(false))
        }
    }

    function _toggleEditMarketType() {
        setEditMarketType(!editMarketType)
    }

    function _submitMarketType() {
        setIsVoteLoading(true);
        ParseServer.voteForMarketFeature({ payload: marketTypeVote, marketId: market.objectId, featureType: "marketType" })
            .then((vote: Vote) => {
                _toggleEditMarketType()
                Toast.show(translate("voteRegistered"), Toast.LONG)
                dispatch({ type: 'UPDATE_USER', payload: { votesFromUser: [...user.votesFromUser, vote] } })
                setMarket(vote.market);
            })
            .catch(error => {
                if (error.code == 209) { // 209 is InvalidSessionToken
                    ParseServer.logout()
                        .catch(error => console.log(error.message))
                        .finally(() => {
                            dispatch({ type: 'TOGGLE_CONNECTED' })
                            Toast.show(translate("403"), Toast.LONG)
                            navigation.navigate("Signing", { goBack: true })
                        })
                }
                else {
                    Toast.show(error.message, Toast.LONG)
                }
            }).finally(() => setIsVoteLoading(false))
    }

    function _submitProductVote(productVotes: Array<ProductType>) {
        if (productVotes.length > 0) {
            setIsProductTypeModalVisible(false)
            setIsVoteLoading(true);
            ParseServer.voteForMarketFeature({ payload: productVotes, marketId: market.objectId, featureType: "productTypes" })
                .then((vote: Vote) => {
                    dispatch({ type: 'UPDATE_USER', payload: { votesFromUser: [...user.votesFromUser, vote] } })
                    Toast.show(translate("voteRegistered"), Toast.LONG)
                    setMarket(vote.market);
                })
                .catch(error => {
                    if (error.code == 209) { // 209 is InvalidSessionToken
                        ParseServer.logout()
                            .catch(error => console.log(error.message))
                            .finally(() => {
                                dispatch({ type: 'TOGGLE_CONNECTED' })
                                Toast.show(translate("403"), Toast.LONG)
                                navigation.navigate("Signing", { goBack: true })
                            })
                    }
                    else {
                        Toast.show(error.message, Toast.LONG)

                    }
                }).finally(() => setIsVoteLoading(false))
        }
    }

    function _toggleEditSchedule() {
        setEditSchedule(!editSchedule)
        setIsScheduleValid(true)
    }

    function _submitScheduleVote() {
        if (ScheduleService.isValid(scheduleVote)) {
            setIsVoteLoading(true);
            ParseServer.voteForMarketFeature({ payload: scheduleVote, marketId: market.objectId, featureType: "schedule" })
                .then((vote: Vote) => {
                    _toggleEditSchedule();
                    Toast.show(translate("voteRegistered"), Toast.LONG)
                    dispatch({ type: 'UPDATE_USER', payload: { votesFromUser: [...user.votesFromUser, vote] } })
                    setMarket(vote.market);
                })
                .catch(error => {
                    if (error.code == 209) { // 209 is InvalidSessionToken
                        ParseServer.logout()
                            .catch(error => console.log(error.message))
                            .finally(() => {
                                dispatch({ type: 'TOGGLE_CONNECTED' })
                                Toast.show(translate("403"), Toast.LONG)
                                navigation.navigate("Signing", { goBack: true })
                            })
                    }
                    else {
                        Toast.show(error.message, Toast.LONG)
                    }
                }).finally(() => setIsVoteLoading(false))
        }
        else {
            setIsScheduleValid(false)
        }
    }

    function _toggleEditAddress() {
        setEditAddress(!editAddress)
    }

    function _submitAddress() {
        if (addressFormRef.current) {
            if (!addressFormRef.current.locationEntries.country || addressFormRef.current.locationEntries.country === 'UNKNOWN') {
                setIsLocationValid(false)
                setLocationErrorMessage(translate("addMarketCountryError"))
                addressFormRef.current.shakeLocationInput()
            }
            else {
                geocode(stringLocation({ ...addressFormRef.current.locationEntries })).then(
                    coords => {
                        setCoords({ latitude: coords.lat, longitude: coords.lng })
                        setIsConfirmGPSVisible(true)
                    }
                ).catch(error => {
                    setIsLocationValid(false)
                    setLocationErrorMessage(error.message)
                    addressFormRef.current?.shakeLocationInput()
                }
                );
            }
        }
        else {
            Toast.show("ADDRESS IS NULL", Toast.LONG)

        }
    }

    function _renderComponent() {
        const name = market.name ?? 'market name unknown';
        const schedule = market.schedule ?? ScheduleService.initSchedule();
        const type = market.marketType ?? '';
        const productTypes = market.productTypes ?? [];
        const location = market.location ?? {};
        const averageRatings = market.averageRatings ?? undefined;
        const isOpen = ScheduleService.isOpen(schedule);
        const canRateMarket = !user?.ratingsFromUser.some(rating => rating.market.objectId == market.objectId)
        return (
            <>
                <Header
                    containerStyle={{ backgroundColor: PRIMARY_COLOR }}
                    leftComponent={<Button
                        onPress={navigation.goBack}
                        type="clear"
                        icon={<Icon
                            name="keyboard-arrow-left"
                            color='#000'
                            size={35}
                        />}
                    />}
                    centerComponent={{ text: name, style: { color: '#000', fontWeight: 'bold', fontSize: 18 } }}
                    rightComponent={(user.logged && _canEdit("marketName")) ? <Icon
                        reverse
                        name='edit'
                        type='entypo'
                        iconStyle={{ color: 'black' }}
                        color={'#fff'}
                        size={12}
                        onPress={_toggleEditMarketName} /> : undefined}

                />
                {editMarketName &&
                    <View style={styles.header}>
                        <View style={styles.title}>
                            <Input
                                keyboardAppearance="light"
                                autoCorrect={false}
                                keyboardType="default"
                                returnKeyType={'done'}
                                containerStyle={{
                                    marginTop: 16,
                                    borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                                    width: '75%'
                                }}
                                inputStyle={{ marginLeft: 10 }}
                                ref={nameInput}
                                maxLength={50}
                                defaultValue={market.name}
                                onSubmitEditing={() => _submitMarketName()}
                                onChangeText={(text) => marketNameEntry.current = text}
                                errorMessage={
                                    isNameValid ? undefined : translate("addMarketNameError")
                                }
                            />
                            {_renderCancelButton(() => _toggleEditMarketName())}
                            {_renderConfirmButton(() => _submitMarketName())}
                        </View>

                    </View>
                }
                <ProductTypeModalComponent
                    defaultTypes={productTypes}
                    isVisible={isProductTypeModalVisible}
                    onHide={() => setIsProductTypeModalVisible(false)}
                    onSubmitProductVote={_submitProductVote}
                />
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ maxWidth: '85%' }}>
                        <ProductBadgesComponent
                            productTypes={productTypes}
                        />
                    </View>
                    {user.logged && _canEdit("productTypes") ? _renderEditButton(() => setIsProductTypeModalVisible(true)) : null}
                </View>
                {
                    editMarketType ?
                        <View style={{ flexDirection: 'row' }}>
                            <Picker
                                style={{ width: '60%' }}
                                onValueChange={(itemValue, _) => setMarketTypeVote(itemValue as MarketType)}
                                selectedValue={marketTypeVote}>
                                {Object.keys(MARKET_TYPE).map(type => <Picker.Item key={type} label={translate(type)} value={type} />)}
                            </Picker>
                            {_renderCancelButton(() => _toggleEditMarketType())}
                            {_renderConfirmButton(() => _submitMarketType())}
                        </View>
                        :
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={MARKET_TYPE[type].logo} resizeMode="contain" style={{ maxWidth: 28, maxHeight: 28, margin: 4 }} />
                            <Text>{translate(type)}</Text>
                            {user.logged && _canEdit("marketType") ? _renderEditButton(() => _toggleEditMarketType()) : null}
                        </View>
                }
                <Divider style={styles.divider} />
                <View style={{ flexDirection: 'row' }}>
                    <Text h4 style={{ paddingLeft: 4 }}>{translate("addMarketSchedule")}</Text>
                    <Text style={[styles.openingStatus, !isOpen && styles.closed]}>{isOpen ? ' (' + translate("open") + ')' : ' (' + translate("closed") + ')'}</Text>
                </View>
                {
                    editSchedule ?
                        <>
                            <ScheduleInputComponent
                                schedule={scheduleVote}
                                onScheduleChange={(schedule) => setScheduleVote(schedule)}
                            />
                            <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center' }}>
                                <Button
                                    containerStyle={theme.modalButtonContainer}
                                    buttonStyle={theme.cancelButton}
                                    titleStyle={theme.cancelButtonTitle}
                                    onPress={_toggleEditSchedule}
                                    title={translate("cancel")}
                                    type='outline'
                                />
                                <Button
                                    containerStyle={theme.modalButtonContainer}
                                    buttonStyle={theme.submitButton}
                                    onPress={_submitScheduleVote}
                                    title={translate("submit")}
                                />
                            </View>
                            {
                                !isScheduleValid ?
                                    <Text style={theme.errorText}> {translate("addMarketScheduleError")}</Text>
                                    : null
                            }
                        </>
                        :
                        <View style={{ flexDirection: 'row', width: SCREEN_WIDTH, alignItems: 'center', justifyContent: 'space-evenly' }}>
                            <DisplayScheduleComponent schedule={schedule} />
                            <View style={{ width: 'auto' }}>
                                {user.logged && _canEdit("schedule") ? _renderEditButton(() => _toggleEditSchedule()) : null}
                            </View>
                        </View>
                }
                <Divider style={styles.divider} />
                <Text h4 style={{ paddingLeft: 4 }}>{translate("address")}</Text>
                {
                    editAddress ?
                        <>
                            <AddressFormComponent
                                defaultAddress={location}
                                ref={addressFormRef}
                            />
                            {
                                !isLocationValid &&
                                <Text style={theme.errorText}>{locationErrorMessage}</Text>
                            }
                            <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center' }}>
                                <Button
                                    containerStyle={theme.modalButtonContainer}
                                    buttonStyle={theme.cancelButton}
                                    titleStyle={theme.cancelButtonTitle}
                                    onPress={_toggleEditAddress}
                                    title={translate("cancel")}
                                    type='outline'
                                />
                                <Button
                                    containerStyle={theme.modalButtonContainer}
                                    buttonStyle={theme.submitButton}
                                    onPress={_submitAddress}
                                    title={translate("submit")}
                                />
                            </View>
                        </>
                        :
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: "center" }}>
                            <Text style={{ maxWidth: "80%" }}>{stringLocation(location)}</Text>
                            {user.logged && _canEdit("address") ? _renderEditButton(() => _toggleEditAddress()) : null}
                        </View>
                }
                <Divider style={styles.divider} />
                <RatingModalComponent
                    onRatingCompleted={_ratingCompleted}
                    isVisible={isRatingModalVisible}
                    onHide={() => setIsRatingModalVisible(false)}
                ></RatingModalComponent>
                <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%' }}>
                    <Text h4 style={{ paddingLeft: 4 }}>{translate("ratings")}:</Text>
                    {(ratings?.length > 0 && averageRatings) && <Rating
                        readonly
                        ratingCount={5}
                        startingValue={averageRatings}
                        fractions={1}
                        imageSize={30} />}
                </View>
                {(user.logged && canRateMarket) ?
                    <Button
                        onPress={() => setIsRatingModalVisible(true)}
                        type='clear'
                        titleStyle={{ color: PRIMARY_COLOR }}
                        title={translate("rateMarket")}
                    />
                    :
                    null
                }
            </>
        );
    }

    function _renderRating(rating: RatingModel) {
        return (
            <Card
                containerStyle={{ marginBottom: 8 }}>
                <RatingWithCommentComponent
                    rating={rating}
                />
            </Card>
        );
    }

    function _renderEditButton(onPressFunction: (event: GestureResponderEvent) => void) {

        return (
            <Icon
                reverse
                name='edit'
                type='entypo'
                color={PRIMARY_COLOR}
                size={12}
                onPress={onPressFunction} />
        );
    }

    function _renderConfirmButton(onPressFunction: (event: GestureResponderEvent) => void) {
        return (
            <Icon
                reverse
                name='check'
                type='entypo'
                color={PRIMARY_COLOR}
                size={12}
                onPress={onPressFunction} />
        );
    }

    function _renderCancelButton(onPressFunction: (event: GestureResponderEvent) => void) {
        return (
            <Icon
                reverse
                name='cross'
                type='entypo'
                color='#FFA07A'
                size={12}
                onPress={onPressFunction} />
        );
    }

    function _ratingCompleted(rating: number, comment: string | undefined) {
        setAreRatingsLoading(true);
        setIsRatingModalVisible(false)
        ParseServer.rateMarket(market.objectId, rating, comment)
            .then((rating: RatingModel) => {
                setRatings([rating, ...ratings])
                dispatch({ type: 'UPDATE_USER', payload: { ratingsFromUser: [...user.ratingsFromUser, rating] } })
            })
            .catch(error => {
                if (error.code == 209) { // 209 is InvalidSessionToken
                    ParseServer.logout()
                        .catch(error => console.log(error.message))
                        .finally(() => {
                            dispatch({ type: 'TOGGLE_CONNECTED' })
                            Toast.show(translate("403"), Toast.LONG)
                            navigation.navigate("Signing", { goBack: true })
                        })
                }
                else {
                    Toast.show(error.message, Toast.LONG)
                }
            }).finally(() => setAreRatingsLoading(false))
    }


    function _validateGPS() {
        setIsConfirmGPSVisible(false)
        if (addressFormRef.current) {
            const location = { ...addressFormRef.current.locationEntries, coordinates: coords };
            setIsVoteLoading(true);
            ParseServer.voteForMarketFeature(
                {
                    payload: location,
                    marketId: market.objectId,
                    featureType: "address"
                })
                .then((vote: Vote) => {
                    setIsConfirmGPSVisible(false)
                    Toast.show(translate("voteRegistered"), Toast.LONG)
                    dispatch({ type: 'UPDATE_USER', payload: { votesFromUser: [...user.votesFromUser, vote] } })
                    _toggleEditAddress();
                    setMarket(vote.market);
                })
                .catch(error => {
                    if (error.code == 209) { // 209 is InvalidSessionToken
                        ParseServer.logout()
                            .catch(error => console.log(error.message))
                            .finally(() => {
                                dispatch({ type: 'TOGGLE_CONNECTED' })
                                Toast.show(translate("403"), Toast.LONG)
                                navigation.navigate("Signing", { goBack: true })
                            })
                    }
                    else {
                        Toast.show(error.message, Toast.LONG)
                    }
                }).finally(() => setIsVoteLoading(false))
        }
        else {
            Toast.show("ADDRESS IS NULL", Toast.LONG)
        }
    }

    return (
        <View style={styles.container}>
            {isVoteLoading &&
                <View style={styles.loadingOverlay} >
                    <ActivityIndicator size={50} color={ACCENT_COLOR} />
                </View>}
            <ConfirmGPSComponent
                type={market.marketType}
                onHide={() => setIsConfirmGPSVisible(false)}
                onValidate={_validateGPS}
                coords={coords}
                isVisible={isConfirmGPSVisible}
            />
            <FlatList
                ListHeaderComponent={_renderComponent()}
                ListFooterComponent={areRatingsLoading ? <ActivityIndicator size="large" color={PRIMARY_COLOR} /> : (!(ratings && ratings.length > 0) && <Text>{translate("noRatings")}</Text>)}
                data={ratings}
                extraData={ratings}
                keyExtractor={(item) => item.objectId}
                onEndReachedThreshold={0}
                onEndReached={_loadRatings}
                renderItem={({ item }) => _renderRating(item)}
            />
        </View>

    );
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: TERNARY_COLOR
    },
    loadingOverlay: {
        position: "absolute",
        top: 0,
        width: "100%",
        height: "100%",
        backgroundColor: 'rgba(0,0,0,0.3)',
        zIndex: 1,
        justifyContent: 'center'
    },
    header: {
        flexDirection: 'row',
        width: '100%',
        height: 50,
        paddingLeft: 5,
        paddingRight: 5,
        alignItems: 'center',
    },
    divider: {
        marginBottom: 16,
        marginTop: 8
    },
    title: {
        flex: 3,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 8
    },
    openingStatus: {
        flex: 1,
        fontWeight: 'bold',
        color: 'green'
    },
    closed: {
        color: 'tomato',
    },
    container: {
        backgroundColor: 'white',
        flex: 1
    }
});


export default MarketDashboardComponent
