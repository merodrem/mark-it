import React, { useState } from 'react';
import { View } from 'react-native';
import DatePeriodInputComponent from './dateperiod-input.component';
import { DAYS } from '../../assets/data/day';
import * as DateService from '../../services/date.service';
import cloneDeep from 'lodash/cloneDeep'
import DayInputComponent from './day-input.component';
import { Schedule, DayOfWeek, Day } from '../../models/schedule.model';
import { CheckBox, Button } from 'react-native-elements';
import { translate } from '../../services/i18n.service';
import { ACCENT_COLOR, PRIMARY_COLOR } from '../../assets/data/theme';
import { isOpenAllYearRound } from '../../services/schedule.service';


type Props = {
    schedule: Schedule,
    onScheduleChange: (schedule: Schedule) => void
}
function ScheduleInputComponent(props: Props) {

    const [openAllYear, setOpenAllYear] = useState(isOpenAllYearRound(props.schedule));


    function _addPeriod() {
        const schedule = { ...props.schedule };
        const [lastPeriod] = schedule.openingPeriods.slice(-1);
        schedule.openingPeriods.push(lastPeriod);
        props.onScheduleChange(schedule)
    }

    function _popPeriod() {
        const schedule = { ...props.schedule };
        schedule.openingPeriods.pop();
        props.onScheduleChange(schedule)
    }
    // const onDateChange = (startingDay: Date, endingDay: Date) => {
    //     const schedule = { ...props.schedule }
    //     schedule.startingDay = DateService.toISODate(startingDay)
    //     schedule.endingDay = DateService.toISODate(endingDay)
    //     props.onScheduleChange(schedule)
    // }

    function onDayChange(day: DayOfWeek, newValue: Day) {

        const schedule = { ...props.schedule }
        schedule[day] = newValue
        props.onScheduleChange(schedule)
    }

    function copyDay(day: DayOfWeek) {
        const dayToCopy = props.schedule[day];
        const schedule = { ...props.schedule }
        DAYS.forEach(day => schedule[day] = cloneDeep(dayToCopy))

        props.onScheduleChange(schedule)
    }

    return (
        <>
            {DAYS.map(dayOfWeek => <DayInputComponent
                key={dayOfWeek}
                dayOfWeek={dayOfWeek}
                dayValue={props.schedule[dayOfWeek]}
                onDayChange={onDayChange}
                onCopy={copyDay}
            />)}
            <View style={{ alignItems: 'center' }}>
                <CheckBox
                    center
                    title={translate("allYear") + '?'}
                    checkedColor={ACCENT_COLOR}
                    checked={openAllYear}
                    onPress={() => {
                        if (!openAllYear) {
                            props.onScheduleChange(
                                {
                                    ...props.schedule,
                                    openingPeriods: [
                                        {
                                            start: DateService.toISODate(new Date(new Date().getFullYear(), 0, 1)),
                                            end: DateService.toISODate(new Date(new Date().getFullYear(), 11, 31))
                                        }
                                    ]
                                }
                            )
                        }
                        setOpenAllYear(!openAllYear)
                    }}
                />
                {
                    !openAllYear &&
                    <>
                        {
                            props.schedule.openingPeriods.map((period, index) =>
                                <DatePeriodInputComponent
                                    key={index}
                                    startingDay={new Date(period.start)}
                                    endingDay={new Date(period.end)}
                                    onDateChange={(startingDay, endingDay) => {
                                        const schedule = { ...props.schedule }
                                        schedule.openingPeriods[index] = { start: DateService.toISODate(startingDay), end: DateService.toISODate(endingDay) }
                                        props.onScheduleChange(schedule)
                                    }
                                    }
                                />
                            )
                        }
                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                            {props.schedule.openingPeriods.length > 1 && <Button onPress={_popPeriod} type='clear' titleStyle={{ color: 'salmon' }} title={translate("removePeriod")} />}
                            <Button onPress={_addPeriod} type='clear' titleStyle={{ color: PRIMARY_COLOR }} title={translate("addPeriod")} />
                        </View>
                    </>
                }
            </View>
        </>
    );
}



export default ScheduleInputComponent