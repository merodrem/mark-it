import React from 'react';
import Modal from 'react-native-modal';
import { StyleSheet, View } from 'react-native';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../../assets/data/appData';
import theme, { ACCENT_COLOR } from '../../assets/data/theme';
import { Text, Button } from 'react-native-elements';
import { translate } from '../../services/i18n.service';
import { MARKET_TYPE } from '../../assets/data/marketType';
import { ModalBaseProps } from '../../models/modalBaseProps.model';
import { MarketType } from '../../models/market.model';
import MapboxGL from '@react-native-mapbox-gl/maps';
import { LatLng } from '../../models/location.model';

type Props = {
  coords: LatLng,
  type: MarketType | undefined,
  onValidate: () => void
} & ModalBaseProps

function ConfirmGPSComponent(props: Props) {
  return (
    <Modal isVisible={props.isVisible}
      deviceWidth={SCREEN_WIDTH}
      deviceHeight={SCREEN_HEIGHT}
      hideModalContentWhileAnimating={true}
      onBackButtonPress={props.onHide}
      onBackdropPress={props.onHide}
    >
      <View style={theme.modal}>

        <Text style={{ textAlign: 'center' }}>{translate("confirmLocation")}</Text>
        <View
          style={{ width: '100%', height: 250, overflow: "hidden" }}
        >



          <MapboxGL.MapView
            rotateEnabled={false}
            localizeLabels={true}
            style={{ width: 'auto', height: 280, zIndex: -1 }}
          >
            <MapboxGL.Camera
              zoomLevel={12}
              centerCoordinate={[props.coords.longitude, props.coords.latitude]}
            />
            <MapboxGL.ShapeSource id="exampleShapeSource"
              shape={
                {
                  type: 'Feature',
                  properties: {
                    icon: 'user_marker',
                  },

                  geometry: {
                    type: 'Point',
                    coordinates: [props.coords.longitude, props.coords.latitude]
                  },
                }
              }>
              <MapboxGL.SymbolLayer id="exampleIconName" style={
                {
                  iconImage: MARKET_TYPE[props.type]?.pin ?? require('../../assets/img/user_marker.png'),
                  iconSize: 0.25,
                  iconRotationAlignment: 'map',
                  iconAllowOverlap: true,
                  iconAnchor: "bottom"
                }
              } />
            </MapboxGL.ShapeSource>
          </MapboxGL.MapView>
        </View>
        <View style={{ width: '100%', justifyContent: 'center' }}>
          <Button
            containerStyle={theme.modalButtonContainer}
            titleStyle={theme.cancelButtonTitle}
            onPress={props.onHide}
            title={translate("confirmLocationNo")}
            type="clear"
          />
          <Button
            containerStyle={theme.modalButtonContainer}
            buttonStyle={theme.submitButton}
            onPress={props.onValidate}
            title={translate("confirmLocationYes")}
          />
        </View>
      </View>
    </Modal>
  );
}

function areEqual(prevProps: Props, nextProps: Props) {
  return prevProps.isVisible == nextProps.isVisible;
}
export default React.memo(ConfirmGPSComponent, areEqual)