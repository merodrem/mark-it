import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, CheckBox, Divider, Icon } from 'react-native-elements';
import SlotInputComponent from './slot-input.component';
import { PRIMARY_COLOR, ACCENT_COLOR } from '../../assets/data/theme';
import { translate } from '../../services/i18n.service';
import { DayOfWeek, Day } from '../../models/schedule.model';
import { forceToUTC } from '../../services/date.service';



type Props = {
    dayOfWeek: DayOfWeek,
    dayValue: Day,
    onDayChange: (dayOfWeek: DayOfWeek, dayValue: Day) => void,
    onCopy: (dayOfWeek: DayOfWeek) => void
}

function DayInputComponent(props: Props) {

    function onCheck() {

        const day = { ...props.dayValue }
        day.open = !day.open
        props.onDayChange(props.dayOfWeek, day)
    }

    function addSlot() {
        const day = { ...props.dayValue }
        day.slots.push({ start: undefined, end: undefined })
        props.onDayChange(props.dayOfWeek, day)
    }

    const _removeSLot = (index: number) => {
        const day = { ...props.dayValue }
        day.slots.splice(index, 1)
        props.onDayChange(props.dayOfWeek, day)
    }

    return (

        <>
            <View style={styles.mainContainer}>
                <CheckBox
                    title={translate(props.dayOfWeek)}
                    checkedColor={ACCENT_COLOR}
                    checked={props.dayValue.open}
                    containerStyle={styles.checker}
                    onPress={onCheck}
                />

                <View style={{ flexDirection: 'column' }}>
                    {
                        // key={index} is an anti-pattern but there is no stable ID here
                        props.dayValue.slots.map((slot, index) =>
                            <View style={{ flexDirection: "row" }}
                                key={props.dayOfWeek + index}
                            >
                                <SlotInputComponent
                                    onTimeChange={(time, isStart) => {
                                        const day = { ...props.dayValue }
                                        if (isStart) {
                                            
                                            day.slots[index].start = forceToUTC(time);
                                            
                                        }
                                        else {
                                            day.slots[index].end = forceToUTC(time);
                                        }
                                        props.onDayChange(props.dayOfWeek, day)
                                    }}
                                    start={slot.start ? new Date(slot.start):undefined}
                                    end={slot.end? new Date(slot.end): undefined}
                                />
                                {index > 0 && <Button
                                    type="clear"
                                    icon={<Icon
                                        size={16}
                                        name='cross'
                                        type='entypo'
                                    />}
                                    buttonStyle={{ paddingHorizontal: 0 }}
                                    onPress={() => _removeSLot(index)}
                                />
                                }
                            </View>
                        )
                    }
                </View>
            </View>
            {props.dayValue.open &&
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    {props.dayValue.slots.length < 7 && <Button onPress={addSlot} type='clear' titleStyle={{ color: PRIMARY_COLOR }} title={translate("addSlot")} buttonStyle={{ paddingTop: 0 }} />}
                    <Button onPress={() => props.onCopy(props.dayOfWeek)} titleStyle={{ color: 'black' }} type='clear' title={translate("copySlot")} buttonStyle={{ paddingTop: 0 }} />
                </View>
            }
            <Divider />
        </>

    );
}

const styles = StyleSheet.create({
    mainContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        fontSize: 14
    },
    checker: {
        backgroundColor: 'white',
        borderWidth: 0,
        width: 120,
    },
});

export default DayInputComponent