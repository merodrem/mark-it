import React from 'react';
import { Text, Button } from 'react-native-elements';
import DateTimePicker from '@react-native-community/datetimepicker';
import { StyleSheet, View } from 'react-native';
import { translate } from '../../services/i18n.service';
import { formatTime } from '../../services/date.service';


type Props = {
    onTimeChange: (time: Date, isStart: boolean) => void
    start: Date | undefined,
    end: Date | undefined,
}

class SlotInputComponent extends React.Component<Props> {
    state = {
        isStartTimePickerVisible: false,
        isEndTimePickerVisible: false,
    }


    private _updateTime(eventType: string, time: Date | undefined, isStart: boolean) {

        if (isStart) {
            this.setState({ isStartTimePickerVisible: false })
            if (eventType == 'set') {
                time && this.props.onTimeChange(time, true)
            }
        }
        else {
            this.setState({ isEndTimePickerVisible: false })
            if (eventType == 'set') {
                time && this.props.onTimeChange(time, false)
            }
        }
    }

    shouldComponentUpdate(nextProps: Props, nextState) {

        return this.props.start != nextProps.start
            || this.props.end != nextProps.end
            || this.state.isStartTimePickerVisible != nextState.isStartTimePickerVisible
            || this.state.isEndTimePickerVisible != nextState.isEndTimePickerVisible
    }

    render() {
        const { isStartTimePickerVisible, isEndTimePickerVisible } = this.state;
        const { start, end } = this.props;
        return (

            <View style={dateStyles.mainContainer}>
                {isStartTimePickerVisible && <DateTimePicker
                    display="spinner"
                    mode='time'
                    value={start ? new Date(1970, 0, 1, start.getUTCHours(), start.getUTCMinutes()) : new Date(1970, 0, 1, 8)}
                    onChange={(event, time) => { this._updateTime(event.type, time, true) }}
                />}
                {isEndTimePickerVisible && <DateTimePicker
                    display="spinner"
                    mode='time'
                    value={end ? new Date(1970, 0, 1, end.getUTCHours(), end.getUTCMinutes()) : new Date(1970, 0, 1, 16, 30)}
                    onChange={(event, time) => { this._updateTime(event.type, time, false) }}
                />}
                <Button
                    containerStyle={dateStyles.dateButtonContainer}
                    buttonStyle={this.props.start ? dateStyles.dateButtonSolid : dateStyles.dateButtonOutline}
                    type={this.props.start ? "solid" : "outline"}
                    titleStyle={this.props.start ? { color: 'white', } : { color: 'grey' }}
                    title={this.props.start ? formatTime(this.props.start) : translate('fromTime')}
                    onPress={() => this.setState({ isStartTimePickerVisible: true })}>
                </Button>
                <Text>-</Text>
                <Button
                    containerStyle={dateStyles.dateButtonContainer}
                    buttonStyle={this.props.end ? dateStyles.dateButtonSolid : dateStyles.dateButtonOutline}
                    type={this.props.end ? "solid" : "outline"}
                    titleStyle={this.props.end ? { color: 'white', } : { color: 'grey' }}
                    title={this.props.end ? formatTime(this.props.end) : translate('toTime')}
                    onPress={() => this.setState({ isEndTimePickerVisible: true })}>
                </Button>

            </View>
        );
    }

}

export const dateStyles = StyleSheet.create({
    mainContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        fontSize: 14,
        paddingBottom: 2,
    },
    dateButtonContainer: {
        marginHorizontal: 8,
        paddingVertical: 2
    },
    dateButtonOutline: {
        borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 0,
        minWidth: 70,
        paddingVertical: 4

    },
    dateButtonSolid: {
        backgroundColor: 'grey',
        borderRadius: 0,
        minWidth: 70,
        paddingVertical: 4

    }
});


export default SlotInputComponent