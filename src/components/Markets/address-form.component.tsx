import React from 'react';
import { View } from 'react-native';
import { Input } from 'react-native-elements';
import { COUNTRIES } from '../../assets/data/countries';
import { Picker } from '@react-native-community/picker'
import { translate } from '../../services/i18n.service';
import { Location } from '../../models/location.model'

type Props = {
    defaultAddress: Location
}
class AddressFormComponent extends React.PureComponent {
    state: Location;
    constructor(props: Props) {
        super(props)
        this.state = {
            street: props.defaultAddress?.street.trim() ?? '',
            number: props.defaultAddress?.number ?? '',
            zip: props.defaultAddress?.zip ?? '',
            city: props.defaultAddress?.city.trim() ?? '',
            country: props.defaultAddress?.country ?? 'UNKNOWN',
        }
    }

    shakeLocationInput = this.shakeLocationInput.bind(this)

    get locationEntries():Location {
        return { ...this.state, country: this.state.country };
    }


    shakeLocationInput() {
        this.streetInput.shake()
        this.numberInput.shake()
        this.zipInput.shake()
        this.cityInput.shake()
    }

    render() {
    return (
            <>
                <View style={{ flexDirection: 'row' }}>
                    <Input placeholder={translate("street")}
                        value={this.state.street}
                        containerStyle={{ width: '70%', margin: 5 }}
                        inputStyle={{ fontSize: 16 }}
                        onChangeText={street => this.setState({ street: street })}
                        returnKeyType={'next'}
                        ref={input => this.streetInput = input}
                        onSubmitEditing={() => this.numberInput.focus()} />
                    <Input placeholder={translate("number")}
                        value={this.state.number.toString()}
                        containerStyle={{ width: '25%', margin: 5 }}
                        inputStyle={{ fontSize: 16 }}
                        keyboardType='number-pad'
                        onChangeText={number => this.setState({ number: number })}
                        returnKeyType={'next'}
                        ref={input => this.numberInput = input}
                        onSubmitEditing={() => this.zipInput.focus()} />
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Input placeholder={translate("zip")}
                        value={this.state.zip}
                        containerStyle={{ width: '40%', margin: 5 }}
                        inputStyle={{ fontSize: 16 }}
                        onChangeText={zip => this.setState({ zip: zip })}
                        returnKeyType={'next'}
                        ref={input => this.zipInput = input}
                        onSubmitEditing={() => this.cityInput.focus()} />
                    <Input placeholder={translate("city")}
                        value={this.state.city}
                        containerStyle={{ width: '55%', margin: 5 }}
                        inputStyle={{ fontSize: 16 }}
                        ref={input => this.cityInput = input}
                        onChangeText={city => this.setState({ city: city })}
                        returnKeyType={'done'} />
                </View>
                <Picker
                    onValueChange={(itemValue, _) => this.setState({ country: itemValue })}
                    style={{ width: '50%' }}
                    selectedValue={this.state.country}>
                    <Picker.Item label={translate("country")} value='UNKWNON' />
                    {COUNTRIES.map(country => <Picker.Item key={country.alpha2} label={country.name} value={country.name} />)}
                </Picker>

            </>
        );
    }
}



export default AddressFormComponent
