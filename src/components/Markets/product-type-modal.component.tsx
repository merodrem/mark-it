
import Modal from 'react-native-modal';
import { View, StyleSheet } from 'react-native';
import React from 'react';
import { Text, Button, CheckBox } from 'react-native-elements';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../../assets/data/appData';
import { PRODUCT_TYPE } from '../../assets/data/productType';
import theme, { ACCENT_COLOR } from '../../assets/data/theme';
import { translate } from '../../services/i18n.service';
import { ModalBaseProps } from '../../models/modalBaseProps.model';
import { ProductType } from '../../models/market.model';
import isEqual from "lodash/isEqual"

type Props = {
    defaultTypes: Array<ProductType>,
    onSubmitProductVote: (productVotes: Array<ProductType>) => void
} & ModalBaseProps
export class ProductTypeModalComponent extends React.Component<Props> {
    state = {
        productVotes: this.props.defaultTypes ?? [],
        isProductTypeValid: true
    }

    shouldComponentUpdate(nextProps:Props, nextState) {
        return this.props.isVisible != nextProps.isVisible
                || !isEqual(nextState, this.state)
    }

    _toggleProduct(type: ProductType) {
        let productArray = [...this.state.productVotes];
        if (productArray.includes(type)) {
            productArray = productArray.filter(elem => elem !== type)
        }
        else {
            productArray.push(type)
        }
        this.setState({ productVotes: productArray })
    }

    render() {
        return (
            <Modal isVisible={this.props.isVisible}
                deviceWidth={SCREEN_WIDTH}
                deviceHeight={SCREEN_HEIGHT}
                hideModalContentWhileAnimating={true}
                onBackdropPress={this.props.onHide}
                onBackButtonPress={this.props.onHide}>
                <View style={theme.modal}>
                    {
                        !this.state.isProductTypeValid ?
                            <Text style={theme.errorText}>{translate("addMarketProductsError")}</Text>
                            : null
                    }
                    {
                        PRODUCT_TYPE.map(type =>
                            <CheckBox
                                containerStyle={styles.productCheck}
                                textStyle={{ fontWeight: 'normal' }}
                                key={type}
                                checkedColor={ACCENT_COLOR}
                                title={translate(type)}
                                checked={this.state.productVotes.includes(type)}
                                onPress={() => { this._toggleProduct(type) }}
                            />
                        )
                    }
                    <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center' }}>
                        <Button
                            containerStyle={theme.modalButtonContainer}
                            buttonStyle={theme.cancelButton}
                            titleStyle={theme.cancelButtonTitle}
                            onPress={this.props.onHide}
                            title={translate("cancel")}
                            type='outline'
                        />
                        <Button
                            containerStyle={theme.modalButtonContainer}
                            buttonStyle={theme.submitButton}
                            onPress={() => {
                                if (this.state.productVotes.length > 0) {
                                    this.setState({ isProductTypeValid: true })
                                    this.props.onSubmitProductVote(this.state.productVotes)
                                }
                                else {
                                    this.setState({ isProductTypeValid: false })
                                }
                            }
                            }
                            title={translate("submit")}
                        />
                    </View>
                </View>
            </Modal>
        );
    }
}


const styles = StyleSheet.create({
    productCheck: {
        width: '40%',
        backgroundColor: 'white',
        borderWidth: 0,
        padding: 0,
        margin: 0,
    },
});
