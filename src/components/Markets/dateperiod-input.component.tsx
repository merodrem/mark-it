import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Text, Button, CheckBox } from 'react-native-elements';
import DateTimePicker from '@react-native-community/datetimepicker';
import * as DateService from '../../services/date.service';
import { ACCENT_COLOR } from '../../assets/data/theme';
import { dateStyles } from './slot-input.component';
import { translate } from '../../services/i18n.service';
import { Slot } from '../../models/schedule.model';




type Props = {
    onDateChange: (start: Date, end: Date) => void,
    startingDay: Date,
    endingDay: Date,
}

function DatePeriodInputComponent(props: Props) {
    const [isStartDatePickerVisible, setIsStartDatePickerVisible] = useState(false);
    const [isEndDatePickerVisible, setIsEndDatePickerVisible] = useState(false);


    return (
        <>
            <View style={styles.mainContainer}>
                <View style={{ flexDirection: 'row', alignItems: "center" }}>
                    <Text h4>{translate("open") + " " + translate('fromDate')}</Text>
                    <Button
                        title={DateService.formatDate(props.startingDay)}
                        containerStyle={dateStyles.dateButtonContainer}
                        buttonStyle={dateStyles.dateButtonSolid}
                        onPress={() => setIsStartDatePickerVisible(true)}>
                    </Button>
                    {
                        isStartDatePickerVisible &&
                        <DateTimePicker
                            value={props.startingDay}
                            mode='date'
                            display='calendar'
                            minimumDate={new Date(new Date().getFullYear(), 0, 1)}
                            maximumDate={new Date(new Date().getFullYear(), 11, 31)}
                            onChange={(event, time) => {
                                setIsStartDatePickerVisible(false);
                                if (time) {
                                    if (event.type == 'set') {
                                        props.onDateChange(time, props.endingDay)
                                    }
                                }
                            }}
                        />
                    }
                    <Text h4>{translate("toDate")}</Text>
                    <Button
                        containerStyle={dateStyles.dateButtonContainer}
                        buttonStyle={dateStyles.dateButtonSolid}
                        title={DateService.formatDate(props.endingDay)}
                        onPress={() => setIsEndDatePickerVisible(true)}>
                    </Button>
                    {
                        isEndDatePickerVisible &&
                        <DateTimePicker
                            value={props.endingDay}
                            mode='date'
                            display='calendar'
                            minimumDate={new Date(new Date().getFullYear(), 0, 1)}
                            maximumDate={new Date(new Date().getFullYear(), 11, 31)}
                            onChange={(event, time) => {
                                setIsEndDatePickerVisible(false);
                                if (time) {
                                    if (event.type == 'set') {
                                        props.onDateChange(props.startingDay, time)
                                    }
                                }
                            }}
                        />
                    }
                </View>
            </View>
        </>
    );
}
const styles = StyleSheet.create({
    mainContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        fontSize: 14
    },
    checker: {
        backgroundColor: 'white',
        borderWidth: 0,
        width: 150,
    },
    dateButton: {
        marginLeft: 5,
        marginRight: 5,
    }
});

function areEqual(prevProps:Props, nextProps:Props) {
    return prevProps.startingDay.toString() == nextProps.startingDay.toString()
    &&prevProps.endingDay.toString() == nextProps.endingDay.toString()
  }

export default React.memo(DatePeriodInputComponent,areEqual)