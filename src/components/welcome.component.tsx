import React, { useState } from 'react';
import { useDispatch } from 'react-redux'
import { Image, StyleSheet, View, ImageBackground } from 'react-native';
import { Button, Icon, Text, Card } from 'react-native-elements';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { CAROUSEL_DATA } from '../assets/data/welcomeComponentData';
import { translate } from '../services/i18n.service'
import theme, { ACCENT_COLOR, PRIMARY_COLOR } from '../assets/data/theme';
import { SCREEN_WIDTH } from '../assets/data/appData';
import { useNavigation } from '@react-navigation/native';

const BG_IMAGE = require('../assets/img/bg_welcome.jpg');



function WelcomeComponent() {
    const [activeSlide, setActiveSlide] = useState(0);
    const navigation = useNavigation()
    const dispatch = useDispatch();

    function _renderCarousel(item: { title: string, image: any }) {
        return (
            <Card
                containerStyle={{
                    padding: 0, shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,

                    elevation: 5,
                }}>
                <Image
                    style={styles.carouselItem}
                    source={item.image}
                    resizeMode="cover"
                />
                <View style={{ height: '20%', justifyContent: 'center', padding: 8, backgroundColor: PRIMARY_COLOR }}>
                    <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>{translate(item.title)}</Text>
                </View>
            </Card>

        );
    }

    return (
        <View style={styles.mainContainer}>
            <ImageBackground source={BG_IMAGE} style={theme.bgImage}>
                <View style={styles.headerContainer}>
                    <Text h2 >{translate("welcome")}</Text>
                    <Image
                        style={styles.logo}
                        resizeMode='contain'
                        source={require('../assets/img/logo.png')}
                    />
                </View>
                <View style={styles.carouselContainer}>
                    <Carousel
                        data={CAROUSEL_DATA}
                        renderItem={({ item }) => _renderCarousel(item)}
                        onSnapToItem={(index: number) => setActiveSlide(index)}
                        sliderWidth={SCREEN_WIDTH}
                        itemWidth={SCREEN_WIDTH * 0.8}
                        layout='default'
                    />
                    <Pagination
                        dotsLength={CAROUSEL_DATA.length}
                        activeDotIndex={activeSlide}
                        containerStyle={{ backgroundColor: 'rgba(0, 0, 0, 0)', padding: 0, margin: 0 }}
                        dotStyle={{
                            width: 10,
                            height: 10,
                            borderRadius: 5,
                            borderWidth: 1,
                            borderColor: 'black',
                            margin: 0,
                            backgroundColor: 'rgba(255, 255, 255, 1)'
                        }}
                        inactiveDotStyle={{
                            opacity: 1
                        }}
                        inactiveDotOpacity={0.4}
                        inactiveDotScale={0.6}
                    />
                </View>
                <View style={styles.buttonContainer}>
                    <Button
                        onPress={() => { dispatch({ type: 'APP_OPENED' }); navigation.navigate("Signing") }}
                        icon={
                            <Icon
                                name='arrow-right'
                                size={25}
                                color='white'
                                type='foundation'
                            />
                        }
                        iconRight
                        titleStyle={{ fontWeight: "600", fontSize: 20 }}
                        buttonStyle={styles.nextButton}
                        title={translate("gotit")}
                    />
                </View>
            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    headerContainer: {
        flex: 2,
        padding: 5,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
    },
    logo: {
        maxWidth: 128,
        maxHeight: 128
    },
    carouselContainer: {
        flex: 8,
    },
    carouselItem: {
        width: '100%',
        height: '80%'

    },
    buttonContainer: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    nextButton: {
        backgroundColor: ACCENT_COLOR,
        borderWidth: 0,
        paddingLeft: 32,
        paddingRight: 32,
        borderRadius: 0,
        marginBottom: 16,
    },
});


export default WelcomeComponent
