import { StyleSheet, Keyboard, View } from "react-native";
import React, { useState, MutableRefObject, useRef } from 'react';
import { Input, Button, Text, Icon } from "react-native-elements";
import * as ParseServer from '../../services/parse/parse-server.service';
import { useNavigation } from "@react-navigation/native";
import { ACCENT_COLOR, PRIMARY_COLOR } from "../../assets/data/theme";
import ErrorMessageComponent from "./error-message.component";
import { validateEmail } from "../../services/email.service";
import { translate } from "../../services/i18n.service";

const ResetPasswordComponent: React.FC = (props) => {
    const [isLoading, setLoading] = useState(false);
    const [isEmailSent, setEmailSent] = useState(false);
    const [isEmailValid, setEmailValid] = useState(true);
    const [emailInput, setEmailInput] = useState("");
    const [errorMessage, setErrorMessage] = useState("");

    const emailRef: MutableRefObject<Input | null> = useRef(null);

    const navigation = useNavigation();

    return (
        <View style={{ flex: 1, paddingHorizontal: "5%", justifyContent: "center", backgroundColor:PRIMARY_COLOR }} >
            <Button
                title={translate("backToLogin")}
                containerStyle={{ position: "absolute", top: 0 }}
                type="clear"
                icon={<Icon
                    name="chevron-left"
                    type="feather"
                    size={24}
                    color='white'
                />
                }
                titleStyle={{ color: 'white' }}
                onPress={() => navigation.goBack()}
            />
            <Text h3 style={{ color: 'white', textAlign: "center", marginBottom: 16 }}>{translate("resetPasswordTitle")}</Text>
            <Text style={{ color: 'white', textAlign: "center" }}>{translate("resetPasswordDescription")}</Text>
            <Input
                containerStyle={{ paddingHorizontal: 0 }}
                inputContainerStyle={styles.inputContainer}
                inputStyle={styles.input}
                placeholderTextColor='rgba(255,255,255,0.7)'
                keyboardType="email-address"
                returnKeyType="done"
                autoCapitalize="none"
                placeholder="Email"
                ref={emailRef}
                onChangeText={text => setEmailInput(text)}
                errorStyle={{ marginTop: 0 }}
                errorMessage={isEmailValid ? "" : translate("emailFormatError")}
            />
            <Button
                title={isEmailSent ? translate("sent") : translate("submit")}
                containerStyle={{ marginVertical: 8, borderRadius: 0 }}
                loading={isLoading}
                disabled={isEmailSent}
                loadingProps={{ color: ACCENT_COLOR }}
                buttonStyle={{ backgroundColor: 'white', borderRadius: 0 }}
                titleStyle={{ color: ACCENT_COLOR }}
                onPress={() => {
                    Keyboard.dismiss()
                    setEmailValid(validateEmail(emailInput) || emailRef.current.shake());
                    if (validateEmail(emailInput)) {
                        setLoading(true)
                        ParseServer.changePassword(emailInput)
                            .then(() => setEmailSent(true))
                            .catch(error => setErrorMessage(error.message))
                            .finally(() => setLoading(false))
                    }

                }}
                iconRight
                icon={isEmailSent &&
                    <Icon
                        name="md-happy"
                        type="ionicon"
                        size={24}
                        color="grey"
                    />
                }
            />
            {errorMessage.length > 0 &&
                <ErrorMessageComponent message={errorMessage} />
            }

        </View>
    );
}
export default ResetPasswordComponent

const styles = StyleSheet.create({
    inputContainer: {
        marginBottom: 4,
        borderRadius: 0,
        borderBottomWidth: 0,
        backgroundColor: "rgba(255,255,255,0.2)",
    },
    input: {
        marginHorizontal: 0,
        color: 'white'
    },
});