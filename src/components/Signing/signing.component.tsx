import React from 'react';
import { ImageBackground, LayoutAnimation, StyleSheet, UIManager, View, Platform, Keyboard } from 'react-native';
import { Button, Icon, Input } from 'react-native-elements';
import theme, { PRIMARY_COLOR, ACCENT_COLOR } from '../../assets/data/theme';
import { SCREEN_WIDTH } from '../../assets/data/appData';
import { connect } from 'react-redux'
import * as ParseServer from '../../services/parse/parse-server.service';
import RNFetchBlob from 'rn-fetch-blob';
import { CommonActions, NavigationProp } from '@react-navigation/native';
import { validateEmail } from '../../services/email.service';
import { translate } from '../../services/i18n.service';
import { Dispatch } from 'redux';
import ErrorMessageComponent from './error-message.component';
import Toast from 'react-native-simple-toast';
import HTMLModalComponent from '../Util/HTML-modal.component';
import { TERMS } from '../../assets/lang/terms_en';
const BG_IMAGE = require('../../assets/img/bg_home.jpg');

if (
    Platform.OS === "android" &&
    UIManager.setLayoutAnimationEnabledExperimental
) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}

type State = {
    email: string,
    username: string,
    password: string,
    passwordConfirmation: string,
    selectedCategory: 0 | 1,
    isLoading: boolean,
    isTermsModalVisible: boolean,
    isUsernameValid: boolean,
    isEmailValid: boolean,
    isPasswordValid: boolean,
    isConfirmationValid: boolean,
    signingErrorMessage: string

}

type Props = {
    dispatch: Dispatch,
    route: any,
    navigation: NavigationProp<any>
}


function errorMessage(error) {
    switch (error.code) {
        case 101:
            return translate("invalidCredentials");
        case 202:
            return translate("userExists");
        case 203:
            return translate("emailExists");
        case 205:
            return translate("notVerified");
        default:
            return error.code;
    }
}

class SigningComponent extends React.Component<Props> {


    state: State = {
        email: '',
        username: '',
        password: '',
        passwordConfirmation: '',
        selectedCategory: 0,
        isLoading: false,
        isUsernameValid: true,
        isEmailValid: true,
        isPasswordValid: true,
        isConfirmationValid: true,
        signingErrorMessage: '',
        isTermsModalVisible: false,
    };

    _selectCategory = this._selectCategory.bind(this);
    login = this.login.bind(this);
    signUp = this.signUp.bind(this);

    componentDidMount() {
    }

    _selectCategory(selectedCategory: 0 | 1) {
        LayoutAnimation.easeInEaseOut()

        this.setState({
            selectedCategory,
            isLoading: false,
            isEmailValid: true,
            isPasswordValid: true,
            isConfirmationValid: true,
            signingErrorMessage: ''
        });
    }


    async login() {
        const { email, password, isEmailValid, isPasswordValid } = this.state;
        if (isEmailValid && isPasswordValid) {

            this.setState({ isLoading: true });
            try {
                const user = await ParseServer.signIn(email, password)
                let avatar = undefined;
                try {
                    const image = await RNFetchBlob.config({ fileCache: true }).fetch('GET', user.avatar.url);
                    avatar = await image.readFile('base64')
                } catch (error) {
                    avatar = undefined;
                }
                this.props.dispatch({
                    type: 'TOGGLE_CONNECTED', payload: {
                        ID: user.objectId,
                        username: user.username,
                        email: user.email,
                        emailVerified: user.emailVerified,
                        avatar: avatar
                    }
                })
                this.setState({ isLoading: false });
                const goBack = this.props.route.params?.goBack
                if (goBack) {
                    this.props.navigation.dispatch(CommonActions.goBack())
                }
                else {
                    const navigationRoute = this.props.route.params?.redirectRoute ?? 'App';
                    this.props.navigation.navigate('App', { screen: navigationRoute })
                    this.props.navigation.dispatch(
                        CommonActions.reset({
                            index: 0,
                            routes: [
                                { name: 'App' }
                            ],
                        })
                    );
                }
            } catch (error) {
                this.setState({
                    isLoading: false,
                    signingErrorMessage: errorMessage(error)
                });
            }
        }
    }


    signUp() {
        const { username, email, password, isUsernameValid, isConfirmationValid, isEmailValid, isPasswordValid } = this.state;
        if (isUsernameValid && isConfirmationValid && isEmailValid && isPasswordValid) {
            this.setState({ isLoading: true });
            ParseServer.signUp(username, email, password).then(
                () => {
                    this._selectCategory(0)
                    Toast.show(translate("signingSuccess"), Toast.LONG)
                },
                error => {
                    this.setState({
                        isLoading: false,
                        signingErrorMessage: errorMessage(error)
                    });
                }
            );
        }
    }

    _renderSigning() {
        const {
            selectedCategory,
            isLoading,
            isUsernameValid,
            isEmailValid,
            isPasswordValid,
            isConfirmationValid,
            username,
            email,
            password,
            passwordConfirmation,
        } = this.state;
        const isLoginPage = selectedCategory === 0;
        const isSignUpPage = selectedCategory === 1;
        return (
            <View style={styles.signingContainer}>
                <View style={{ flexDirection: 'row' }}>
                    <Button
                        activeOpacity={0.7}
                        onPress={() => this._selectCategory(0)}
                        containerStyle={[{ flex: 1, borderRadius: 0 }, isLoginPage && { backgroundColor: 'rgba(255, 255, 255, 0.9)', }]}

                        buttonStyle={styles.categoryButton}
                        titleStyle={[
                            styles.categoryText,
                            isLoginPage && styles.selectedCategoryText,
                        ]}
                        title={translate("login")}
                    />
                    <Button
                        activeOpacity={0.7}
                        onPress={() => this._selectCategory(1)}
                        buttonStyle={styles.categoryButton}
                        containerStyle={[{ flex: 1, borderRadius: 0 }, isSignUpPage && { backgroundColor: 'rgba(255, 255, 255, 0.9)', }]}
                        titleStyle={[
                            styles.categoryText,
                            isSignUpPage && styles.selectedCategoryText,
                        ]}
                        title={translate("register")}
                    />
                </View>
                <View style={styles.formContainer}>
                    {isSignUpPage && (
                        <Input
                            value={username}
                            leftIcon={<Icon
                                name='user-o'
                                type='font-awesome'
                                color="rgba(0, 0, 0, 0.38)"
                                size={25}
                                style={{ backgroundColor: 'transparent' }}
                            />}
                            keyboardAppearance="light"
                            autoCapitalize="none"
                            autoCorrect={false}
                            keyboardType="default"
                            returnKeyType={'next'}
                            maxLength={20}
                            blurOnSubmit={true}
                            containerStyle={{
                                maxHeight: 60,
                                borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                            }}
                            inputStyle={{ marginLeft: 10, fontSize: 16 }}
                            placeholder={translate("username")}
                            ref={input => (this.usernameRef = input)}
                            onSubmitEditing={() => this.emailInput.focus()}
                            onChangeText={username =>
                                this.setState({ username })
                            }
                            errorMessage={
                                isUsernameValid ? undefined : translate('usernameTooShort')
                            }
                            errorStyle={{
                                marginTop: 0
                            }}
                        />
                    )}
                    <Input
                        leftIcon={
                            <Icon
                                name="envelope-o"
                                type="font-awesome"
                                color="rgba(0, 0, 0, 0.38)"
                                size={25}
                                style={{ backgroundColor: 'transparent' }}
                            />
                        }
                        value={email}
                        keyboardAppearance="light"
                        autoFocus={false}
                        autoCapitalize="none"
                        autoCorrect={false}
                        maxLength={50}
                        allowFontScaling
                        keyboardType="email-address"
                        returnKeyType="next"
                        inputStyle={{ marginLeft: 10, fontSize: 16 }}
                        placeholder={translate("email")}
                        containerStyle={{
                            maxHeight: 60,
                            borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                        }}
                        ref={input => (this.emailInput = input)}
                        onSubmitEditing={() => this.passwordInput.focus()}
                        onChangeText={email => this.setState({ email })}
                        errorMessage={
                            isEmailValid ? undefined : translate('emailFormatError')
                        }
                    />
                    <Input
                        leftIcon={
                            <Icon
                                name="lock"
                                type="simple-line-icon"
                                color="rgba(0, 0, 0, 0.38)"
                                size={25}
                                style={{ backgroundColor: 'transparent' }}
                            />
                        }
                        value={password}
                        keyboardAppearance="light"
                        autoCapitalize="none"
                        maxLength={50}
                        autoCorrect={false}
                        secureTextEntry={true}
                        returnKeyType={isSignUpPage ? 'next' : 'done'}
                        blurOnSubmit={true}
                        containerStyle={{
                            maxHeight: 60,
                            borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                        }}
                        inputStyle={{ marginLeft: 10, fontSize: 16 }}
                        placeholder={translate("password")}
                        ref={input => (this.passwordInput = input)}
                        onSubmitEditing={() =>
                            isSignUpPage
                                ? this.confirmationInput.focus()
                                : this.login()
                        }
                        onChangeText={password => this.setState({ password, isPasswordValid: password.length > 5 })}
                        errorMessage={
                            isPasswordValid ? undefined : translate('passwordError')
                        }
                    />
                    {isSignUpPage && (
                        <Input
                            leftIcon={
                                <Icon
                                    name="check"
                                    color="rgba(0, 0, 0, 0.38)"
                                    size={25}
                                    style={{ backgroundColor: 'transparent' }}
                                />
                            }
                            value={passwordConfirmation}
                            maxLength={50}
                            secureTextEntry={true}
                            keyboardAppearance="light"
                            autoCapitalize="none"
                            autoCorrect={false}
                            keyboardType="default"
                            returnKeyType={'done'}
                            blurOnSubmit={true}
                            containerStyle={{
                                maxHeight: 60,
                                borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                            }}
                            inputStyle={{ marginLeft: 10, fontSize: 16 }}
                            placeholder={translate("confirm")}
                            ref={input => (this.confirmationInput = input)}
                            onSubmitEditing={() => this.signUp()}
                            onChangeText={passwordConfirmation =>
                                this.setState({ passwordConfirmation, isConfirmationValid: password === passwordConfirmation })
                            }
                            errorMessage={
                                isConfirmationValid ? undefined : translate("passwordMatchError")
                            }
                        />
                    )}
                    <Button
                        buttonStyle={styles.loginButton}
                        containerStyle={{ marginTop: 16, flex: 0, borderRadius: 0 }}
                        activeOpacity={0.8}
                        title={isLoginPage ? translate("login") : translate("register")}
                        onPress={() => {
                            Keyboard.dismiss()
                            this.setState({
                                isUsernameValid: username.length > 1 || (isSignUpPage && this.usernameRef.shake()),
                                isConfirmationValid: (password === passwordConfirmation) || (isSignUpPage ? this.confirmationInput && this.confirmationInput.shake() : false),
                                isEmailValid: validateEmail(email) || this.emailInput.shake(),
                                isPasswordValid: password.length > 5 || this.passwordInput.shake()
                            }, isLoginPage ? this.login : this.signUp)
                        }
                        }
                        titleStyle={styles.loginTextButton}
                        loading={isLoading}
                        disabled={isLoading}
                    />

                </View>
                <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                    <Button
                        type="clear"
                        titleStyle={{ color: 'white', fontSize:14 }}
                        containerStyle={{ alignSelf: "flex-start", marginTop: 8 }}
                        buttonStyle={{ padding: 0 }}
                        title={translate("forgotPassword")}
                        onPress={() => this.props.navigation.navigate("ResetPassword")}
                    />
                    <Button
                        type="clear"
                        containerStyle={{ alignSelf: "flex-start", marginTop: 8 }}
                        buttonStyle={{ padding: 0 }}
                        titleStyle={{ color: 'white', fontSize:14 }}
                        onPress={() => {
                            this.setState({ isTermsModalVisible: true })
                        }
                        }
                        title={translate("settingsTerms")}
                    />
                </View>
            </View>
        );
    }

    render() {
        return (
            <ImageBackground source={BG_IMAGE} style={theme.bgImage}>
                <HTMLModalComponent
                    html={TERMS}
                    onHide={() => this.setState({ isTermsModalVisible: false })}
                    isVisible={this.state.isTermsModalVisible}
                />
                <View style={styles.mainContainer}>
                    {this._renderSigning()}
                    {
                        (this.state.signingErrorMessage !== '') &&
                        <ErrorMessageComponent message={this.state.signingErrorMessage} />
                    }
                    <View style={styles.alternativeContainer}>
                        <Button
                            buttonStyle={styles.alternativeButton}
                            title={translate("lazy")}
                            onPress={() => this.props.navigation.dispatch(
                                CommonActions.navigate({
                                    name: 'App',
                                })
                            )}
                        />
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        height: "100%",
        alignItems: "center",
        justifyContent: "space-evenly"
    },
    signingContainer: {
        paddingBottom: 16,
    },
    selectorContainer: {
        flex: 1,
        alignItems: 'center',
    },
    selected: {
        position: 'absolute',
        height: 0,
        width: 0,
        top: -5,
        borderRightWidth: 70,
        borderColor: 'rgba(255, 255, 255, 0.9)',
        backgroundColor: 'rgba(255, 255, 255, 0.9)',
    },
    loginContainer: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    loginTextButton: {
        fontSize: 16,
        color: 'white',
        fontWeight: 'bold',
    },
    loginButton: {
        backgroundColor: ACCENT_COLOR,
        height: 50,
        borderRadius: 0,
        width: 200,
    },
    signingError: {
        color: 'tomato',
    },
    formContainer: {
        backgroundColor: 'rgba(255, 255, 255, 0.9)',
        width: SCREEN_WIDTH * 0.9,
        paddingTop: 16,
        paddingBottom: 16,
        alignItems: 'center',
    },
    loginText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: 'white',
    },
    categoryButton: {
        backgroundColor: 'transparent',
    },
    categoryText: {
        textAlign: 'center',
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: 'light',
        opacity: 0.7,
    },
    selectedCategoryText: {
        opacity: 1,
        color: "black",
        fontSize: 20,
        textDecorationLine: 'underline',

    },
    titleText: {
        color: 'white',
        fontSize: 30,
        fontFamily: 'regular',
    },
    alternativeContainer: {
        width: '90%',
        alignItems: 'center',
    },
    alternativeButton: {
        backgroundColor: PRIMARY_COLOR,
        height: 50,
        borderRadius: 0,
        marginVertical: 2
    }
});


const mapStateToProps = (state) => {
    return {
        logged: state.userReducer.logged
    }
}

export default connect(mapStateToProps)(SigningComponent)
