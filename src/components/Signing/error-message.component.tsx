import { StyleSheet } from "react-native";
import React from 'react';
import { Text } from "react-native-elements";

type Props = {
    message: string
}
const ErrorMessageComponent: React.FC<Props> = (props: Props) => {
    return (
        <Text
            style={styles.errorMessage}
        >
            {props.message}
        </Text>
    );
}
export default ErrorMessageComponent

const styles = StyleSheet.create({
    errorMessage: {
        color: 'white',
        backgroundColor: 'salmon',
        textAlign: "center",
        padding: 8,
        marginTop: 8,
        alignSelf: "center"
    }
});