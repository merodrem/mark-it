import React from 'react';
import Modal from 'react-native-modal';
import { StyleSheet, View, ScrollView } from 'react-native';
import HTMLView from 'react-native-htmlview';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../../assets/data/appData';
import theme, { ACCENT_COLOR } from '../../assets/data/theme';
import { Button } from 'react-native-elements';
import { ModalBaseProps } from '../../models/modalBaseProps.model';

type Props = {
  html:string
} & ModalBaseProps

function HTMLModalComponent(props:Props) {

  return (
    <>
      <Modal isVisible={props.isVisible}
        deviceWidth={SCREEN_WIDTH}
        deviceHeight={SCREEN_HEIGHT}
        hideModalContentWhileAnimating={true}
        onBackButtonPress={props.onHide}
        onBackdropPress={props.onHide}
      >
        <View style={theme.modal}>
          <ScrollView style={{maxHeight:'90%'}} contentContainerStyle={{ alignItems: "center", justifyContent:'space-evenly' }}>
            <HTMLView
              value={props.html}
              stylesheet={styles}
            />
          </ScrollView>
          <Button
          type="outline"
          onPress={props.onHide}
          containerStyle={{paddingVertical: 6}}
          buttonStyle={{paddingVertical: 6, paddingHorizontal:16, borderColor: ACCENT_COLOR, borderRadius:0}}
          titleStyle={{color:ACCENT_COLOR}}
          title="OK" />
        </View>
      </Modal>
    </>
  );
}

const styles = StyleSheet.create({
  a: {
    fontWeight: '300',
    color: '#FF3366', // make links coloured pink
  },
});

function areEqual(prevProps: ModalBaseProps, nextProps: ModalBaseProps) {
  return prevProps.isVisible == nextProps.isVisible;
}
export default React.memo(HTMLModalComponent, areEqual)