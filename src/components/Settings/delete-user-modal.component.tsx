
import Modal from 'react-native-modal';
import { View } from 'react-native';
import React, { useState } from 'react';
import { Text, Button } from 'react-native-elements';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../../assets/data/appData';
import theme, { PRIMARY_COLOR } from '../../assets/data/theme';
import { translate } from '../../services/i18n.service';
import { ModalBaseProps } from '../../models/modalBaseProps.model';
import * as ParseServer from '../../services/parse/parse-server.service';
import Toast from 'react-native-simple-toast';
import { useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

type Props = ModalBaseProps &
{
    onDeleteUser: () => void
}
function DeleteUserModalComponent(props: Props) {

    const [isLoading, setIsLoading] = useState(false);
    const dispatch = useDispatch();
    const navigation = useNavigation();

    function _deleteUser() {
        setIsLoading(true)
        ParseServer.deleteUser()
            .then(() => {
                setIsLoading(false);
                props.onDeleteUser();
            })
            .catch(error => {
                setIsLoading(false);
                if (error.code == 209) { // 209 is InvalidSessionToken
                    ParseServer.logout()
                        .catch(error => console.log(error.message))
                        .finally(() => {
                            dispatch({ type: 'TOGGLE_CONNECTED' })
                            Toast.show(translate("403"), Toast.LONG)
                            navigation.navigate("Signing", { goBack: true })
                        })
                }
                else {
                    Toast.show(error.message, Toast.LONG)
                }
            })
    }

    return (
        <Modal isVisible={props.isVisible}
            deviceWidth={SCREEN_WIDTH}
            deviceHeight={SCREEN_HEIGHT}
            hideModalContentWhileAnimating={true}
            onBackdropPress={props.onHide}
            onBackButtonPress={props.onHide}>
            <View style={theme.modal}>
                <Text h4>{translate("deleteTitle")}?</Text>
                <Text >{translate("deleteText")}</Text>
                <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center' }}>
                    <Button
                        containerStyle={theme.modalButtonContainer}
                        buttonStyle={{ borderColor: PRIMARY_COLOR, borderRadius: 0 }}
                        titleStyle={{ color: PRIMARY_COLOR }}
                        onPress={props.onHide}
                        title={translate("deleteWait")}
                        type="clear"

                    />
                    <Button
                        containerStyle={theme.modalButtonContainer}
                        onPress={_deleteUser}
                        loading={isLoading}
                        title={translate("deleteConfirm")}
                        buttonStyle={{ backgroundColor: 'tomato', borderRadius: 0 }}
                    />
                </View>
            </View>
        </Modal>
    );
}

function areEqual(prevProps: ModalBaseProps, nextProps: ModalBaseProps) {
    return prevProps.isVisible == nextProps.isVisible;
  }
export default React.memo(DeleteUserModalComponent, areEqual)