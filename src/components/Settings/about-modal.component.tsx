import React from 'react';
import Modal from 'react-native-modal';
import { StyleSheet, View, ScrollView, Image } from 'react-native';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../../assets/data/appData';
import theme, { ACCENT_COLOR } from '../../assets/data/theme';
import { Text, Button } from 'react-native-elements';
import { translate } from '../../services/i18n.service';
import { ModalBaseProps } from '../../models/modalBaseProps.model';



function AboutModalComponent(props:ModalBaseProps) {

  return (
    <Modal isVisible={props.isVisible}
      deviceWidth={SCREEN_WIDTH}
      deviceHeight={SCREEN_HEIGHT}
      hideModalContentWhileAnimating={true}
      onBackButtonPress={props.onHide}
      onBackdropPress={props.onHide}
    >
      <View style={theme.modal}>
        <ScrollView style={{ maxHeight: '90%' }} contentContainerStyle={{ alignItems: "center", justifyContent: 'space-evenly' }}>
          <Image
            style={styles.logo}
            resizeMode='contain'
            source={require('../../assets/img/logo.png')}
          />
          <Text style={{textAlign:"center"}}>{translate("aboutMarkit")}</Text>
        </ScrollView>
        <Button
          type="outline"
          onPress={props.onHide}
          containerStyle={{ paddingVertical: 6 }}
          buttonStyle={{ paddingVertical: 6, paddingHorizontal: 16, borderColor: ACCENT_COLOR, borderRadius: 0 }}
          titleStyle={{ color: ACCENT_COLOR }}
          title="OK" />
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  a: {
    fontWeight: '300',
    color: ACCENT_COLOR, // make links coloured pink
  },
  logo: {
    maxWidth: 128,
    maxHeight: 128
  },
});

function areEqual(prevProps: ModalBaseProps, nextProps: ModalBaseProps) {
  return prevProps.isVisible == nextProps.isVisible;
}
export default React.memo(AboutModalComponent, areEqual)