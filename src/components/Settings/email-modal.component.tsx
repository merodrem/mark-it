
import Modal from 'react-native-modal';
import { View, Keyboard } from 'react-native';
import React, { useRef, useState } from 'react';
import { Input, Text, Button } from 'react-native-elements';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../../assets/data/appData';
import theme, { PRIMARY_COLOR } from '../../assets/data/theme';
import { validateEmail } from '../../services/email.service';
import { ModalBaseProps } from '../../models/modalBaseProps.model';
import { translate } from '../../services/i18n.service';
import * as ParseServer from '../../services/parse/parse-server.service';
import Toast from 'react-native-simple-toast';
import { useDispatch } from 'react-redux';



function EmailModalComponent(props: ModalBaseProps) {

    const emailEntry = useRef("")
    const [isEmailValid, setIsEmailValid] = useState(true);
    const [isLoading, setIsLoading] = useState(false);
    const dispatch = useDispatch()

    function _submitEmail() {
        if (validateEmail(emailEntry.current)) {
            _changeEmail(emailEntry.current)
        }
        else {
            setIsEmailValid(false)
        }
    }

    function _changeEmail(email: string) {
        setIsLoading(true);
        ParseServer.changeEmail(email)
            .then((response) => {
                props.onHide()
                Toast.show(translate("verifLink") + email, Toast.LONG)
                dispatch({ type: 'UPDATE_USER', payload: { email: email } })
            })
            .catch(error => {
                if (error.code == 209) { // 209 is InvalidSessionToken
                    Toast.show(translate("403"), Toast.LONG)
                    ParseServer.logout()
                        .catch(error => console.log(error.message))
                        .finally(() => {
                            dispatch({ type: 'TOGGLE_CONNECTED' })
                        })
                }
                else {
                    switch (error.code) {
                        case 203:
                            Toast.show(translate("emailExists"), Toast.LONG)
                            break;
                        default:
                            Toast.show(error.message, Toast.LONG)
                            break;
                    }

                }
            })
            .finally(() => {
                Keyboard.dismiss()
                setIsLoading(false)
            })
    }


    return (
        <Modal isVisible={props.isVisible}
            deviceWidth={SCREEN_WIDTH}
            deviceHeight={SCREEN_HEIGHT}
            hideModalContentWhileAnimating={true}
            onBackdropPress={props.onHide}
            onBackButtonPress={props.onHide}>
            <View style={theme.modal}>
                <Input
                    keyboardAppearance="light"
                    autoCorrect={false}
                    keyboardType="email-address"
                    returnKeyType={'done'}
                    containerStyle={{
                        marginTop: 16,
                        borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                        width: '95%'
                    }}
                    autoCapitalize="none"
                    inputStyle={{ marginLeft: 10 }}
                    placeholder={translate("newEmail")}
                    onChangeText={(text) => emailEntry.current = text}
                    errorMessage={
                        isEmailValid ? undefined : "invalid email format"
                    }
                />
                <Text>{translate("changeEmail")}</Text>
                <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center' }}>
                    <Button
                        containerStyle={theme.modalButtonContainer}
                        onPress={props.onHide}
                        buttonStyle={{ borderColor: PRIMARY_COLOR, borderRadius: 0 }}
                        titleStyle={{ color: PRIMARY_COLOR }}
                        title="wait, no!"
                        type='outline'
                    />
                    <Button
                        containerStyle={theme.modalButtonContainer}
                        onPress={() => _submitEmail()}
                        title="Yes, change it!"
                        loading={isLoading}
                        buttonStyle={{ backgroundColor: 'tomato', borderRadius: 0 }}
                    />
                </View>
            </View>
        </Modal>
    );
}

function areEqual(prevProps: ModalBaseProps, nextProps: ModalBaseProps) {
    return prevProps.isVisible == nextProps.isVisible;
}
export default React.memo(EmailModalComponent, areEqual)
