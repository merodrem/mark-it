import React, { useState } from 'react';
import { StyleSheet, View, Linking, TouchableOpacity, Keyboard } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'
import { Icon, Text, Button, Divider, ListItem } from 'react-native-elements';
import * as ParseServer from '../../services/parse/parse-server.service';
import Toast from 'react-native-simple-toast';
import { ACCENT_COLOR, PRIMARY_COLOR, TERNARY_COLOR } from '../../assets/data/theme';
import DeleteUserModalComponent from './delete-user-modal.component';
import { sendEmail } from '../../services/email.service';
import EmailModalComponent from './email-modal.component';
import { translate } from '../../services/i18n.service';
import { PRIVACY } from '../../assets/lang/privacy_en';
import { TERMS } from '../../assets/lang/terms_en';
import HTMLModalComponent from '../Util/HTML-modal.component';
import AboutModalComponent from './about-modal.component';
import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { UserState } from '../../store/Reducers/user/user.model';
import { ScrollView } from 'react-native-gesture-handler';



const linkList = Object.freeze([
    {
        name: "about",
        label: "settingsAbout",
        link: "about"
    },
    {
        name: "terms",
        label: "settingsTerms",
        link: ""
    },
    {
        name: "privacy",
        label: "settingsPrivacy",
        link: ""
    },
    {
        name: "license",
        label: "settingsLicense",
        link: ""
    },
] as const);

function SettingsComponent() {
    const [aboutModalVisible, setAboutModalVisible] = useState(false);
    const [deleteModalVisible, setDeleteModalVisible] = useState(false);
    const [emailModalVisible, setEmailModalVisible] = useState(false);
    const [privacyModalVisible, setPrivacyModalVisible] = useState(false);
    const [termsModalVisible, setTermsModalVisible] = useState(false);
    const [isLogoutLoading, setLogoutLoading] = useState(false);
    const dispatch = useDispatch();
    const navigation: StackNavigationProp<any> = useNavigation();
    const user: UserState = useSelector(state => state.userReducer);

    function _logout() {
        setLogoutLoading(true);
        ParseServer.logout()
            .then(() => {
                setLogoutLoading(false)
                dispatch({ type: 'TOGGLE_CONNECTED' })
                navigation.push("Signing")
            })
            .catch(error => {
                setLogoutLoading(false)
                Toast.show(error.message, Toast.LONG)
            })
    }


    function _followLink(linkItem) {
        switch (linkItem.name) {
            case "about":
                setAboutModalVisible(true);
                break;
            case "terms":
                setTermsModalVisible(true);
                break;
            case "privacy":
                setPrivacyModalVisible(true);
                break;
            case "license":
                Linking.openURL("https://www.gnu.org/licenses/gpl-3.0.en.html").catch(err => console.log('An error occurred', err))
                break;
            default:
                break;
        }

    }

    function _changePassword() {
        if (user.email) {
            ParseServer.changePassword(user.email)
                .then((response) => Toast.show(translate("resetPasswordEmail"), Toast.LONG))
                .catch(error => {
                    if (error.code == 209) { // 209 is InvalidSessionToken
                        ParseServer.logout()
                            .catch(error => console.log(error.message))
                            .finally(() => {
                                dispatch({ type: 'TOGGLE_CONNECTED' })
                            })
                    }
                    else {
                        Toast.show(error.message, Toast.LONG)
                    }
                })
        }
        else {
            Toast.show(translate("403"), Toast.LONG)
        }
    }


    return (
        <ScrollView style={{ flex: 1, backgroundColor: TERNARY_COLOR }}>

            <AboutModalComponent
                onHide={() => setAboutModalVisible(false)}
                isVisible={aboutModalVisible}
            />
            <HTMLModalComponent
                html={TERMS}
                onHide={() => setTermsModalVisible(false)}
                isVisible={termsModalVisible}
            />
            <HTMLModalComponent
                html={PRIVACY}
                onHide={() => setPrivacyModalVisible(false)}
                isVisible={privacyModalVisible}
            />
            {user.logged &&
                <>
                    <EmailModalComponent
                        onHide={() => setEmailModalVisible(false)}
                        isVisible={emailModalVisible}

                    />
                    <DeleteUserModalComponent
                        onHide={() => setDeleteModalVisible(false)}
                        onDeleteUser={() => {
                            setDeleteModalVisible(false);
                            dispatch({ type: 'TOGGLE_CONNECTED' })
                            navigation.navigate("Signing")
                        }}
                        isVisible={deleteModalVisible}
                    />
                    <Text h3 style={styles.titleStyle}>{translate("settingsUserTitle")}</Text>
                    <Divider />
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => setEmailModalVisible(true)}>
                        <ListItem
                            key={"email"}
                            containerStyle={{ paddingVertical: 10 }}
                            title={translate("settingsUserEmail") + " (" + user.email + ")"}
                            bottomDivider
                            chevron
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => _changePassword()}>
                        <ListItem
                            key={"password"}
                            containerStyle={{ paddingVertical: 10 }}
                            title={translate("settingsUserPassowrd")}
                            bottomDivider
                            chevron
                        />
                    </TouchableOpacity>
                </>
            }
            <View style={styles.moreSection}>
                <Button
                    containerStyle={styles.buttonContainer}
                    buttonStyle={styles.button}
                    title={translate("settingsCode")}
                    icon={
                        <Icon
                            name="gitlab"
                            size={25}
                            type='antdesign'
                            color="black"
                            containerStyle={{ marginRight: 8 }}
                        />
                    }
                    onPress={() => Linking.openURL("https://framagit.org/merodrem/mark-it").catch(err => console.log('An error occurred', err))}
                />
                <Button
                    containerStyle={styles.buttonContainer}
                    buttonStyle={styles.button}
                    title={translate("contact")}
                    icon={
                        <Icon
                            name="mail"
                            size={25}
                            type='entypo'
                            color="black"
                            containerStyle={{ marginRight: 8 }}
                        />
                    }
                    onPress={() => sendEmail(
                        'info@nomadic-dev.com',
                        "[mark'IT]",
                        ''
                    )}
                />
            </View>

            <Text h3 style={styles.titleStyle}>{translate("settingsMoreTitle")}</Text>
            <Divider />
            {
                linkList.map((item) => (
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => _followLink(item)}
                        key={item.name}
                    >
                        <ListItem
                            title={translate(item.label)}
                            containerStyle={{ paddingVertical: 10 }}
                            bottomDivider
                            chevron
                        />
                    </TouchableOpacity>

                ))
            }

            <Text style={{ color: 'grey', textAlign: "center", margin: 4 }}>{translate('follow')}:</Text>
            <TouchableOpacity
                style={{ padding: 8 }}
                onPress={() => Linking.openURL("https://www.facebook.com/markit.food/")
                    .catch(error =>
                        Toast.show(error.message, Toast.LONG)
                    )
                }
            >
                <Icon
                    name="facebook"
                    type="material-community"
                />
            </TouchableOpacity>
            <Divider />
            {user.logged &&
                <View style={styles.userSettings}>
                    < Button
                        containerStyle={styles.buttonContainer}
                        buttonStyle={[styles.button, { backgroundColor: 'white', borderColor: 'black' }]}
                        titleStyle={{ color: 'black' }}
                        title={translate("settingsLogOut")}
                        onPress={() => _logout()}
                        loadingProps={{color:'black'}}
                        loading={isLogoutLoading}
                    />
                    <Button
                        containerStyle={styles.buttonContainer}
                        buttonStyle={[styles.button, { backgroundColor: 'white', borderColor: 'tomato' }]}
                        title={translate("settingsDeleteAccount")}
                        titleStyle={{ color: 'tomato' }}
                        onPress={() => setDeleteModalVisible(true)}
                    />
                </View>
            }
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    moreSection: {
        alignItems: 'center',
        marginTop: 8,
    },
    userSettings: {
        flexDirection: "row",
        justifyContent: "center"
    },
    titleStyle: {
        textAlign: "center",
        marginTop: 16,
    },
    buttonContainer: {
        paddingHorizontal: 4,
        paddingVertical: 4,
        borderRadius: 0,
        width: "100%",
        flex: 1
    },
    button: {
        elevation: 1,
        backgroundColor: ACCENT_COLOR,
        borderRadius: 0,
    },
});


export default SettingsComponent

