import React from 'react';
import { Icon, Text } from 'react-native-elements';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import WelcomeComponent from '../welcome.component';
import SigningComponent from '../Signing/signing.component';
import UserComponent from '../User/user.component';
import MapComponent from '../Markets/Map/map.component';
import SettingsComponent from '../Settings/settings.component';
import { PRIMARY_COLOR, ACCENT_COLOR } from '../../assets/data/theme';
import MarketDashboardComponent from '../Markets/market-dashboard.component';
import AddMarketComponent from '../Markets/add-market.component';
import { useNetInfo } from '@react-native-community/netinfo';
import { View } from 'react-native';
import { useSelector } from 'react-redux';
import ResetPasswordComponent from '../Signing/reset-password.component';
import { translate } from '../../services/i18n.service';



const TabNavigator = createMaterialTopTabNavigator()

const TabScreen = () => {
    return (
        <TabNavigator.Navigator
            tabBarPosition={"bottom"}
            swipeEnabled={false}
            lazy={false}
            initialRouteName={"Map"}
            sceneContainerStyle={
                {
                    backgroundColor: 'white'
                }
            }
            tabBarOptions={{
                showIcon: true,
                showLabel: false,
                activeTintColor: 'white',
                inactiveTintColor: 'black',
                labelStyle: { fontSize: 10 },
                tabStyle: {
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                },
                style: {
                    backgroundColor: PRIMARY_COLOR,
                },
                indicatorStyle: {
                    backgroundColor: ACCENT_COLOR
                }
            }}>
            <TabNavigator.Screen
                options={{
                    tabBarLabel: "",
                    tabBarIcon: ({ focused, color }) =>
                        <Icon
                            name='map'
                            type='font-awesome'
                            color={color}
                        />
                }}
                name="Map" component={MapComponent} />
            <TabNavigator.Screen
                options={{
                    tabBarLabel: "",
                    tabBarIcon: ({ focused, color }) =>
                        <Icon
                            name='ios-settings'
                            type='ionicon'
                            color={color}
                        />
                }}
                name="Settings" component={SettingsComponent} />
            <TabNavigator.Screen
                options={{
                    tabBarLabel: "",
                    tabBarIcon: ({ focused, color }) =>
                        <Icon
                            name='user'
                            type='font-awesome'
                            color={color}
                        />
                }}
                name="User" component={UserComponent} />
        </TabNavigator.Navigator>
    )
}

const AppStackNavigator = createStackNavigator();

function AppStack() {
    return (
        <AppStackNavigator.Navigator
            initialRouteName="Tabs"
            headerMode="none">
            <AppStackNavigator.Screen name="Tabs" component={TabScreen} />
            <AppStackNavigator.Screen name="MarketDashboard" component={MarketDashboardComponent} />
            <AppStackNavigator.Screen name="AddMarket" component={AddMarketComponent} />
        </AppStackNavigator.Navigator>
    );
}

const MainNavigator = createStackNavigator();

export default function Navigation() {
    const netInfo = useNetInfo();
    const firstLaunch: boolean = useSelector(state => state.globalSettingsReducer.firstLaunch);

    return (
        <>
            {netInfo.isConnected || <View style={{ backgroundColor: 'tomato', alignItems: 'center', paddingVertical: 2 }}><Text>{translate("noInternet")}</Text></View>}
            <NavigationContainer>
                <MainNavigator.Navigator
                    headerMode="none"
                    initialRouteName={firstLaunch ? "Welcome" : "App"}
                >
                    <MainNavigator.Screen name="Welcome" component={WelcomeComponent} />
                    <MainNavigator.Screen name="App" component={AppStack} />
                    <MainNavigator.Screen name="ResetPassword" component={ResetPasswordComponent} />
                    <MainNavigator.Screen name="Signing" component={SigningComponent} />
                </MainNavigator.Navigator>
            </NavigationContainer>
        </>
    );
}
