import React, { useState } from 'react'
import Toast from 'react-native-simple-toast';
import ImagePicker from 'react-native-image-picker'
import { StyleSheet, Image, TouchableOpacity, ActivityIndicator } from 'react-native'
import { connect, useDispatch, useSelector } from 'react-redux'
import * as ParseServer from '../../services/parse/parse-server.service';
import { ACCENT_COLOR } from '../../assets/data/theme';
import { translate } from '../../services/i18n.service';
import { useNavigation } from '@react-navigation/native';
import { Icon } from 'react-native-elements';

function Avatar() {

    const [isImageLoading, setIsImageLoading] = useState(false);
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const avatarBase64: string | undefined = useSelector(state => state.userReducer.avatar)

    function _avatarClicked() {
        const options = {
            maxWidth: 400,
            maxHeight: 400,
            quality: 0.8,
        };
        ImagePicker.launchImageLibrary(options, (response) => {
            if (response.error) {
                Toast.show(response.error, Toast.LONG)
            }
            else if (!response.didCancel) {
                setIsImageLoading(true)
                if (response.fileSize < 1000000) { // compressed file smaller than 1 Mo
                    ParseServer.changeAvatar({ filename: response.fileName, base64: response.data })
                        .then(() => {
                            ("update")
                            const action = { type: "UPDATE_USER", payload: { avatar: response.data } }
                            dispatch(action)
                        })
                        .catch(error => {
                            if (error.code == 209) { // 209 is InvalidSessionToken
                                ParseServer.logout()
                                    .catch(error => Toast.show(error.message, Toast.LONG))
                                    .finally(() => {
                                        dispatch({ type: 'TOGGLE_CONNECTED' })
                                        Toast.show(translate("403"), Toast.LONG)
                                        navigation.navigate("Signing", { goBack: true })
                                    })
                            }
                            else {
                                Toast.show(error.message, Toast.LONG)
                            }
                        })
                        .finally(() => {
                            setIsImageLoading(false);
                        })

                }
                else {
                    setIsImageLoading(false);
                    Toast.show(translate("imageSizeError"), Toast.LONG)
                }
            }
        })
    }

    return (
        <TouchableOpacity
            style={styles.touchableOpacity}
            onPress={_avatarClicked}>
            {isImageLoading ?
                <ActivityIndicator color={ACCENT_COLOR} size={50} />
                :
                <>
                    <Image
                        style={styles.avatar} source={avatarBase64 ? { uri: `data:image;base64,${avatarBase64}` } : require('../../assets/img/default_avatar.png')} />
                    <Icon
                        containerStyle={{ position: "absolute", bottom: 0, right:0, zIndex: 10 }}
                        name="camera"
                        reverse
                        color="grey"
                        size={10}
                        type="entypo"
                    />
                </>
            }
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    touchableOpacity: {
        margin: 5,
        width: 100,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatar: {
        width: 100,
        height: 100,
        borderRadius: 50,
        borderColor: '#9B9B9B',
        borderWidth: 2
    }
})

export default React.memo(Avatar)
