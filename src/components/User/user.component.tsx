import React, { useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { StyleSheet, View, TouchableOpacity, ScrollView, ActivityIndicator } from 'react-native';
import { Button, Icon, Text, Card, Divider } from 'react-native-elements'
import { SCREEN_WIDTH } from '../../assets/data/appData';
import { ACCENT_COLOR, TERNARY_COLOR, PRIMARY_COLOR } from '../../assets/data/theme';
import Avatar from './avatar.component';
import * as ParseServer from '../../services/parse/parse-server.service';
import { MARKET_TYPE } from '../../assets/data/marketType';
import ProductBadgesComponent from '../Markets/product-badges.component';
import DisplayScheduleComponent from '../Markets/display-schedule.component';
import { stringLocation } from '../../services/market.service';
import { translate } from '../../services/i18n.service';
import RatingWithCommentComponent from '../Markets/rating-with-comment.component';
import { MarketFeature } from '../../models/vote.model';
import { UserState } from '../../store/Reducers/user/user.model';
import { useNavigation, useRoute } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';



function formatVote(votedValue: string, target: MarketFeature) {
    switch (target) {
        case "marketName":
            return <Text style={{ textAlign: "center" }}>{votedValue}</Text>;
        case "marketType":
            return <Text style={{ textAlign: "center" }}>{translate(JSON.parse(votedValue))}</Text>;
        case "productTypes":
            return <ProductBadgesComponent productTypes={JSON.parse(votedValue)} />;
        case "schedule":
            return <DisplayScheduleComponent schedule={JSON.parse(votedValue)} />;
        case "address":
            return <Text style={{ textAlign: "center" }}>{stringLocation(JSON.parse(votedValue))}</Text>;
        default:
            return <Text style={{ textAlign: "center" }}>{votedValue}</Text>;
    }
}

function formatTarget(target: MarketFeature) {
    switch (target) {
        case "marketName":
            return translate("userSuggestedName");
        case "marketType":
            return translate("userSuggestedType");
        case "productTypes":
            return translate("userSuggestedProducts");
        case "schedule":
            return translate("userSuggestedSchedule");
        case "address":
            return translate("userSuggestedAddress");
        default:
            return target;
    }
}

function useIsMountedRef() {
    const isMountedRef = useRef(null);
    useEffect(() => {
        isMountedRef.current = true;
        return () => isMountedRef.current = false;
    });
    return isMountedRef;
}

function UserComponent() {
    const dispatch = useDispatch();
    const isMountedRef = useIsMountedRef();
    const user: UserState = useSelector(state => state.userReducer);
    const { addedMarkets, votesFromUser, ratingsFromUser, username, logged } = user;
    useEffect(
        () => {
            setIsloading(true)
            if (logged) {
                ParseServer.getAddedMarketsRatingsAndVotes()
                    .then(([addedMarkets, ratingsFromUser, votesFromUser]) => {
                        if (isMountedRef.current)
                            dispatch({
                                type: 'UPDATE_USER',
                                payload: {
                                    addedMarkets: addedMarkets,
                                    ratingsFromUser: ratingsFromUser,
                                    votesFromUser: votesFromUser,
                                }
                            })
                    }
                    )
                    .finally(() => setIsloading(false))
            }
        }, [])


    const [isLoading, setIsloading] = useState(true)
    const navigation: StackNavigationProp<any> = useNavigation();
    const route = useRoute()

    return (
        (logged) ? (
            <ScrollView
                keyboardShouldPersistTaps='handled'
                style={styles.mainContainerConnected}>
                <View style={styles.header}>
                    <Avatar />
                    <Text h1>{username}</Text>
                </View>
                <Text h2 style={styles.titleStyle}>{translate("userAddedMarkets")}</Text>
                <Divider />
                {isLoading ?
                    <ActivityIndicator color={ACCENT_COLOR} size={35} />
                    :
                    addedMarkets.length > 0 ? addedMarkets.map(market =>
                        <TouchableOpacity
                            activeOpacity={0.5}
                            key={market.objectId}
                            onPress={() => navigation.push("MarketDashboard", { ...market })}>
                            <Card
                                image={MARKET_TYPE[market.marketType]?.image ?? require('../../assets/img/logo.png')}
                                imageProps={{ resizeMode: 'cover' }}
                                imageWrapperStyle={styles.cardImageContainer}
                            >
                                <Text h3>{market.name}</Text>
                                <Text>{market.location.city}, {market.location.country}</Text>
                            </Card>
                        </TouchableOpacity>
                    )
                        :
                        <Text style={{paddingLeft:4}}>{translate("userNoMarket")}</Text>

                }
                <Text h2 style={styles.titleStyle}>{translate("userRatings")}</Text>
                <Divider />

                {isLoading ?
                    <ActivityIndicator color={ACCENT_COLOR} size={35} />
                    :
                    ratingsFromUser.length > 0 ? ratingsFromUser.map(rating =>
                        <TouchableOpacity
                            activeOpacity={0.5}
                            key={rating.objectId}
                            onPress={() => navigation.push("MarketDashboard", { ...rating.market })}>
                            <Card>
                                <View style={{ alignItems: "center", justifyContent: "center" }}>
                                    <Text h4>{rating.market.name}</Text>
                                    <Text>{rating.market.location.city}, {rating.market.location.country}</Text>
                                </View>
                                <Divider />
                                <RatingWithCommentComponent
                                    rating={rating}
                                />

                            </Card>
                        </TouchableOpacity>
                    )
                        :
                        <Text style={{paddingLeft:4}}>{translate("userNoRating")}</Text>

                }
                <Text h2 style={styles.titleStyle}>{translate("userContributions")}</Text>
                <Divider />
                {isLoading ?
                    <ActivityIndicator color={ACCENT_COLOR} size={35} />
                    :
                    votesFromUser.length > 0 ? votesFromUser.map(vote =>
                        <TouchableOpacity
                            activeOpacity={0.5}
                            key={vote.objectId}
                            onPress={() => navigation.push("MarketDashboard", { ...vote.market })}>
                            <Card
                                containerStyle={{ marginBottom: 8 }}>
                                <View style={{ alignItems: "center", justifyContent: "center" }}>
                                    <Text h4>{vote.market.name}</Text>
                                    <Text>{vote.market.location.city}, {vote.market.location.country}</Text>
                                </View>
                                <Divider />
                                <Text style={{ fontWeight: "bold" }}>{formatTarget(vote.target)}:</Text>
                                {formatVote(vote.votedValue, vote.target)}
                            </Card>
                        </TouchableOpacity>
                    )
                        :
                        <Text style={{paddingLeft:4}}>{translate("userNoVote")}</Text>

                }
            </ScrollView>
        ) : (
                <View style={styles.mainContainer}>
                    <Icon
                        name='user-times'
                        type='font-awesome'
                        color='#F4F4F4'
                        containerStyle={styles.iconContainer}
                        size={SCREEN_WIDTH * 0.8}
                    />
                    <Text h3 style={styles.text}>{translate("userTooLazy")}</Text>
                    <Icon
                        name='emoji-flirt'
                        type='entypo'
                    />
                    <Button
                        containerStyle={styles.buttonContainer}
                        buttonStyle={styles.button}
                        title={translate('login') + " !"}
                        onPress={() => navigation.push("Signing", { redirectRoute: route.name })}
                    />
                </View>
            )
    );
}

const styles = StyleSheet.create({
    mainContainerConnected: {
        flex: 1,
        backgroundColor: TERNARY_COLOR
    },
    header: {
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15,
        backgroundColor: PRIMARY_COLOR,
        marginBottom: 10
    },
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ratingText: {
        fontStyle: "italic",
        textAlign: "center"
    },
    iconContainer: {
        zIndex: -1,
        position: 'absolute',
    },
    cardImageContainer: {
        justifyContent: 'center',

    },
    titleStyle: {
        textAlign: "center",
        marginTop: 16,
    },
    text: {
        textAlign: 'center',
    },
    buttonContainer: {
        padding: 16,
        width: '80%'
    },
    button: {
        backgroundColor: ACCENT_COLOR,
        borderRadius: 0,
        elevation: 1,
    },
    
});



export default UserComponent

