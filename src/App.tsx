// App.js

import React, { useEffect} from 'react'
import { Provider } from 'react-redux'
import Store from './store/configureStore'
import { persistStore } from 'redux-persist'
import { PersistGate } from 'redux-persist/es/integration/react'
import Navigation from './components/Navigation/navigation.component'
import * as RNLocalize from "react-native-localize"
import { setI18nConfig } from './services/i18n.service'
import MapboxGL from "@react-native-mapbox-gl/maps";
import { MAPBOX_TOKEN } from './assets/data/conf'

MapboxGL.setAccessToken(MAPBOX_TOKEN);


export default function App() {
  useEffect(() => {
    RNLocalize.addEventListener("change", _handleLocalizationChange);
    
    return () => {
      RNLocalize.removeEventListener("change", _handleLocalizationChange);
    }
  }, [])
  setI18nConfig(); 

  const _handleLocalizationChange = () => {
    setI18nConfig();
  };

  const persistor = persistStore(Store)
  return (
    <Provider store={Store} >
      <PersistGate persistor={persistor}>
        <Navigation />
      </PersistGate>
    </Provider>
  );
}