import { UserState } from "./user.model"

export const TOGGLE_CONNECTED = 'TOGGLE_CONNECTED'
export const UPDATE_USER = 'UPDATE_USER'

export class ToggleConnectedAction {
    readonly type: string = TOGGLE_CONNECTED
    constructor(public payload: UserState) { }
}

export class ToggleUpdateUserAction {
    readonly type: string = UPDATE_USER
    constructor(public payload: UserState) { }
}


export type UserAction = ToggleConnectedAction | ToggleUpdateUserAction