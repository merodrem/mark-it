import { Market } from "../../../models/market.model";
import { RatingModel } from "../../../models/rating.model";
import { Vote } from "../../../models/vote.model";

export interface UserState {
    ID : string|number|undefined,
    logged: boolean;
    addedMarkets: Array<Market>,
    votesFromUser: Array<Vote>,
    ratingsFromUser: Array<RatingModel>,
    username: string|undefined,
    email: string|undefined,
    emailVerified: boolean,
    avatar: string|undefined,//base64 encoded image
}