import { UserAction } from "./user.action"
import { UserState } from "./user.model"

const initState: UserState = {
    ID: undefined,
    logged: false,
    addedMarkets: [],
    votesFromUser: [],
    ratingsFromUser: [],
    username: undefined,
    email: undefined,
    emailVerified: false,
    avatar: undefined
}

function userReducer(state = initState, action: UserAction) {
    switch (action.type) {
        case 'TOGGLE_CONNECTED':
            const payload = action.payload ? action.payload : initState
            return { ...state, logged: !state.logged, ...payload } || state
        case 'UPDATE_USER':
            return { ...state, ...action.payload } || state
        default:
            return state
    }
}

export default userReducer