import { GlobalSettingsState } from "./globalSettings.model"
import { Action } from "./globalSettings.actions"

const initState: GlobalSettingsState = {
    firstLaunch: true
}

function globalSettingsReducer(state = initState, action: Action) {
    switch (action.type) {
        case 'APP_OPENED':
            const nextState = {...state, firstLaunch: false}
            return nextState || state
        default:
            return state
    }
}

export default globalSettingsReducer