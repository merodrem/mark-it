import { GlobalSettingsState } from "./globalSettings.model"

export const APP_OPENED = 'APP_OPENED'

export class ToggleLoginAction { 
    readonly type: string = APP_OPENED;
} 


export type Action = ToggleLoginAction