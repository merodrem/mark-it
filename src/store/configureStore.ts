import { createStore } from 'redux';
import userReducer from './Reducers/user/user.reducer';
import globalSettingsReducer from './Reducers/globalSettings/globalSettings.reducer'
import { persistCombineReducers } from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage';


const rootPersistConfig = {
  key: 'root',
  storage: AsyncStorage
}

const store = createStore(persistCombineReducers(rootPersistConfig, { userReducer, globalSettingsReducer}))
export default store;