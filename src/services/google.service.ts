import { GOOGLE_API_KEY } from "../assets/data/conf";

const GEOCODING_URL = 'https://maps.googleapis.com/maps/api/geocode/json'
export async function geocode(stringAddress: string) {
    let result = {
        lat: 0,
        lng: 0,
    }
    await fetch(GEOCODING_URL + "?address=" + stringAddress.replace(/ /g, "+") + "&key=" + GOOGLE_API_KEY)
        .then(results => results.json())
        .then(addresses => {
            if (addresses.results.length > 0) {
                result = addresses.results[0].geometry.location
            }
            else {
                return Promise.reject(new Error(addresses.status));
            }
        }
        );
    return result;
}

