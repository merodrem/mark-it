import { format } from 'date-fns'
import { enGB, fr } from 'date-fns/locale'
import { getCurrentLanguage } from './i18n.service';

export function toISODate(date: Date) {
    return date.getFullYear() + '-' +
        ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
        ('0' + date.getDate()).slice(-2);
}

const locales = { enGB, fr }


export function formatDate(date: Date) {
    return format(date, 'd MMM', { locale: locales[getCurrentLanguage()] });
}
export function formatTimestamp(date: Date) {
    return format(date, 'd MMM yyyy, HH:mm', { locale: locales[getCurrentLanguage()] });
}
export function formatTime(stringDate: string) {
    const date = new Date(stringDate);
    const hour = date.getUTCHours();
    const minute = date.getUTCMinutes();

    return (hour < 10 ? '0' + hour : hour) + ':' + (minute < 10 ? '0' + minute : minute);
}
/**
 * Force a date to UTC timezone. Eg: 06:00GMT+2 becomes 06:00z 
 * @param date 
 */
export function forceToUTC(date: Date): string {

    let current_hrs = date.getHours(),
        current_mins = date.getMinutes(),
        current_secs = date.getSeconds(),
        current_datetime;

    current_hrs = current_hrs < 10 ? '0' + current_hrs : current_hrs;
    current_mins = current_mins < 10 ? '0' + current_mins : current_mins;
    current_secs = current_secs < 10 ? '0' + current_secs : current_secs;

    current_datetime =  '1970-01-01T' + current_hrs + ':' + current_mins + ':' + current_secs;
    return (current_datetime + 'Z');
}