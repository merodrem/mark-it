import { DAYS } from "../assets/data/day";
import { toISODate } from "./date.service";
import cloneDeep from "lodash/cloneDeep"
import { Schedule, Day, Slot } from "../models/schedule.model";

const INIT_DAY: Day = {
    slots: [{ start: undefined, end: undefined }],
    open: false,
}

// function compareTime(timeA: Date, timeB: Date) {
//     return (currentHour > start.getUTCHours() || (currentHour === start.getUTCHours() && currentMinutes >= start.getMinutes()))
//         && (currentHour < end.getUTCHours() || (currentHour === end.getUTCHours() && currentMinutes <= end.getMinutes()));

// }

export function initSchedule(mon = cloneDeep(INIT_DAY), tue = cloneDeep(INIT_DAY), wed = cloneDeep(INIT_DAY), thu = cloneDeep(INIT_DAY), fri = cloneDeep(INIT_DAY), sat = cloneDeep(INIT_DAY), sun = cloneDeep(INIT_DAY), startingDay = toISODate(new Date(new Date().getFullYear(), 0, 1)), endingDay = toISODate(new Date(new Date().getFullYear(), 11, 31))): Schedule {

    return {
        monday: { ...mon },
        tuesday: { ...tue },
        wednesday: { ...wed },
        thursday: { ...thu },
        friday: { ...fri },
        saturday: { ...sat },
        sunday: { ...sun },
        openingPeriods: [{ start: startingDay, end: endingDay }]
    }
}

export function autocompleteSchedule(schedule: Schedule, start: string, end: string, depth: number) {

    DAYS.forEach((day) => {
        if (!schedule[day].open && schedule[day].slots.length == depth - 1) {
            schedule[day].slots.push({ start: start, end: end })
        }
    })
    return schedule;
}


const validateHours = (function (slots: Array<Slot>) {
    for (let index = 0; index < slots.length; index++) {
        if (
            !slots[index].start
            || !slots[index].end
            || (index > 0 && slots[index].start < slots[index - 1].end)
            || (index < (slots.length - 1) && slots[index].end < slots[index].start)//the last slot can cross midnight, so start > end possible
        ) {
            return false;
        }
    }
    return true;
}
)
const validatePeriods = (function (periods: Array<{ start: string, end: string }>) {
    for (let index = 0; index < periods.length; index++) {
        
        if (
            !periods[index].start
            || !periods[index].end
            || (index > 0 && periods[index].start < periods[index - 1].end)
            || (periods[index].end < periods[index].start)
        ) {            
            return false;
        }
    }
    return true;
}
)
export function isValid(schedule: Schedule) {
    return DAYS.some(day => schedule[day].open)
        && validatePeriods(schedule.openingPeriods)
        && !DAYS.some(
            day => schedule[day].open && !validateHours(schedule[day].slots)
        );
}

export function isOpen(schedule: Schedule) {
    const currentDate = new Date();
    const dayOfWeek = DAYS[(currentDate.getDay() + 6) % 7];
    const currentHour = currentDate.getHours();
    const currentMinutes = currentDate.getMinutes();
    const currentMonth = currentDate.getMonth();
    const currentDay = currentDate.getDate();

    //Used to check if slot of previous day crossed midnight
    const yesterday = DAYS[(currentDate.getDay() + 5) % 7]
    const lastSlot = schedule[yesterday].slots[schedule[yesterday].slots.length - 1];
    const lastSlotStart = new Date(lastSlot.start);
    const lastSlotEnd = new Date(lastSlot.end);

    return schedule.openingPeriods.some(period => {
        const openingDay = new Date(period.start)
        const closingDay = new Date(period.end)
        return (currentMonth > openingDay.getUTCMonth() || ((currentMonth == openingDay.getUTCMonth()) ? currentDay >= openingDay.getUTCDate() : false))
            && (currentMonth < closingDay.getUTCMonth() || ((currentMonth == closingDay.getUTCMonth()) ? currentDay <= closingDay.getUTCDate() : false))
    })
        && (
            (schedule[yesterday].open && ((lastSlotStart > lastSlotEnd) && (currentHour < lastSlotEnd.getUTCHours() || (currentHour === lastSlotEnd.getUTCHours() && currentMinutes <= lastSlotEnd.getMinutes()))))
            || (
                schedule[dayOfWeek].open && schedule[dayOfWeek].slots.some(slot => {
                    const start = new Date(slot.start);
                    const end = new Date(slot.end);
                    return (currentHour > start.getUTCHours() || (currentHour === start.getUTCHours() && currentMinutes >= start.getMinutes()))
                        && (currentHour < end.getUTCHours() || (currentHour === end.getUTCHours() && currentMinutes <= end.getMinutes()));

                })
            )
        )
}

export function isOpenAllYearRound(schedule: Schedule) {

    if (schedule.openingPeriods.length > 1) {
        return false
    }
    else {
        const period = schedule.openingPeriods[0];
        const startingDay = new Date(period.start);
        const endingDay = new Date(period.end)
        const startDay = startingDay.getDate();
        const startMonth = startingDay.getMonth();
        const endDay = endingDay.getDate();
        const endMonth = endingDay.getMonth();

        return startDay == 1 && startMonth == 0 && endDay == 31 && endMonth == 11;
    }
}