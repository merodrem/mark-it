import { Location } from "../models/location.model";


function _clearUndefined(text:string) {
    return !text ? '' : text;
}

export function stringLocation(location:Location) {

    return _clearUndefined(location.street) 
    + ' ' 
    + _clearUndefined(location.number) 
    + ', ' 
    + _clearUndefined(location.zip) 
    + ' ' 
    + _clearUndefined(location.city) 
    + ' ' 
    + _clearUndefined(location.country.toUpperCase());
}