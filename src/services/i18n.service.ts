import memoize from 'lodash/memoize'
import I18n from "i18n-js"
import { I18nManager } from 'react-native'
import * as RNLocalize from "react-native-localize"

const translationGetters = {
    // lazy requires (metro bundler does not support symlinks)
    en: () => require("../assets/lang/en.json"),
    fr: () => require("../assets/lang/fr.json"),
    nl: () => require("../assets/lang/nl.json")
};

export const translate = memoize(
    (key, config = undefined) => I18n.t(key, config),
    (key, config = undefined) => (config ? key + JSON.stringify(config) : key)
);

export function setI18nConfig() {
    // fallback if no available language fits
    const fallback = { languageTag: "en", isRTL: false };

    const { languageTag } =
        RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
        fallback;

    // clear translation cache
    translate.cache.clear();

    I18n.defaultLocale = "en";
    // set i18n-js config
    I18n.translations = { [languageTag]: translationGetters[languageTag]() };
    I18n.locale = languageTag;
};

export function getCurrentLanguage() {
    return I18n.locale;
}