import Geolocation, {GeolocationOptions, GeolocationResponse} from '@react-native-community/geolocation';
/**
 * wrapping of the geolocation API into a promise
 */
export function getPosition(options?: GeolocationOptions ):Promise<GeolocationResponse> {
  return new Promise(function (resolve, reject) {
    Geolocation.getCurrentPosition(resolve, reject, options);
  });
}
