/**
 * This file is for futur dev. It isn't used so far
 */


import Parse from 'parse/react-native';
import { JAVASCRIPT_KEY, APP_ID, MARKIT_SERVER } from '../../assets/data/conf';
import store from '../../store/configureStore';
const LiveQuery = new class {
    subscription;
    liveClient = new Parse.LiveQueryClient({
        applicationId: APP_ID,
        serverURL: 'wss://' + MARKIT_SERVER,
        javascriptKey: JAVASCRIPT_KEY,
    });;
    query= new Parse.Query('_User');
       
    subscribeUser =this.subscribeUser.bind(this)
    unsubscribe =this.unsubscribe.bind(this)

    constructor() {
        this.liveClient.open();
    }

    subscribeUser(email) {
        this.query.equalTo('email', email)
        this.query.limit(1)
        this.subscription = this.liveClient.subscribe(this.query);
        this.subscription.on('update', user => {
            store.dispatch({type:"UPDATE_USER", payload: user.toJSON()})
        });
    }

    unsubscribe() {
        this.liveClient.unsubscribe(this.subscription);
    }

}
export default LiveQuery;
