var cloudFunctions = require("./cloud-functions");
var cloudJobs = require("./cron-job");

Parse.Cloud.define("signUp", cloudFunctions.signUp(Parse));

Parse.Cloud.define("getMarkets", cloudFunctions.getMarkets(Parse));

Parse.Cloud.define("loadRatings", cloudFunctions.loadRatings(Parse));

Parse.Cloud.define("getAddedMarkets", cloudFunctions.getAddedMarkets(Parse));

Parse.Cloud.define("getVotesFromUser", cloudFunctions.getVotesFromUser(Parse));

Parse.Cloud.define("getRatingsFromUser", cloudFunctions.getRatingsFromUser(Parse));

Parse.Cloud.define("getAddedMarketsRatingsAndVotes", async (request) => {
    return await Promise.all([
        cloudFunctions.getAddedMarkets(Parse)(request),
        cloudFunctions.getRatingsFromUser(Parse)(request),
        cloudFunctions.getVotesFromUser(Parse)(request),
    ])
}
);

Parse.Cloud.define("voteForFeature", cloudFunctions.voteForFeature(Parse));

Parse.Cloud.define("deleteUser", cloudFunctions.deleteUser(Parse));

Parse.Cloud.define("addMarket", cloudFunctions.addMarket(Parse));

Parse.Cloud.define("rateMarket", cloudFunctions.rateMarket(Parse));

Parse.Cloud.define("changeAvatar", cloudFunctions.changeAvatar(Parse));

Parse.Cloud.define("changeEmail", cloudFunctions.changeEmail(Parse));

Parse.Cloud.job("removeInvalidLogin", cloudJobs.removeInvalidLogin(Parse));

Parse.Cloud.afterDelete("Market", (request) => {

    const voteQuery = new Parse.Query('Vote');
    voteQuery.equalTo('market', request.object);
    voteQuery.find()
        .then(votes => Parse.Object.destroyAll(votes, { useMasterKey: true }))
        .catch((error) => {
            console.error("Error finding related votes " + error.code + ": " + error.message);
        });

    const ratingsQuery = new Parse.Query("Rating");
    ratingsQuery.equalTo('market', request.object);
    ratingsQuery.find()
        .then(ratings => Parse.Object.destroyAll(ratings, { useMasterKey: true }))
        .catch((error) => {
            console.error("Error finding related ratings " + error.code + ": " + error.message);
        });

});
