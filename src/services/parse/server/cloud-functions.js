

/********************************
*                               *
*          TEST CONFIG          *
*                               *
********************************/


// if (typeof Parse == 'undefined') {
//     var Parse = require('parse/node');
//     var constants = require("../../../../spec/constants");
//     Parse.initialize(constants.APPLICATION_ID, null, constants.MASTER_KEY);
//     Parse.serverURL = 'https://parseapi.back4app.com'
//     Parse.Cloud.useMasterKey()
// }

/********************************
*                               *
*           CONSTANTS           *
*                               *
********************************/

const MAX_RETURNED_MARKETS = 15;
const MAX_NAME_LENGTH = 60;


/*******************************************
*                                          *
*          MEMOIZATION FROM LODASH         *
*                                          *
*******************************************/

/**
 * Creates a function that memoizes the result of `func`. If `resolver` is
 * provided, it determines the cache key for storing the result based on the
 * arguments provided to the memoized function. By default, the first argument
 * provided to the memoized function is used as the map cache key. The `func`
 * is invoked with the `this` binding of the memoized function.
 *
 * **Note:** The cache is exposed as the `cache` property on the memoized
 * function. Its creation may be customized by replacing the `memoize.Cache`
 * constructor with one whose instances implement the
 * [`Map`](http://ecma-international.org/ecma-262/7.0/#sec-properties-of-the-map-prototype-object)
 * method interface of `clear`, `delete`, `get`, `has`, and `set`.
 * @param {Function} func The function to have its output memoized.
 * @param {Function} [resolver] The function to resolve the cache key.
 * @returns {Function} Returns the new memoized function.
 */
function memoize(func, resolver) {
    if (typeof func !== 'function' || (resolver != null && typeof resolver !== 'function')) {
        throw new TypeError('Expected a function')
    }
    const memoized = function (...args) {
        const key = resolver ? resolver.apply(this, args) : args[0]
        const cache = memoized.cache

        if (cache.has(key)) {
            return cache.get(key)
        }
        const result = func.apply(this, args)
        memoized.cache = cache.set(key, result) || cache
        return result
    }
    memoized.cache = new (memoize.Cache || Map)
    return memoized
}

memoize.Cache = Map



/********************************
*                               *
*       HELPER FUNCTIONS        *
*                               *
********************************/

/**
 * Stores a new vote for a feature in the DB. E.g. a new name, new product types. 
 * This function does not update the Market table, it simply logs the vote in
 * the Vote table.
 * @param {Market} market the market for which we store a vote
 * @param {Parse.User} user the user that voted for a value
 * @param {"marketName"|"marketType"|"productTypes"|"schedule"|"address"} target the target feature we are voting for
 * @param {string} votedValue the JSON-stringified voted value.
 */
async function storeVote(market, user, target, votedValue) {
    var Vote = Parse.Object.extend('Vote');
    const vote = new Vote();
    const query = new Parse.Query('Vote');
    query.equalTo('market', market)
    query.equalTo('user', user)
    query.equalTo('target', target)
    const results = await query.find()
    if (results.length > 0) {
        return Promise.reject(new Error("User " + user.get("username") + " has already voted for the feature " + target));
    }
    else {
        return vote.save(
            {
                market: market,
                user: user,
                target: target,
                votedValue: JSON.stringify(votedValue),
            },
            { useMasterKey: true }
        )
            .then(success => { return Promise.resolve(success) })
            .catch(error => {
                return Promise.reject(error)
            });
    }
}
/**
 * Function to encode texts mor uniformly by
 * trimming, setting to lower case and replacing
 * spaces by '-'
 * Used for addresses
 * @param {*} text 
 */
function normalizeText(text) {
    return text.toLowerCase().trim().substring(0, MAX_NAME_LENGTH).replace(/\s+/g, '-');
}

/**
 * Compares if 2 schedules are equals. Schedules
 * are Objects of structure:
 * {
 *      monday: {
 *          open: boolean,
 *          slots: Array<{ start: isoString, end: isoString }>
 *      }
 *      tuesday: {...}
 *      ...
 *      startingDay: isoString,
 *      endingDay: isoString
 * }
 * @param {*} scheduleA the first schedule
 * @param {*} scheduleB the second schedule
 */
function areSchedulesEqual(scheduleA, scheduleB) {
    const periodsA = scheduleA.openingPeriods;
    const periodsB = scheduleB.openingPeriods;

    const areOpeningPeriodsEqual = periodsA.length == periodsB.length
        && !periodsA.some((period, index) => {
            const startingDayA = new Date(period.start);
            const endingDayA = new Date(period.end)
            const startingDayB = new Date(periodsB[index].start);
            const endingDayB = new Date(periodsB[index].end)
            const startDayA = startingDayA.getDate();
            const startMonthA = startingDayA.getMonth();
            const endDayA = endingDayA.getDate();
            const endMonthA = endingDayA.getMonth();
            const startDayB = startingDayB.getDate();
            const startMonthB = startingDayB.getMonth();
            const endDayB = endingDayB.getDate();
            const endMonthB = endingDayB.getMonth();

            return startDayA !== startDayB
                || startMonthA !== startMonthB
                || endDayA !== endDayB
                || endMonthA !== endMonthB;
        })
    /*
     * schedules are equals if there isn't a day where:
     * - one schedule is open and not the other one OR
     * - the number of slots differ for an open day OR
     * - there is a slot for an open day where times don't match
     */
    const result = areOpeningPeriodsEqual && !['sunday',
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'saturday'].some(day => {
            const is_A_open = scheduleA[day].open;
            const is_B_open = scheduleB[day].open;
            const number_slots_A = scheduleA[day].slots.length
            const number_slots_B = scheduleB[day].slots.length

            return is_A_open ?
                is_A_open !== is_B_open
                || number_slots_A !== number_slots_B
                || scheduleA[day].slots.some((slot, index) => {
                    const slotB = scheduleB[day].slots[index]
                    const a_start_hours = new Date(slot.start).getUTCHours()
                    const a_start_minutes = new Date(slot.start).getUTCMinutes()
                    const b_start_hours = new Date(slotB.start).getUTCHours()
                    const b_start_minutes = new Date(slotB.start).getMinutes()
                    const a_end_hours = new Date(slot.end).getUTCHours()
                    const a_end_minutes = new Date(slot.end).getMinutes()
                    const b_end_hours = new Date(slotB.end).getUTCHours()
                    const b_end_minutes = new Date(slotB.end).getMinutes()

                    return a_start_hours !== b_start_hours
                        || a_start_minutes !== b_start_minutes
                        || a_end_hours !== b_end_hours
                        || a_end_minutes !== b_end_minutes;
                })
                : is_A_open !== is_B_open

        })

    return result;

}


/**
 * compares two locations by checking their coordinates then adresses
 * 
 * @param {*} location1 
 * @param {*} location2 
 */
function areLocationsEquals(location1, location2) {

    var point1 = new Parse.GeoPoint(location1.coordinates);
    var point2 = new Parse.GeoPoint(location2.coordinates);
    if (point1.kilometersTo(point2) !== 0) {
        return false;
    }
    else {
        return normalizeText(location1.street) == normalizeText(location2.street)
            && location1.number == location2.number
            && location1.zip == location2.zip
            && normalizeText(location1.city) == normalizeText(location2.city)
            && normalizeText(location1.country) == normalizeText(location2.country)
    }
}

/**
 * From pairs of [type, voteCount], this function determines which
 * types should be taken into account. The algorithm is simple:
 * We compute the average voteCount, and return types which have a 
 * count superior to the average.
 * @param {*} productTypeVote 
 */
function selectProductType(productTypeVote) {
    const averageRating = productTypeVote.map(elem => elem[1]).reduce((acc, currentValue) => acc + currentValue) / productTypeVote.length;
    return productTypeVote.filter(elem => elem[1] >= averageRating).map(elem => elem[0]);
}


async function getMostVotedValue(market, targetFeature) {
    // FIXME: this query could be greatly improved. We have to find a way to use aggregate and map-reduce
    // in order to get most voted value.
    const query = new Parse.Query("Vote");
    query.equalTo('market', market);
    query.equalTo('target', targetFeature);
    query.limit(50)//Quick fix to only evaluate most recent ratings. 
    query.descending("updatedAt")
    const votes = await query.find();

    const voteCount = {}

    const lookupIndex = memoize(function (votedValue, targetFeature) {
        switch (targetFeature) {
            case "marketName":
            case "marketType":
                return votes.findIndex(v => normalizeText(JSON.parse(v.get("votedValue"))) == normalizeText(JSON.parse(votedValue)));
            case "schedule":
                return votes.findIndex(v => areSchedulesEqual(JSON.parse(v.get("votedValue")), JSON.parse(votedValue)));
            case "address":
                return votes.findIndex(v => areLocationsEquals(JSON.parse(v.get("votedValue")), JSON.parse(votedValue)));
            default:
                return undefined;
        }
    });
    if (targetFeature != "productTypes") {
        votes.forEach(vote => {
            const indexOfVote = lookupIndex(vote.get("votedValue"), targetFeature);

            const voteObjectKey = votes[indexOfVote].id;
            if (voteObjectKey in voteCount) {
                voteCount[voteObjectKey]++;
            }
            else {
                voteCount[voteObjectKey] = 1
            }
        })
        const entries = Object.entries(voteCount);
        const sorted = entries.sort((a, b) => b[1] - a[1]);

        return JSON.parse(votes.find(v => v.id == sorted[0][0]).get("votedValue"))
    }
    else {
        votes.forEach(vote => {
            JSON.parse(vote.get("votedValue")).forEach(type => (type in voteCount) ? voteCount[type]++ : voteCount[type] = 1)
        })
        return selectProductType(Object.entries(voteCount))
    }
}


/**
 * Checks if a market with the given location exist
 * @param {*} location 
 * @param {*} marketId Optionnal parameter. If set, the function will search markets excluding this market
 */
async function marketLocationExists(location, marketId = null) {
    const point = new Parse.GeoPoint(location.coordinates);
    const query = new Parse.Query("Market");
    query.withinKilometers("gps", point, 0)
    const results = await query.find()

    return results.some(market => market.id !== marketId && areLocationsEquals(location, market.get('location')))
}

/**
 * Computes the average of all ratings given to a market
 * @param {*} marketId the ID of the market
 */
async function getAverageRating(marketId) {
    const query = new Parse.Query('Rating');
    var pipeline = {
        match: { market: marketId },
        group: {
            objectId: null,
            ratingAverage: { $avg: '$rating' }
        },
        project: {
            ratingAverage: "$ratingAverage"
        }
    };
    const ratings = await query.aggregate(pipeline);
    return (ratings.length > 0 ? ratings[0].ratingAverage : undefined)
}

function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

/********************************
*                               *
*        CLOUD FUNCTIONS        *
*                               *
********************************/

/**
 * @returns a promise resolving to the created user
 */
module.exports.signUp = function (Parse) {
    return async function (request) {
        if (!('username' in request.params) || !('password' in request.params) || !('email' in request.params)) {
            throw "email, password, and username are mandatory!"
        }
        else if (!validateEmail(request.params.email)) {
            throw 'invalid email address: ' + request.params.email;
        }

        const user = new Parse.User();
        user.set("username", request.params.username)
        user.set("password", request.params.password)
        user.set("email", request.params.email)

        return user.signUp();


    }
}

/**
 * @returns a promise resolving to a list of found markets
 */
module.exports.getMarkets = function (Parse) {
    return async function (request) {
        //region is a [northEast, southWest] array where points are expressed as [longitude, latitude]
        const { region, filters } = request.params;
        if (!filters) {
            throw "Filters are missing from the request params"

        }
        else if (!filters.types) {
            throw "Market types are missing from the request params"
        }
        const query = new Parse.Query('Market');
        const northEast = new Parse.GeoPoint({ latitude: region[0][1], longitude: region[0][0] })
        const southWest = new Parse.GeoPoint({ latitude: region[1][1], longitude: region[1][0] })
        query.withinGeoBox("gps", southWest, northEast)
        query.containedIn('marketType', filters.types)
        query.limit(MAX_RETURNED_MARKETS)
        const result = await query.select("objectId", "marketType", "productTypes", "name", "location", "schedule", "averageRatings", "originalPoster").find();
        return result.map(m => ({
            objectId: m.id,
            name: m.get("name"),
            productTypes: m.get("productTypes"),
            marketType: m.get("marketType"),
            location: m.get("location"),
            schedule: m.get("schedule"),
            averageRatings: m.get("averageRatings"),
            originalPoster: {
                objectId: m.get("originalPoster").id
            }
        })
        )
    }
}

module.exports.loadRatings = function (Parse) {
    return async function (request) {
        const { marketId, page, pageSize } = request.params;
        const market = await new Parse.Query("Market").get(marketId);
        const query = new Parse.Query("Rating");
        query.equalTo("market", market);
        query.skip(page * pageSize);
        query.limit(pageSize);
        query.descending("updatedAt")
        return (await query.select("objectId", "rating", "comment", "market", "createdAt").find()).map(rating => rating.toJSON());
    }
}

module.exports.getAddedMarkets = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw "the user must be logged in";
        }
        const query = new Parse.Query('Market');
        query.equalTo('originalPoster', user)
        const markets = await query.find();
        return markets.map(market => (market = market.toJSON(), { ...market, originalPoster: { objectId: market.originalPoster.objectId } }))

    }
}

module.exports.getVotesFromUser = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw "the user must be logged in";
        }
        const query = new Parse.Query('Vote');
        query.equalTo('user', user)
        const innerQuery = new Parse.Query('Market')
        innerQuery.notEqualTo("originalPoster", user);
        query.matchesQuery("market", innerQuery);
        const votes = await Parse.Object.fetchAllWithInclude(await query.find(), 'market')
        return votes.map(vote => {
            vote = vote.toJSON();
            vote.market = {
                objectId: vote.market.objectId,
                name: vote.market.name,
                productTypes: vote.market.productTypes,
                marketType: vote.market.marketType,
                location: vote.market.location,
                schedule: vote.market.schedule,
                averageRatings: vote.market.averageRatings,
                originalPoster: {
                    objectId: vote.market.originalPoster.objectId
                }
            };
            return vote;
        })
    }
}

module.exports.getRatingsFromUser = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw "the user must be logged in";
        }
        const query = new Parse.Query('Rating');
        query.equalTo('user', user)
        const ratings = await Parse.Object.fetchAllWithInclude(await query.find(), 'market')
        return ratings.map(rating => {
            rating = rating.toJSON();
            rating.market = {
                objectId: rating.market.objectId,
                name: rating.market.name,
                productTypes: rating.market.productTypes,
                marketType: rating.market.marketType,
                location: rating.market.location,
                schedule: rating.market.schedule,
                averageRatings: rating.market.averageRatings,
                originalPoster: {
                    objectId: rating.market.originalPoster.objectId
                }
            };
            return rating;
        })
    }
}

module.exports.voteForFeature = function (Parse) {
    return async (request) => {
        if (!request.user) {
            throw 'you must be logged in to vote for a market feature!';
        }
        else {
            const query = new Parse.Query("Market")
            query.get(request.params.marketId)
            const result = await query.find()

            if (result.length == 1) {
                const market = result[0]
                if (market.get("originalPoster").id == request.user.id) {
                    throw "you cannot vote for a market you added yourself!"
                }
                const vote = (await storeVote(market, request.user, request.params.featureType, request.params.payload)).toJSON()
                const mostVotedValue = await getMostVotedValue(market, request.params.featureType)
                switch (request.params.featureType) {
                    case "marketName":
                        market.set("name", mostVotedValue)
                        break;
                    case "marketType":
                        market.set("marketType", mostVotedValue)
                        break;
                    case "productTypes":
                        market.set("productTypes", mostVotedValue)
                        break;
                    case "schedule":
                        market.set("schedule", mostVotedValue)
                        break;
                    case "address":
                        const newCoordinates = mostVotedValue.coordinates;
                        market.set('gps', new Parse.GeoPoint(newCoordinates))
                        market.set("location", mostVotedValue)
                        break;
                    default:
                        throw "invalid feature: " + request.params.featureType;
                }
                await market.save(
                    null,
                    { useMasterKey: true }
                )
                vote.market = {
                    objectId: market.id,
                    name: market.get("name"),
                    productTypes: market.get("productTypes"),
                    marketType: market.get("marketType"),
                    location: market.get("location"),
                    schedule: market.get("schedule"),
                    averageRatings: market.get("averageRatings"),
                    originalPoster: market.get("originalPoster")
                };
                vote.user = {
                    objectId: vote.user.objectId,
                };
                return vote;
            }
            else if (result.length == 0) {
                throw ("Couldn't find market with id: " + request.params.marketId);
            }
            else {
                throw ("there should be only one market with id: " + request.params.marketId);
            }


        }
    }
}

module.exports.deleteUser = function (Parse) {
    return async function (request) {
        if (!request.user) {
            throw 'no user logged in. Impossible to delete account';
        }
        else {
            return await request.user.destroy({ useMasterKey: true });
        }
    }
}

module.exports.addMarket = function (Parse) {
    return async function (request) {
        if (!request.user) {
            throw 'you must be logged in to create a market';
        }
        else {
            const Market = Parse.Object.extend('Market');
            const market = new Market();
            const locationUpdated = request.params.marketData.location
            const alreadExists = await marketLocationExists(locationUpdated)
            if (alreadExists) {
                throw new Parse.Error(1, 'A market already exists at this address!');
            }
            const point = new Parse.GeoPoint(locationUpdated.coordinates);
            locationUpdated.coordinates = point;
            const result = await market.save(
                {
                    originalPoster: request.user,
                    name: request.params.marketData.name.trim().substring(0, MAX_NAME_LENGTH),
                    marketType: request.params.marketData.type,
                    productTypes: request.params.marketData.productTypes,
                    schedule: request.params.marketData.schedule,
                    location: locationUpdated,
                    averageRatings: undefined,
                    gps: point
                },
                { useMasterKey: true }
            )
            storeVote(result, request.user, "marketName", request.params.marketData.name.trim().substring(0, MAX_NAME_LENGTH))
            storeVote(result, request.user, "marketType", request.params.marketData.type)
            storeVote(result, request.user, "productTypes", request.params.marketData.productTypes)
            storeVote(result, request.user, "schedule", request.params.marketData.schedule)
            storeVote(result, request.user, "address", locationUpdated)
            return {
                objectId: result.id,
                name: result.get("name"),
                productTypes: result.get("productTypes"),
                marketType: result.get("marketType"),
                location: result.get("location"),
                schedule: result.get("schedule"),
                averageRatings: result.get("averageRatings"),
                originalPoster: {
                    objectId: result.get("originalPoster").id
                },
            }
        }


    }
}

module.exports.rateMarket = function (Parse) {
    return async function (request) {

        if (!request.user) {
            throw ('you must be logged in to add a rating');
        }
        else {
            const market = await new Parse.Query('Market').get(request.params.marketId);
            var Rating = Parse.Object.extend('Rating');
            const rating = new Rating();
            const query = new Parse.Query('Rating');
            query.equalTo('market', market)
            query.equalTo('user', request.user)
            const results = await query.find()
            if (results.length > 0) {
                throw 'This user has already rated the market';
            }
            else {
                await rating.save(
                    {
                        market: market,
                        user: request.user,
                        rating: request.params.rating,
                        comment: request.params.comment,
                    },
                    { useMasterKey: true }
                )
                market.save({
                    averageRatings: await getAverageRating(request.params.marketId)
                }, { useMasterKey: true })
                const result = (await rating.fetchWithInclude("market")).toJSON();

                //Ugly as hell, related to:
                //https://github.com/parse-community/Parse-SDK-JS/issues/258#issuecomment-209988940
                result.market = {
                    objectId: result.market.objectId,
                    name: result.market.name,
                    productTypes: result.market.productTypes,
                    marketType: result.market.marketType,
                    location: result.market.location,
                    schedule: result.market.schedule,
                    averageRatings: result.market.averageRatings,
                    originalPoster: result.market.originalPoster,
                }

                return result;
            }
        }
    }
}

module.exports.changeAvatar = function (Parse) {
    return async function (request) {
        if (!request.user) {
            throw ('you must be logged in change your avatar');
        }
        const { filename, base64 } = request.params
        const userUpdated = await request.user.save(
            {
                avatar: new Parse.File(filename ? filename : "unnamed_file", { base64: base64 })
            },
            { useMasterKey: true }
        )
        return userUpdated.toJSON();
    }
}

module.exports.changeEmail = function (Parse) {
    return async function (request) {
        if (!request.user) {
            throw ('you must be logged in to change your email');
        }
        const savedUser = await request.user.save({ email: request.params.email }, { useMasterKey: true });
        return savedUser.toJSON()

    }
}