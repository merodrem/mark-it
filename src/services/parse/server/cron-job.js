module.exports.removeInvalidLogin = function (Parse) {
    return async function(request) {
        let date = new Date();
        let timeNow = date.getTime();
        let intervalOfTime = 48 * 60 * 60 * 1000;  // delete unverified users older than 2 days
        let timeThen = timeNow - intervalOfTime;

        // Limit date
        let queryDate = new Date();
        queryDate.setTime(timeThen);

        // The query object
        let query = new Parse.Query(Parse.User);

        // Query the users that still unverified after 3 minutes
        query.equalTo("emailVerified", false);
        query.lessThanOrEqualTo("createdAt", queryDate);

        const results = await query.find({ useMasterKey: true });

        results.forEach(object => {
            object.destroy({ useMasterKey: true }).then(destroyed => {
                console.log("Successfully destroyed object" + JSON.stringify(destroyed));
            }).catch(error => {
                console.log("Error: " + error.code + " - " + error.message);
            })
        });

        return ("Successfully retrieved " + results.length + " invalid logins.");
    }
}