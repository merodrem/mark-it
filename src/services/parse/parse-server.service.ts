import Parse from 'parse/react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { RATING_PAGE_SIZE } from '../../assets/data/appData';
import { APP_ID, JAVASCRIPT_KEY, SERVER_URL } from '../../assets/data/conf';
import { MarketFilters, Market, MarketType, ProductType } from '../../models/market.model';
import { Location, Region } from '../../models/location.model';
import { Schedule } from '../../models/schedule.model';
import { Avatar } from '../../models/avatar.model';
import { MarketFeature, Vote } from '../../models/vote.model';
import { RatingModel } from '../../models/rating.model';
import { isOpen } from '../schedule.service';

Parse.serverURL = SERVER_URL;
Parse.initialize(APP_ID, JAVASCRIPT_KEY);
Parse.setAsyncStorage(AsyncStorage)
Parse.masterKey = "qFNT4BZITaZHvIilxs8aUAlXCBT8LRpZF7sXZHKz"

/**
 * 
 * @param {*} email 
 * @param {*} password 
 * @returns the new user in JSON
 */
async function signUp(username: string, email: string, password: string) {
    try {
        return await Parse.Cloud.run('signUp', { username, email, password });
    } catch (error) {
        return Promise.reject(error)
    }
}
/**
 * 
 * @param {string} email 
 * @param {string} password 
 * @returns the JSON encoded user logged in
 */
async function signIn(email: string, password: string) {

    try {
        const user = await Parse.User.logIn(email, password)
        return user.toJSON()
    } catch (error) {
        return Promise.reject(error)
    }
}


async function logout() {
    try {
        return await Parse.User.logOut();

    } catch (error) {
        return Promise.reject(error)

    }
}


async function deleteUser() {
    try {
        await Parse.Cloud.run('deleteUser', {});
        return await Parse.User.logOut();
    } catch (error) {
        return Promise.reject(error)
    }
}


/**
 * 
 * @param {Region} region - the region where to look for markets
 * @param {MarketFilters} filters - the filter of the request
 */
async function getMarkets(region: Region, filters: MarketFilters): Promise<Market[]> {
    try {
        const results: Array<Market> = await Parse.Cloud.run('getMarkets', { region: region, filters: filters });
        if (filters.filterOnOpen) {
            return results.filter(elem => isOpen(elem.schedule))
        }
        else {
            return results;
        }
    }
    catch (error) {
        return Promise.reject(error)
    }

}

export type MarketPayload = {
    name: string,
    schedule: Schedule,
    location: Location,
    type: MarketType,
    productTypes: ProductType[],
}
async function addMarket(marketData: MarketPayload) {
    const params = { marketData: marketData };
    try {
        return await Parse.Cloud.run('addMarket', params);
    }
    catch (error) {
        return Promise.reject(error)
    }
}

async function rateMarket(marketId: string | number, rating: number, comment: string | undefined): Promise<RatingModel> {
    const params = { marketId: marketId, rating: rating, comment: comment };
    try {
        const rating: RatingModel = await Parse.Cloud.run('rateMarket', params)
        return rating;
    } catch (error) {
        return Promise.reject(error)
    }
}


async function loadRatings(marketId: string | number, page = 0) {
    try {
        return await Parse.Cloud.run("loadRatings", { marketId, page, pageSize: RATING_PAGE_SIZE });
    }
    catch (error) {
        return Promise.reject(error)
    }

}
type VoteParams = {
    marketId: string | number,
    featureType: MarketFeature,
    payload: any
}
async function voteForMarketFeature(params: VoteParams): Promise<Vote> {

    try {
        return await Parse.Cloud.run("voteForFeature", params);

    } catch (error) {
        return Promise.reject(error)
    }
}

async function changeEmail(email: string) {
    return await Parse.Cloud.run('changeEmail', {email});

}

function changePassword(email: string) {
    return Parse.User.requestPasswordReset(email)
}

// Ah bit of copy/pasting from getAddedMarkets(), but it seems more readable this way than with 
// a generic function
async function getAddedMarkets() {
    const markets: Array<any> = await Parse.Cloud.run('getAddedMarkets');
    return markets;
}

// Ah bit of copy/pasting from getAddedMarkets(), but it seems more readable this way than with 
// a generic function
async function getVotesFromUser() {
    const votes: Array<any> = await Parse.Cloud.run('getVotesFromUser');
    return votes;

}

// Ah bit of copy/pasting from getAddedMarkets(), but it seems more readable this way than with 
// a generic function
async function getRatingsFromUser() {
    const ratings: Array<any> = await Parse.Cloud.run('getRatingsFromUser');
    return ratings;

}


async function getAddedMarketsRatingsAndVotes(): Promise<[Market[], RatingModel[], Vote[]]> {
    return await Parse.Cloud.run('getAddedMarketsRatingsAndVotes');
}

async function changeAvatar(avatar: Avatar) {
    return await Parse.Cloud.run('changeAvatar', avatar);
}

export {
    signUp,
    signIn,
    logout,
    deleteUser,
    addMarket,
    getMarkets,
    rateMarket,
    loadRatings,
    voteForMarketFeature,
    changeEmail,
    changePassword,
    getAddedMarkets,
    getVotesFromUser,
    getRatingsFromUser,
    changeAvatar,
    getAddedMarketsRatingsAndVotes
}

