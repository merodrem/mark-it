import { Market } from "./market.model";

export interface RatingModel {
    objectId: number|string,
    rating: number,
    comment: string,
    market: Market,
    createdAt: string
}