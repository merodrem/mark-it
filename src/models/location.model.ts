
export interface LatLng {
    latitude: number,
    longitude:number
}

export interface Location {
    street: string,
    number: string,
    zip: string,
    city: string,
    country: string,
    coordinates?: LatLng,
}
//NorthEast, SouthWest boundary coordinates
export type Region = [[number, number],[number, number]]