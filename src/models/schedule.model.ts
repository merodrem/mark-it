export type Slot = { start: string|undefined, end: string|undefined }
export type DayOfWeek = 'sunday' | 'monday' | 'tuesday' | 'wednesday' | 'thursday' | 'friday' | 'saturday'
export interface Day {
    open: boolean,
    slots: Array<Slot>
}
export interface Schedule {
    monday: Day,
    tuesday: Day,
    wednesday: Day,
    thursday: Day,
    friday: Day,
    saturday: Day,
    sunday: Day,
    openingPeriods: Array<{ start: string, end: string }>
}