import { Location } from "./location.model";
import { Schedule } from "./schedule.model";

export type MarketType = "indoor"|"outdoor"|"producer";
export type ProductType = 'organic'|'food'|'streetFood'|'alcohol'|'groceries'|'clothes'|'flea'|'other';

export interface Market {
    objectId: number|string,
    name: string,
    productTypes: Array<ProductType>,
    marketType: MarketType,
    location: Location,
    schedule: Schedule,
    averageRatings: number|undefined,
    originalPoster:any
}

export interface MarketFilters {
    filterOnOpen: boolean, 
    types: Array<MarketType
}