export interface Avatar {
    filename: string | undefined,
    base64: string
}