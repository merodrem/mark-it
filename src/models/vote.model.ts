import { Market } from "./market.model";

export type MarketFeature = "marketName"|"marketType"|"productTypes"|"schedule"|"address"

export interface Vote{
    objectId: number|string,
    market: Market,
    target: MarketFeature,
    votedValue: string
}